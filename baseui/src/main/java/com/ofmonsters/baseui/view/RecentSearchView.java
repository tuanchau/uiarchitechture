package com.ofmonsters.baseui.view;

import android.content.Context;
import android.support.v7.widget.SearchView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ofmonsters.baseui.R;
import com.ofmonsters.baseui.listfragment.SearchStringAction;

import java.util.List;

/**
 * Created by tuanchauict on 11/24/15.
 */
@Deprecated
public class RecentSearchView extends FrameLayout {
    ListView mListView;
    RecentSearchAdapter mAdapter;

    SearchView mSearchView;

    View mBtnClear;

    private SearchStringAction mSearchStringAction;

    public RecentSearchView(Context context) {
        this(context, null);
    }

    public RecentSearchView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecentSearchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.recent_search, this, true);
        mListView = (ListView) view.findViewById(R.id.list_view);
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(params);

        mBtnClear = view.findViewById(R.id.btn_clear);
        mBtnClear.setOnClickListener(v -> {
            if (mSearchStringAction != null) {
                mSearchStringAction.clearSearchStrings();
            }
//            AppSettings.setRecentSearchTexts(null);
            mAdapter.setItems(null);
            mBtnClear.setEnabled(false);
        });

        mListView.setOnItemClickListener((parent, view1, position, id) -> {
            if (mSearchView != null) {
                mSearchView.setQuery(mAdapter.getItem(position), true);
            }

            hide();
        });
    }


    public void setSearchStringAction(SearchStringAction keywordAction) {
        mSearchStringAction = keywordAction;
    }

    public void setSearchView(SearchView searchView) {
        mSearchView = searchView;
    }

    public void show() {
        if (mAdapter == null) {
            mAdapter = new RecentSearchAdapter(getContext());
        }
        if (mSearchStringAction != null) {
            mAdapter.setItems(mSearchStringAction.getRecentSearchStrings());
        } else {
            mAdapter.setItems(null);
        }
        mBtnClear.setEnabled(mAdapter.getCount() > 0);
        mListView.setAdapter(mAdapter);
        setVisibility(VISIBLE);
    }

    public void hide() {
        setVisibility(GONE);
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
    }



    private static class RecentSearchAdapter extends ArrayAdapter<String> {

        private List<String> mItems;

        private LayoutInflater mInflater;

        public RecentSearchAdapter(Context context) {
            super(context, 0);
            mInflater = LayoutInflater.from(context);
        }

        public void setItems(List<String> items) {
            mItems = items;
            notifyDataSetChanged();
        }

        @Override
        public String getItem(int position) {
            return mItems.get(position);
        }

        @Override
        public int getCount() {
            return mItems == null ? 0 : mItems.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.item_recent_search_suggest, parent, false);
                TextView txt = (TextView) convertView.findViewById(R.id.text);
                convertView.setTag(txt);
            }

            ((TextView) convertView.getTag()).setText(mItems.get(position));
            return convertView;
        }
    }
}
