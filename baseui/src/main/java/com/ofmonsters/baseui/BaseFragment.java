package com.ofmonsters.baseui;

import android.support.annotation.IdRes;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewStub;

import com.trello.rxlifecycle.components.support.RxFragment;

/**
 * Created by tuanchauict on 9/18/16.
 */
public abstract class BaseFragment extends RxFragment {
    private SparseArray<View> mViewStubSparseArray;
    
    @Override
    public void onDestroyView() {
        mViewStubSparseArray = null;
        super.onDestroyView();
    }
    
    /**
     * Find view inside ViewStub layout
     *
     * @param stubId
     * @param viewId
     * @param <T>
     * @return
     */
    public <T extends View> T findLazyViewById(@IdRes int stubId, @IdRes int viewId) {
        if (mViewStubSparseArray == null) {
            mViewStubSparseArray = new SparseArray<>();
        }
        View container = getView();
        if (container == null) {
            return null;
        }
        View parent = mViewStubSparseArray.get(stubId);
        if (parent == null) {
            ViewStub stub = (ViewStub) container.findViewById(stubId);
            if (stub == null) {
                return null;
            }
            parent = stub.inflate();
            mViewStubSparseArray.put(stubId, parent);
            if (parent == null) {
                return null;
            }
        }
        return (T) parent.findViewById(viewId);
    }
    
    public <T extends View> T findLazyViewById(View view, int stubId, int viewId) {
        ViewStub stub = (ViewStub) view.findViewById(stubId);
        if(stub == null)
            return null;
        
        return (T) stub.inflate().findViewById(viewId);
    }
    
    public <T extends View> T findView(View view, @IdRes int id) {
        View v = view.findViewById(id);
        if (v == null)
            return null;
        return (T) v;
        
    }
    
    protected boolean isContextInvalid() {
        return getContext() == null || getActivity().isFinishing();
    }

    public boolean isValid() {
        return getActivity() != null && !getActivity().isFinishing() && isAdded();
    }

}
