package com.ofmonsters.baseui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by tuanchauict on 10/22/16.
 */

public abstract class AbstractCatalogViewHolder<ItemClickListener> extends RecyclerView.ViewHolder {

    private Object mData;
    private ItemClickListener mListener;


    public AbstractCatalogViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void render(Object data, boolean selectionMode, boolean selected);

    public Object getData() {
        return mData;
    }

    void setData(Object data) {
        mData = data;
    }

    protected ItemClickListener getListener() {
        return mListener;
    }

    void setListener(ItemClickListener listener) {
        mListener = listener;
    }
}
