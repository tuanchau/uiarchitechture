package com.ofmonsters.baseui.adapter;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ofmonsters.baseui.list.Item;
import com.ofmonsters.baseui.list.MasterList;
import com.ofmonsters.baseui.utils.ScreenUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tuanchauict on 9/15/16.
 */
@Deprecated
public abstract class AdMasterAdapter<ItemClickListener> extends AbstractMasterAdapter<ItemClickListener> {

    public interface NativeAdHandler {
        AdEntry getAd();
    }

    private Map<Item, AdEntry> mAdEntryCache = new HashMap<>();
    private final NativeAdHandler mNativeAdHandler;


    /**
     * @param context
     * @param masterList
     * @param adHandler  may be null on no ad
     */
    public AdMasterAdapter(Context context, @Nullable MasterList masterList, @Nullable NativeAdHandler adHandler) {
        super(context, masterList);
        mNativeAdHandler = adHandler;
    }
}
