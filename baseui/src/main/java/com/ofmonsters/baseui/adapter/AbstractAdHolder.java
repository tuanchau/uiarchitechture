package com.ofmonsters.baseui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by tuanchauict on 9/18/16.
 */
@Deprecated
public abstract class AbstractAdHolder extends RecyclerView.ViewHolder {
    public AbstractAdHolder(View itemView) {
        super(itemView);
    }
    
    public abstract void renderAd(AdEntry adEntry);
}
