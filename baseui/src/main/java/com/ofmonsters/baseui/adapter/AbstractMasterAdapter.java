package com.ofmonsters.baseui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ofmonsters.baseui.list.AdProvider;
import com.ofmonsters.baseui.list.DataProvider;
import com.ofmonsters.baseui.list.Item;
import com.ofmonsters.baseui.list.MasterList;
import com.ofmonsters.baseui.list.Section;
import com.ofmonsters.baseui.list.SectionProvider;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by tuanchauict on 9/11/16.
 */
public abstract class AbstractMasterAdapter<ItemClickListener> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    
    private MasterList mMasterList;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<String> mSectionIndexNames = new ArrayList<>();
    private List<Integer> mSectionAbsolutePosition = new ArrayList<>();
    
    private ItemClickListener mOnItemClickListener;
    
    private Collection<Integer> mSelectedData = new HashSet<>();
    private boolean mSelectionMode = false;
    
    
    public AbstractMasterAdapter(Context context, @Nullable MasterList masterList) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        setMasterList(masterList);
    }
    
    public MasterList getMasterList() {
        return mMasterList;
    }
    
    public synchronized void setMasterList(@Nullable MasterList masterList) {
        if (masterList != mMasterList && mMasterList != null)
            mMasterList.release(mMasterList);
        
        mMasterList = masterList;
        
        initSectionIndexes(masterList == null ? null : masterList.getSectionProvider());
        notifyDataSetChanged();
    }
    
    public Item getItem(int position) {
        return mMasterList.get(position);
    }
    
    @Override
    public int getItemViewType(int position) {
        Item item = mMasterList.get(position);
        if (item == null) {
            throw new RuntimeException("position = " + position);
        }
        if(item.getType() == MasterList.TYPE_AD){
            return (item.getType() << 10) + ((AdProvider.AdItem)item).getAdType();
        }
        return item.getType();
    }
    
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder;
        Logger.d("viewType = %s", viewType);
        if (viewType == MasterList.TYPE_ITEM) {
            holder = onCreateItemViewHolder(mLayoutInflater, parent);
        } else if (isTypeAd(viewType)) {
            holder = onCreateAdViewHolder(mLayoutInflater, parent, getAdType(viewType));
        } else if (viewType == MasterList.TYPE_SECTION) {
            holder = onCreateSectionViewHolder(mLayoutInflater, parent);
        } else if (viewType == MasterList.TYPE_FOOTER) {
            holder = onCreateFooterViewHolder(mLayoutInflater, parent);
        } else {
            holder = onCreateCustomViewHolder(mLayoutInflater, parent, viewType);
        }
        return holder;
    }
    
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = holder.getItemViewType();
        Item item = mMasterList.get(position);
        if (item == null) {
            throw new RuntimeException("position = " + position);
        }
        if (type == MasterList.TYPE_ITEM) {
            onBindItemViewHolder(holder, item);
        } else if (isTypeAd(type)) {
            onBindAdViewHolder(holder, item);
        } else if (type == MasterList.TYPE_SECTION) {
            onBindSectionViewHolder(holder, item);
        } else if (type == MasterList.TYPE_FOOTER) {
            onBindFooterViewHolder(holder, item);
        } else {
            onBindCustomViewHolder(holder, item);
        }
    }
    
    @NonNull
    public abstract AbstractCatalogViewHolder onCreateItemViewHolder(LayoutInflater inflater, ViewGroup parent);
    
    public void onBindItemViewHolder(RecyclerView.ViewHolder holder, Item item) {
        AbstractCatalogViewHolder vh = (AbstractCatalogViewHolder) holder;
        ((AbstractCatalogViewHolder) holder).setListener(mOnItemClickListener);
        Object data = item.getData();

        // TODO beware of screen orientation. Maybe keep selected positions instead?
        boolean isSelected = false;
        if (item instanceof DataProvider.DataItem) {
            DataProvider.DataItem dataItem = (DataProvider.DataItem) item;
            isSelected = mSelectedData.contains(dataItem.getData().getId());
        }

        vh.setData(data);
        vh.render(item.getData(), mSelectionMode, isSelected);
    }
    
    @NonNull
    public abstract AbstractSectionViewHolder onCreateSectionViewHolder(LayoutInflater inflater, ViewGroup parent);
    
    public void onBindSectionViewHolder(RecyclerView.ViewHolder holder, Item item) {
        ((AbstractSectionViewHolder) holder).render((Section) item.getData());
    }
    
    @NonNull
    public abstract RecyclerView.ViewHolder onCreateAdViewHolder(LayoutInflater inflater, ViewGroup parent, int adType);
    
    public abstract void onBindAdViewHolder(RecyclerView.ViewHolder holder, Item item);
    
    @NonNull
    public AbstractCustomViewHolder onCreateCustomViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        throw new UnsupportedOperationException("Custom view must be implemented this function before used");
    }
    
    public void onBindCustomViewHolder(RecyclerView.ViewHolder viewHolder, Item item) {
        ((AbstractCustomViewHolder) viewHolder).render(item.getData());
    }
    
    @NonNull
    public AbstractFooterHolder onCreateFooterViewHolder(LayoutInflater inflater, ViewGroup parent) {
        throw new UnsupportedOperationException("Footer view must be implemented this function before used");
    }
    
    public void onBindFooterViewHolder(RecyclerView.ViewHolder viewHolder, Item item) {
        ((AbstractFooterHolder) viewHolder).render(item.getData(), item.getColumn(), item.getSpanSize());
    }
    
    
    @Override
    public int getItemCount() {
        return (mMasterList == null ? 0 : mMasterList.size());
    }
    
    public Context getContext() {
        return mContext;
    }
    
    private void initSectionIndexes(@Nullable SectionProvider sectionProvider) {
        mSectionIndexNames.clear();
        mSectionAbsolutePosition.clear();
        if (sectionProvider == null) {
            return;
        }
        for (int i = 0, l = sectionProvider.size(); i < l; i++) {
            SectionProvider.SectionItem item = (SectionProvider.SectionItem) sectionProvider.get(i);
            Section section = (Section) item.getData();
            if (section.index) {
                mSectionIndexNames.add(section.shortTitle);
                mSectionAbsolutePosition.add(item.getAbsolutePosition());
            }
        }
    }
    
    public Section getSection(int position) {
        SectionProvider sectionProvider = getMasterList().getSectionProvider();
        if (sectionProvider == null) {
            return null;
        }
        Item item = getItem(position);
        if (item instanceof SectionProvider.SectionItem) {
            Section section = (Section) (item).getData();
            if (section.index) {
                return section;
            } else {
                
                SectionProvider.SectionItem sectionItem = (SectionProvider.SectionItem) sectionProvider.get(item.getSectionIndex());
                if (sectionItem != null) {
                    return ((Section) sectionItem.getData());
                } else {
                    return null;
                }
            }
        } else if (item.getSectionIndex() < 0) {
            return null;
        } else {
//            Logger.d("position = %s, sectionPos = %s", position, item.getSectionIndex());
            
            SectionProvider.SectionItem sectionItem = (SectionProvider.SectionItem) sectionProvider.get(item.getSectionIndex());
            if (sectionItem != null) {
                return ((Section) sectionItem.getData());
            } else {
                return null;
            }
        }
    }
    
    public ItemClickListener getOnItemClickListener() {
        return mOnItemClickListener;
    }
    
    public void setOnItemClickListener(ItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }
    
    //region Selection
    public boolean isSelectionMode() {
        return mSelectionMode;
    }
    
    public void setSelectionMode(boolean selectionMode) {
        if (selectionMode == mSelectionMode) {
            return;
        }
        if (selectionMode) {
            if (mSelectedData == null)
                mSelectedData = new HashSet<>();
        } else {
            if (mSelectedData != null)
                mSelectedData.clear();
        }
        mSelectionMode = selectionMode;
        notifyDataSetChanged();
    }
    
    public void toggleSelect(Item item) {
        if (!mSelectionMode) {
            return;
        }

        // TODO beware of screen orientation. Maybe keep selected positions instead?
        if (item instanceof DataProvider.DataItem) {
            DataProvider.DataItem dataItem = (DataProvider.DataItem) item;
            if (mSelectedData.contains(dataItem.getData().getId())) {
                mSelectedData.remove(dataItem.getData().getId());
            } else {
                mSelectedData.add(dataItem.getData().getId());
            }
        }
    }
    
    public void selectAll() {
        if (!mSelectionMode) {
            return;
        }
        
        MasterList list = getMasterList();
        for (int i = 0, l = list.size(); i < l; i++) {
            Item item = list.get(i);
            if (item != null && item instanceof DataProvider.DataItem) {
                DataProvider.DataItem dataItem = (DataProvider.DataItem) item;
                mSelectedData.add(dataItem.getData().getId());
            }
        }
        
        notifyDataSetChanged();
    }
    
    public void deselectAll() {
        if (!mSelectionMode) {
            return;
        }
        mSelectedData.clear();
        notifyDataSetChanged();
    }
    
    public int getSelectionCount() {
        if (!mSelectionMode)
            return -1;
        return mSelectedData.size();
    }
    
    public Collection<Integer> getSelectedData() {
        if (!mSelectionMode)
            return null;
        return mSelectedData;
    }
    
    //endregion
    
    @Override
    public void registerAdapterDataObserver(RecyclerView.AdapterDataObserver observer) {
        try {
            super.registerAdapterDataObserver(observer);
        } catch (Exception e) {
            
        }
    }
    
    @Override
    public void unregisterAdapterDataObserver(RecyclerView.AdapterDataObserver observer) {
        try {
            super.unregisterAdapterDataObserver(observer);
        } catch (Exception e) {
            
        }
    }
    
    public boolean isTypeAd(int viewType){
        return viewType >> 10 == MasterList.TYPE_AD;
    }
    
    public int getAdType(int viewType){
        return viewType & 0x03FF;
    }
}
