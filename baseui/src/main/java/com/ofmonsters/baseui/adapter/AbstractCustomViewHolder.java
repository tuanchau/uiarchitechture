package com.ofmonsters.baseui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by tuanchauict on 10/25/16.
 */

public abstract class AbstractCustomViewHolder<T> extends RecyclerView.ViewHolder {
    public AbstractCustomViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void render(T data);
}
