package com.ofmonsters.baseui.adapter;

import android.content.Context;


/**
 * Created by tuanchauict on 12/2/15.
 */
@Deprecated
public abstract class AdEntry {
    protected boolean mFocused = false;


    public void destroy(){
    }

    public boolean isFocused() {
        return mFocused;
    }

    public void setFocused(boolean focused) {
        mFocused = focused;
    }

    // Tracking impression
    public void gotFocus() {
        mFocused = true;
    }

    public String getIdentifyString() {
        return "" + this.hashCode();
    }

    public int getPriority() {
        return 0;
    }

    public void render(AbstractAdHolder holder) {
    }

    public void releaseAd(){
    }

    public String getAppLink() {
        return null;
    }

    public abstract void setContext(Context context);

    public boolean shouldCache(){
        return true;
    }
}
