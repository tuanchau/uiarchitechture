package com.ofmonsters.baseui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ofmonsters.baseui.list.Section;

/**
 * Created by tuanchauict on 10/22/16.
 */

public abstract class AbstractSectionViewHolder extends RecyclerView.ViewHolder {
    public AbstractSectionViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void render(Section section);
}
