package com.ofmonsters.baseui.utils;

import android.content.Context;
import android.content.res.TypedArray;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tuanchauict on 10/24/16.
 */

public class ResourceUtils {
    private static Map<Integer, Object> sResources = new HashMap<>();

    public static float getDimension(Context context, int resId, float defaultValue) {
        if (sResources.containsKey(resId)) {
            return (float) sResources.get(resId);
        } else {
            TypedArray a = context.obtainStyledAttributes(new int[]{resId});
            float value = a.getDimension(0, defaultValue);
            sResources.put(resId, value);
            a.recycle();
            return value;
        }
    }

    public static float getDimension(Context context, int resId, int defaultResId) {
        if (sResources.containsKey(resId)) {
            return (float) sResources.get(resId);
        } else {
            float defaultValue = context.getResources().getDimension(defaultResId);
            TypedArray a = context.obtainStyledAttributes(new int[]{resId});
            float value = a.getDimension(0, defaultValue);
            sResources.put(resId, value);
            a.recycle();
            return value;
        }
    }

    public static String getString(Context context, int resId) {
        if (sResources.containsKey(resId)) {
            return (String) sResources.get(resId);
        } else {
            TypedArray a = context.obtainStyledAttributes(new int[]{resId});
            String value = a.getString(0);
            a.recycle();
            sResources.put(resId, value);
            return value;
        }
    }
}
