package com.ofmonsters.baseui.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;

/**
 * Created by tuanchauict on 6/3/16.
 */
public class ScreenUtils {
    private static final DisplayMetrics DISPLAY_METRICS = new DisplayMetrics();
    
    public static int getWidthPixels(Activity context){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            context.getWindowManager().getDefaultDisplay().getRealMetrics(DISPLAY_METRICS);
        } else {
            context.getWindowManager().getDefaultDisplay().getMetrics(DISPLAY_METRICS);
        }
        return DISPLAY_METRICS.widthPixels;
    }

    public static int getHeightPixels(Activity context){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            context.getWindowManager().getDefaultDisplay().getRealMetrics(DISPLAY_METRICS);
        } else {
            context.getWindowManager().getDefaultDisplay().getMetrics(DISPLAY_METRICS);
        }
        return DISPLAY_METRICS.heightPixels;
    }

    public static int getOrientation(Context context){
        return context.getResources().getConfiguration().orientation;
    }


}
