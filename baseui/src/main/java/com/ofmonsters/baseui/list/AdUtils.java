package com.ofmonsters.baseui.list;


import java.util.Random;

/**
 * Created by tuanchauict on 12/1/15.
 */
public class AdUtils {
//    private static int rangeBegin = AdConfig.getGridCatalogNativeRangeBegin();
//    private static int rangeEnd = AdConfig.getGridCatalogNativeRangeEnd();

    private static Random mRandom = new Random(1000L);

    public static void resetRandom(){
        mRandom = new Random(1000L);
    }

//    public static int nextAdStep(int firstPlace){
//        if(firstPlace == 0){
//            resetRandom();
//        }
//        int rand = randomBetween(rangeBegin, rangeEnd);
//        Logger.d("first place = %s, [%s, %s], -> rand = %s", firstPlace, rangeBegin, rangeEnd, rand);
//        return rand;
//    }

    public static int nextAdStep(int firstPlace, int begin, int end){
        if(firstPlace == 0){
            resetRandom();
        }
        int rand = randomBetween(begin, end);
//        Logger.d("first place = %s, [%s, %s], -> rand = %s", firstPlace, begin, end, rand);
        return rand;
    }

    public static int randomBetween(int begin, int end){
        int rand = Math.abs(mRandom.nextInt());
        return begin + (rand % (end - begin + 1));
    }
}
