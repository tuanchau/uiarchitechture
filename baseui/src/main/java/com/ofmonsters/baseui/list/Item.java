package com.ofmonsters.baseui.list;

/**
 * Created by tuanchauict on 9/15/16.
 */ //custom section type must be > 0
public abstract class Item<T> {
    private int mColumn;
    private int mSpanSize;
    private int mSectionIndex;

    public Item(int column, int spanSize) {
        mColumn = column;
        this.mSpanSize = spanSize;
        mSectionIndex = -1;
    }

    public Item(int column, int spanSize, int sectionIndex) {
        mColumn = column;
        mSpanSize = spanSize;
        mSectionIndex = sectionIndex;
    }

    public int getColumn() {
        return mColumn;
    }

    public int getSpanSize() {
        return mSpanSize;
    }

    public void setColumn(int column) {
        mColumn = column;
    }

    public void setSpanSize(int spanSize) {
        this.mSpanSize = spanSize;
    }

    public int getSectionIndex() {
        return mSectionIndex;
    }

    public void setSectionIndex(int sectionIndex) {
        mSectionIndex = sectionIndex;
    }

    // TODO Item should be paramterized by the data type.
    public abstract T getData();

//    // TODO in general, should be a hashable object. for now we use int for more compact memory usage.
//    public abstract int getDataId();

    public abstract int getType();

    @Override
    public String toString() {
        return String.format("[%d, %d, %d, %s]", mColumn, mSpanSize, mSectionIndex, getData());
    }
}
