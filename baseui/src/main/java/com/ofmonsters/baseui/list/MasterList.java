package com.ofmonsters.baseui.list;

import android.support.annotation.Nullable;

import com.orhanobut.logger.LLogger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by tuanchauict on 9/11/16.
 */
public class MasterList {
    private static final LLogger LOG = LLogger.getLocalLogger().withTag("MASTERLIST").on();
    
    public static final int TYPE_SECTION = -1;
    public static final int TYPE_AD = -2;
    public static final int TYPE_ITEM = -3;
    public static final int TYPE_FOOTER = -4;
    
    private static MasterList sEmptyList;
    
    public static MasterList getEmptyList(){
        if(sEmptyList == null){
            sEmptyList = new MasterList();
        }
        return sEmptyList;
    }
    
    private List<AbstractProvider> mProviders = new ArrayList<>();
    private List<Integer> mProviderIndexes = new ArrayList<>(50);
    private List<Integer> mOffsetIndexes = new ArrayList<>(50);
    private List<Item> mItems = new ArrayList<>(1000);
    
    private SectionProvider mSectionProvider;
    
    
    public void add(int providerIndex, int offset) {
        mProviderIndexes.add(providerIndex);
        mOffsetIndexes.add(offset);
    }
    
    @Nullable
    public Item get(int position) {
//        if(position >= mProviderIndexes.size())
//            return null;
        int pIndex = mProviderIndexes.get(position);
        int offset = mOffsetIndexes.get(position);
        AbstractProvider provider = mProviders.get(pIndex);
        if (provider.isValid(offset)) {
            
            Item item = provider.get(offset);
            if (item == null) {
                LOG.w("[%s] pIndex = %s  offset = %s", hashCode(), pIndex, offset);
            } else {
//                LOG.d("[%s] pIndex = %s  offset = %s", hashCode(),pIndex, offset);
            }
            return item;
        } else {
            LOG.d("get item: pos = %s valid = %s, provider = %s pIndex = %s", position, provider.isValid(pIndex), provider.getClass().getSimpleName(), pIndex);
            
            return null;
        }
    }
    
    public boolean remove(int position) {
        if (position < 0 || position >= mProviderIndexes.size())
            return false;
        Integer pIndex = mProviderIndexes.get(position);
        Integer offset = mOffsetIndexes.get(position);
        AbstractProvider provider = mProviders.get(pIndex);
        return provider.remove(offset);
    }
    
    public void setProviders(List<AbstractProvider> providers) {
        mProviders = providers;
        mSectionProvider = null;
        for (AbstractProvider provider : providers) {
            if (provider instanceof SectionProvider) {
                mSectionProvider = (SectionProvider) provider;
                break;
            }
        }
        mProviderIndexes.clear();
        mOffsetIndexes.clear();
    }
    
    
    @Nullable
    public SectionProvider getSectionProvider() {
        return mSectionProvider;
    }
    
    public int size() {
        return mOffsetIndexes.size();
    }
    
    /**
     * @param oldMasterList to prevent close shared cursors
     */
    public void release(MasterList oldMasterList) {
        Set<AbstractProvider> set = new HashSet<>();
        if (oldMasterList != null && oldMasterList.mProviders != null) {
            for (AbstractProvider p : oldMasterList.mProviders) {
                set.add(p);
            }
        }
        for (AbstractProvider provider : mProviders) {
            if (!set.contains(provider))
                provider.close();
        }
    }
}
