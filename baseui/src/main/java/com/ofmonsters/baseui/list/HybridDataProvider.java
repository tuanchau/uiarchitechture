package com.ofmonsters.baseui.list;

import com.orhanobut.logger.LLogger;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by tuanchauict on 10/3/16.
 */

public class HybridDataProvider extends DataProvider {
    private static final LLogger LOG = LLogger.getLocalLogger().withTag("HYBRIDDATAPROVIDER").on();
    private TreeMap<Integer, DataProvider> mTreeMap;

    private int mLength;

    public HybridDataProvider() {
        mTreeMap = new TreeMap<>();
        mLength = 0;
    }

    public HybridDataProvider(Collection<DataProvider> providers) {
        mTreeMap = new TreeMap<>();
        mLength = 0;
        addAll(providers);
    }

    public void add(DataProvider provider) {
        mTreeMap.put(mLength, provider);
        mLength += provider.size();
    }

    public void addAll(Collection<DataProvider> providers) {
        for (DataProvider p : providers)
            add(p);
    }

    @Override
    public void clearItems() {
        for (DataProvider provider : mTreeMap.values())
            provider.clearItems();
    }

    @Override
    public void setItem(int index, int column, int sectionIndex) {
        Map.Entry<Integer, DataProvider> entry = mTreeMap.floorEntry(index);
        entry.getValue().setItem(index - entry.getKey(), column, sectionIndex);
    }

    @Override
    public int size() {
        return mLength;
    }

    @Override
    public Item get(int index) {
        Map.Entry<Integer, DataProvider> entry = mTreeMap.floorEntry(index);
        Item item = entry.getValue().get(index - entry.getKey());
        if(item == null){
            DataProvider dp = entry.getValue();
            for(int i = 0; i < dp.size(); i++){
                LOG.d("entry#%s = %s", i, dp.get(i));
            }
            LOG.d("size = %s, valid = %s", dp.size(), dp.isValid(index - entry.getKey()));
            LOG.d("entry = %s, offset = %s", index, index - entry.getKey());
        }
        return item;
    }

    @Override
    public void close() {
        for (DataProvider provider : mTreeMap.values()) {
            provider.close();
        }
    }

    public void clear(){
        clearItems();
        close();
        mTreeMap.clear();
        mLength = 0;
    }
}
