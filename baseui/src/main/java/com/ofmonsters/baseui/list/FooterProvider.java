package com.ofmonsters.baseui.list;

/**
 * Created by tuanchauict on 9/15/16.
 */
public class FooterProvider extends AbstractProvider {
    public static abstract class FooterItem extends Item{
        private Object mData;
        public FooterItem(int column, int spanSize, Object data) {
            super(column, spanSize);
            mData = data;
        }

        public FooterItem(int column, int spanSize, int sectionIndex, Object data) {
            super(column, spanSize, sectionIndex);
            mData = data;
        }
        @Override
        public int getType() {
            return MasterList.TYPE_FOOTER;
        }

        @Override
        public Object getData() {
            return mData;
        }

        public void setData(Object data) {
            mData = data;
        }
    }
    public static class FlexColumnFooterItem extends FooterItem {
        public FlexColumnFooterItem(int column, int spanSize, Object data) {
            super(column, spanSize, data);
        }

        @Override
        public String toString() {
            return "Flex Footer " + getColumn() + " " + getSpanSize();
        }
    }

    public static class FixColumnFooterItem extends FooterItem {
        public FixColumnFooterItem(int column, int spanSize, Object data) {
            super(column, spanSize, data);
        }

        @Override
        public void setColumn(int column) {
        }

        @Override
        public String toString() {
            return "Fix Footer" + getColumn() + " " + getSpanSize();
        }
    }



    private Item mFooterItem;

    public FooterProvider(FooterItem footerItem) {
        mFooterItem = footerItem;
    }

    @Override
    public Item get(int index) {
        return mFooterItem;
    }

    @Override
    public void close() {

    }
}
