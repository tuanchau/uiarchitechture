package com.ofmonsters.baseui.list;

import java.util.Arrays;
import java.util.List;

/**
 * Created by tuanchauict on 9/11/16.
 */
public class ArrayItemProvider<T extends Identifiable> extends DataProvider {
    public static class ArrayItem<T extends Identifiable> extends DataItem<T> {
        private T mData;

        public ArrayItem(T data, int column, int sectionIndex) {
            super(column, 1, sectionIndex);
            mData = data;
        }

        @Override
        public int getType() {
            return MasterList.TYPE_ITEM;
        }

        @Override
        public T getData() {
            return mData;
        }

//        @Override
//        public int getDataId() {
//            return mData.getId();
//        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    private Item<T>[] mItems;
    private List<T> mData;

    ArrayItemProvider(List<T> data) {
        mData = data;
        mItems = new Item[mData.size()];
    }

    @Override
    public void clearItems() {
        Arrays.fill(mItems, null);
    }

    @Override
    public void setItem(int index, int column, int sectionIndex) {
        mItems[index] = new ArrayItem<>(mData.get(index), column, sectionIndex);
    }

    @Override
    public int size() {
        return mData.size() - getNoOfRemoved();
    }

    @Override
    public Item<T> get(int index) {
        return mItems[index];
    }

    public T getData(int row) {
        return mData.get(row);
    }

    @Override
    public void close() {
    }

    @Override
    protected AbstractProvider cloneHelper() {
        return new ArrayItemProvider<>(mData);
    }
}
