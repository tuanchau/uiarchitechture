package com.ofmonsters.baseui.list;

import android.database.Cursor;

import java.util.List;

/**
 * Created by tuanchauict on 9/12/16.
 */
public abstract class DataProvider extends AbstractProvider {
    public static <T extends Identifiable> DataProvider newProvider(List<T> list) {
        return new ArrayItemProvider<>(list);
    }

    public static <T extends Identifiable> DataProvider newProvider(List<T> list, String name) {
        DataProvider provider = new ArrayItemProvider<>(list);
        provider.setName(name);
        return provider;
    }
    
    public static <T extends Identifiable> CursorItemProvider<T> newProvider(Cursor cursor,
                                                        CursorItemProvider.DataExtractor<T> dataExtractor) {
        return new CursorItemProvider<>(cursor, dataExtractor);
    }

    public static <T extends Identifiable> CursorItemProvider<T> newProvider(Cursor cursor,
                                                        CursorItemProvider.DataExtractor<T> dataExtractor,
                                                        String name) {
        CursorItemProvider<T> provider = new CursorItemProvider<>(cursor, dataExtractor);
        provider.setName(name);
        return provider;
    }
    
    public static abstract class DataItem<T extends Identifiable> extends Item<T> {

        public DataItem(int column, int spanSize) {
            super(column, spanSize);
        }

        public DataItem(int column, int spanSize, int sectionIndex) {
            super(column, spanSize, sectionIndex);
        }

        public abstract T getData();
    }

    /**
     * reset all items in array to null
     */
    public abstract void clearItems();
    public abstract void setItem(int index, int column, int sectionIndex);

//
//    public abstract T   /**
//     * Get object of array list or parse data from cursor
//     * @param item
//     * @return
//     */getData(Item item);

    public abstract int size();


}
