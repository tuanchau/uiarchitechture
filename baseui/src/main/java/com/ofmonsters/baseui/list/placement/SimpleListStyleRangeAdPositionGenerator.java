package com.ofmonsters.baseui.list.placement;

import com.ofmonsters.baseui.list.AdUtils;

/**
 * Created by tuanchauict on 10/14/16.
 * This is just a sample of position generator. Best in the app level instead of library level.
 */
public class SimpleListStyleRangeAdPositionGenerator implements AdPositionGenerator {
    private int mRangeBegin;
    private int mRangeEnd;
    private int mFirstPost;
    private int mAdCount;
    private int mAdType;

    public SimpleListStyleRangeAdPositionGenerator(int rangeBegin, int rangeEnd, int firstPost, int adType) {
        mRangeBegin = rangeBegin;
        mRangeEnd = rangeEnd;
        mFirstPost = firstPost;
        mAdCount = 0;
        mAdType = adType;
    }

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public AdPosition next(int nextItemPos, int nextItemCol, int columnCount) {
        int result = AdUtils.nextAdStep(mAdCount, mRangeBegin, mRangeEnd);
        if (mAdCount == 0 && mFirstPost >= 0 && mFirstPost < result) {
            result = mFirstPost;
        }
//        mAdCount += 1;
        return new AdPosition(columnCount, 0, nextItemPos + result * columnCount, mAdType);
    }
    
    @Override
    public void confirmUsed(AdPosition adPosition) {
        mAdCount += 1;
    }
    
    @Override
    public void reset() {
        mAdCount = 0;
    }
}
