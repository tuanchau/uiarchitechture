package com.ofmonsters.baseui.list.placement;

/**
 * Created by tuanchauict on 10/14/16.
 */
public interface AdPositionGenerator {
    boolean hasNext();

    AdPosition next(int nextItemPos, int nextItemCol, int columnCount);
    
    /**
     * AdPosition created from next is not sure to be added to master list.
     */
    void confirmUsed(AdPosition adPosition);

//    int adColumn();
//
//    int adSpan();

    void reset();
}
