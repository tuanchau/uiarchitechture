package com.ofmonsters.baseui.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tuanchauict on 9/11/16.
 */
public class AdProvider extends AbstractProvider {

    public static class AdItem extends Item {
        private int mAdType;
        public AdItem(int column, int spanSize, int sectionIndex, int adType) {
            super(column, spanSize, sectionIndex);
            mAdType = adType;
        }

        @Override
        public int getType() {
            return MasterList.TYPE_AD;
        }
        
        public int getAdType(){
            return mAdType;
        }

        @Override
        public Object getData() {
            return null;
        }

        @Override
        public String toString() {
            return "Ad: " + super.toString();
        }
    }

    private List<Item> mAds = new ArrayList<>();
    
    public int addAd(int spanSize, int column, int sectionIndex, int adType) {
        mAds.add(new AdItem(column, spanSize, sectionIndex, adType));
        return mAds.size() - 1;
    }

    @Override
    public Item get(int index) {
        return mAds.get(index);
    }

    @Override
    public void close() {
    }
}
