package com.ofmonsters.baseui.list.placement;

import java.util.List;

/**
 * Created by tuanchauict on 10/14/16.
 * This is just a sample of position generator. Best in the app level instead of library level.
 */
public class SimpleListStyleArrayAdPositionGenerator implements AdPositionGenerator {
    private List<Integer> mPositions;
    private int mCurrentPos = 0;
    
    public SimpleListStyleArrayAdPositionGenerator(List<Integer> positions) {
        mPositions = positions;
        for (int i = mPositions.size() - 1; i > 0; i--) {
            mPositions.set(i, mPositions.get(i) - mPositions.get(i - 1));
        }
    }
    
    @Override
    public boolean hasNext() {
        return mCurrentPos < mPositions.size();
    }
    
    @Override
    public AdPosition next(int nextItemPos, int nextItemCol, int columnCount) {
        if (hasNext()) {
            int last = mCurrentPos == 0 ? 0 : mPositions.get(mCurrentPos - 1);
            int next = mPositions.get(mCurrentPos);
            
//            mCurrentPos += 1;
            return new AdPosition(columnCount, 0, (next - last) * columnCount, 0);
        } else {
            return AdPosition.NO_MORE_ADS;
        }
    }
    
    @Override
    public void confirmUsed(AdPosition adPosition) {
        mCurrentPos += 1;
    }
    
    @Override
    public void reset() {
        mCurrentPos = 0;
    }
}
