package com.ofmonsters.baseui.list.placement;

import com.ofmonsters.baseui.list.AdUtils;

/**
 * Created by tuanchauict on 5/9/17.
 */

public class SimpleItemStyleRangeAdPositionGenerator implements AdPositionGenerator {
    private int mRangeBegin;
    private int mRangeEnd;
    private int mFirstPost;
    private int mAdCount;
    private int mAdColumn;
    private int mAdType;
    
    public SimpleItemStyleRangeAdPositionGenerator(int rangeBegin, int rangeEnd, int firstPost, int column, int adType) {
        mRangeBegin = rangeBegin;
        mRangeEnd = rangeEnd;
        mFirstPost = firstPost;
        mAdColumn = column;
        mAdType = adType;
    }
    
    @Override
    public boolean hasNext() {
        return true;
    }
    
    @Override
    public AdPosition next(int nextItemPos, int nextItemCol, int columnCount) {
        int rows = AdUtils.nextAdStep(mAdCount, mRangeBegin, mRangeEnd);
        if (mAdCount == 0 && mFirstPost >= 0 && mFirstPost < rows) {
            rows = mFirstPost;
        }
        
        int adColumn = mAdColumn < 0 || mAdColumn >= columnCount ? AdUtils.randomBetween(0, columnCount - 1) : mAdColumn;
        int pos = nextItemPos + rows * columnCount - nextItemCol + adColumn;
        
        return new AdPosition(1, adColumn, pos, mAdType);
    }
    
    @Override
    public void confirmUsed(AdPosition adPosition) {
        mAdCount += 1;
    }
    
    @Override
    public void reset() {
        mAdCount = 0;
    }
}
