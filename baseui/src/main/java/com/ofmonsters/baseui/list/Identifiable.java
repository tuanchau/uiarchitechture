package com.ofmonsters.baseui.list;

/**
 * Created by vuhoang on 4/21/17.
 */
// TODO should be parameterized by ID type
public interface Identifiable {
    int getId();
}
