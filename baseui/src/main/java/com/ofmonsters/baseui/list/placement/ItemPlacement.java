package com.ofmonsters.baseui.list.placement;

import com.ofmonsters.baseui.list.AbstractProvider;
import com.ofmonsters.baseui.list.AdProvider;
import com.ofmonsters.baseui.list.DataProvider;
import com.ofmonsters.baseui.list.FooterProvider;
import com.ofmonsters.baseui.list.MasterList;
import com.ofmonsters.baseui.list.Section;
import com.ofmonsters.baseui.list.SectionProvider;
import com.orhanobut.logger.LLogger;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by tuanchauict on 9/11/16.
 */
public class ItemPlacement {
    private static final LLogger LOG = LLogger.getLocalLogger().withTag("ITEMPLACEMENT").off();
    
    
    public static Observable<MasterList> generate(List<DataProvider> dataProviders,
                                                  List<Section> sections, int columnCount,
                                                  AdPositionGenerator adGenerator) {
        return generate(dataProviders, sections, columnCount, adGenerator, null);
    }
    
    public static Observable<MasterList> generate(List<DataProvider> dataProviders,
                                                  List<Section> sections, int columnCount,
                                                  AdPositionGenerator adGenerator,
                                                  FooterProvider.FooterItem footerItem) {
        return Observable.create(subscriber -> {
            if (subscriber.isUnsubscribed())
                return;
            cleanDataProvider(dataProviders);
            if ((dataProviders == null || dataProviders.isEmpty())
                    && (sections == null || sections.isEmpty())
                    && footerItem == null) {
                subscriber.onNext(new MasterList());
                subscriber.onCompleted();
                return;
            }
            
            if (sections == null || sections.isEmpty()) {
                subscriber.onNext(generateWithoutSection(dataProviders, columnCount, adGenerator, footerItem));
            } else {
                subscriber.onNext(generateWithinSection(dataProviders, sections, columnCount, adGenerator, footerItem));
            }
            subscriber.onCompleted();
        });
    }
//
//    public static Observable<MasterList> generate(DataProvider dataProvider, List<Section> sections,
//                                                  int columnCount, AdPositionGenerator adGenerator) {
//        return generate(dataProvider, sections, columnCount, adGenerator, null);
//    }
//
//    public static Observable<MasterList> generate(DataProvider dataProvider, List<Section> sections,
//                                                  int columnCount, AdPositionGenerator adGenerator,
//                                                  FooterProvider.FooterItem footerItem) {
//        return Observable.create(subscriber -> {
//            if (subscriber.isUnsubscribed()) {
//                return;
//            }
//            if ((dataProvider == null || dataProvider.size() == 0)
//                    && (sections == null || sections.isEmpty())
//                    && footerItem == null) {
//                subscriber.onNext(new MasterList());
//                subscriber.onCompleted();
//                return;
//            }
//
//            if (sections == null || sections.isEmpty()) {
//                subscriber.onNext(generateWithoutSection(dataProvider, columnCount, adGenerator, footerItem));
//            } else {
//                subscriber.onNext(generateWithinSection(dataProvider, sections, columnCount,
//                        adGenerator, footerItem));
//            }
//            subscriber.onCompleted();
//        });
//    }
    
    private static final AdPositionGenerator sNoAdGenerator = new AdPositionGenerator() {
        @Override
        public boolean hasNext() {
            return false;
        }
        
        @Override
        public AdPosition next(int nextItemPos, int nextItemCol, int columnCount) {
            return AdPosition.NO_MORE_ADS;
        }
        
        @Override
        public void confirmUsed(AdPosition adPosition) {
        }
        
        @Override
        public void reset() {
            
        }
    };

//    @VisibleForTesting
//    private static MasterList generateWithinSection(DataProvider dataProvider,
//                                                    @NonNull List<Section> sections, int columnCount,
//                                                    AdPositionGenerator adGenerator,
//                                                    FooterProvider.FooterItem footerItem) {
//        if (adGenerator == null) {
//            adGenerator = sNoAdGenerator;
//        }
//
//        adGenerator.reset();
//        int adStep = 0;
//        int adStop = adGenerator.next();
//        int lastColIndex = columnCount - 1;
//        int dataIndex = 0;
//        int curSectionIndex = -1;
//
//        AdProvider adProvider = new AdProvider(columnCount);
//        SectionProvider sectionProvider = new SectionProvider(columnCount);
//        MasterList masterList = new MasterList();
//        if (footerItem != null) {
//            masterList.setProviders(Arrays.asList(dataProvider, sectionProvider, adProvider, new FooterProvider(footerItem)));
//        } else {
//            masterList.setProviders(Arrays.asList(dataProvider, sectionProvider, adProvider));
//        }
//
//        //last item of section contains the left item
//        int itemCount = dataProvider.size();
//        for (int i = 0, l1 = sections.size() - 1; i < l1; i++) {
//            itemCount -= sections.get(i).itemCount;
//        }
//        sections.get(sections.size() - 1).itemCount = itemCount;
//        dataProvider.clearItems();
//
//        if (adStop == 0) {
//            int adIndex = adProvider.addAd(0, curSectionIndex);
//            masterList.add(2, adIndex);
//            adStop = adGenerator.next();
//        }
//
//        int realItemCount = 0;
//        boolean addedSection;
//
//        for (int i = 0, l = sections.size(); i < l; i++) {
//            Section section = sections.get(i);
//
//            if (!section.customView) {
//                curSectionIndex = i;
//            }
//            sectionProvider.addSection(section, masterList.size(), curSectionIndex);
//
//            realItemCount = 0;
//            adStep = 0;//TODO flexible ad step through sections
//            addedSection = false;
//            for (int j = 0, ll = section.itemCount, ll1 = ll - 1; j < ll; j++, dataIndex++) {
//                if (!dataProvider.isValid(dataIndex)) {
//                    continue;
//                }
//                if (!addedSection) {
//                    masterList.add(1, i);
//                    addedSection = true;
//                }
//
//                int col = realItemCount % columnCount;
////                System.out.println(j + " " + col + " " + realItemCount);
//                dataProvider.setItem(dataIndex, col, curSectionIndex);
//                masterList.add(0, dataIndex);
//
//                if (col == lastColIndex) {
//                    adStep++;
//                    if (adStep >= adStop && j < ll1) {
//                        int adIndex = adProvider.addAd(0, curSectionIndex);
//                        masterList.add(2, adIndex);
//                        adStop = adGenerator.next();
//                        adStep = 0;
//                    }
//                }
//                realItemCount++;
//            }
//            if (realItemCount == 0) {
//
//            }
//        }
//        if (footerItem != null) {
//            masterList.add(3, 0);
//            int col = realItemCount % columnCount;
//            if (footerItem.getSpanSize() + col > columnCount) {
//                footerItem.setColumn(0);
//            } else {
//                footerItem.setColumn(col);
//            }
//        }
//
//        return masterList;
//    }
//
//    @VisibleForTesting
//    private static MasterList generateWithoutSection(DataProvider dataProvider, int columnCount,
//                                                     AdPositionGenerator adGenerator,
//                                                     FooterProvider.FooterItem footerItem) {
//        if (adGenerator == null) {
//            adGenerator = sNoAdGenerator;
//        }
//        adGenerator.reset();
//        int adStep = 0;
//        int adStop = adGenerator.next();
//        int lastColIndex = columnCount - 1;
//
//        AdProvider adProvider = new AdProvider(columnCount);
//        MasterList masterList = new MasterList();
//        if (footerItem != null) {
//            masterList.setProviders(Arrays.asList(dataProvider, adProvider,
//                    new FooterProvider(footerItem)));
//        } else {
//            masterList.setProviders(Arrays.asList(dataProvider, adProvider));
//        }
//
//        int numberItems = dataProvider.size();
//        dataProvider.clearItems();
//        int realItemCount = 0;
//        if (adStop == 0) {
//            int adIndex = adProvider.addAd(0, -1);
//            masterList.add(1, adIndex);
//            adStop = adGenerator.next();
//        }
//        for (int i = 0, l1 = numberItems - 1; i < numberItems; i++) {
//            if (!dataProvider.isValid(i)) {
//                continue;
//            }
//            int col = realItemCount % columnCount;
//            dataProvider.setItem(i, col, -1);
//            masterList.add(0, i);
//
//            if (col == lastColIndex) {
//                adStep++;
//                if (adStep >= adStop && i < l1) {
//                    int adIndex = adProvider.addAd(0, -1);
//                    masterList.add(1, adIndex);
//                    adStop = adGenerator.next();
//                    adStep = 0;
//                }
//            }
//            realItemCount++;
//        }
//        if (footerItem != null) {
//            masterList.add(2, 0);
//            int col = realItemCount % columnCount;
//            if (footerItem.getSpanSize() + col > columnCount) {
//                footerItem.setColumn(0);
//            } else {
//                footerItem.setColumn(col);
//            }
//        }
//        return masterList;
//    }
    
    static MasterList generateWithinSection(List<DataProvider> dataProviders,
                                            List<Section> sections, int columnCount,
                                            AdPositionGenerator adGenerator,
                                            FooterProvider.FooterItem footerItem) {
        cleanDataProvider(dataProviders);
        
        adGenerator = adGenerator == null ? sNoAdGenerator : adGenerator;
        adGenerator.reset();

//        int adStep = 0;
        AdPosition adStop = adGenerator.next(0, 0, columnCount);
//        int lastColIndex = columnCount - 1;
        int curSectionIndex = -1;
        
        AdProvider adProvider = new AdProvider();
        SectionProvider sectionProvider = new SectionProvider(columnCount);
        MasterList masterList = new MasterList();
        int sectionPos = dataProviders.size();
        int adPos = sectionPos + 1;
        int footerPos = adPos + 1;
        List<AbstractProvider> providers = new ArrayList<>(dataProviders);
        providers.add(sectionProvider);
        providers.add(adProvider);
        if (footerItem != null)
            providers.add(new FooterProvider(footerItem));
        masterList.setProviders(providers);
        
        correctItemCountLastSection(dataProviders, sections);
        int nextItemColumn = 0;
        
        if (adStop.getPosition() == 0) {
            int adIndex = adProvider.addAd(adStop.getSpan(), adStop.getColumn(), curSectionIndex, adStop.getAdType());
            masterList.add(adPos, adIndex);
            nextItemColumn = adStop.getSpan() % columnCount;
            adGenerator.confirmUsed(adStop);
        }
        adStop = null;
//        int masterLength = 0;
        boolean addedSection;
        int currentProvider = 0;
        DataProvider dataProvider = dataProviders.get(currentProvider);
        int providerOffset = 0;
        int providerLength = dataProvider.size();
        for (int i = 0, l = sections.size(); i < l; i++) {
            Section section = sections.get(i);
            
            if (!section.customView)
                curSectionIndex = i;
            
            sectionProvider.addSection(section, masterList.size(), curSectionIndex);

//            masterLength = 0;
//            adStep = 0;
            addedSection = false;
            nextItemColumn = 0; //reset after each section

//            System.out.println("section = " + i + " sectionCount = " + section.itemCount);
            
            if (section.itemCount == 0) {
                masterList.add(sectionPos, i);
//                adStop.setPosition(adStop.getPosition() + 1);
            }
            
            for (int j = 0, ll = section.itemCount, ll1 = ll - 1; j < ll; j++) {
                if (adStop != null)
//                    LOG.d("adpos: %d - adspan: %d", adStop.getPosition(), adStop.getSpan());
                if (!dataProvider.isValid(providerOffset))
                    continue;
                if (!addedSection) {
                    masterList.add(sectionPos, i);
                    addedSection = true;
                    nextItemColumn = 0;
                    adStop = adGenerator.next(masterList.size(), nextItemColumn, columnCount); //FIXME reset or something like that
//                    adStop.setPosition(adStop.getPosition() + 1);
                }
                
                dataProvider.setItem(providerOffset, nextItemColumn, curSectionIndex);
                masterList.add(currentProvider, providerOffset);
                nextItemColumn = (nextItemColumn + 1) % columnCount;
//                    adStep++;
                if (masterList.size() >= adStop.getPosition() && j < ll1) {
                    int adIndex = adProvider.addAd(adStop.getSpan(), adStop.getColumn(), curSectionIndex, adStop.getAdType());
                    masterList.add(adPos, adIndex);
                    adGenerator.confirmUsed(adStop);
                    nextItemColumn = (nextItemColumn + adStop.getSpan()) % columnCount;
                    adStop = adGenerator.next(masterList.size(), nextItemColumn, columnCount);
//                        adStep = 0;
                }

//                masterLength++;
                
                providerOffset++;
                if (providerOffset >= providerLength) {
                    currentProvider++;
//                    System.out.println("---");
//                    System.out.println("curProvider = " + currentProvider);
//                    System.out.println("section = " + i);
//                    System.out.println("j = " + j + " end = " + ll);
//                    System.out.println("provider offset = " + providerOffset + "  provider length = " + providerLength);
                    if (currentProvider < dataProviders.size()) {
                        dataProvider = dataProviders.get(currentProvider);
                        providerLength = dataProvider.size();
                        providerOffset = 0;
                    }
                }
            }
            
        }
        
        
        if (footerItem != null) {
            masterList.add(footerPos, 0);
//            int col = masterLength % columnCount;
            if (footerItem.getSpanSize() + nextItemColumn > columnCount) {
                footerItem.setColumn(0);
            } else {
                footerItem.setColumn(nextItemColumn);
            }
        }
        return masterList;
    }
    
    static MasterList generateWithoutSection(List<DataProvider> dataProviders, int columnCount,
                                             AdPositionGenerator adGenerator, FooterProvider.FooterItem footerItem) {
        cleanDataProvider(dataProviders);
        adGenerator = adGenerator == null ? sNoAdGenerator : adGenerator;
        adGenerator.reset();

//        int adStep = 0;
//        int lastColIndex = columnCount - 1;
        
        AdProvider adProvider = new AdProvider();
        MasterList masterList = new MasterList();
        AdPosition adStop = adGenerator.next(0, 0, columnCount);
        int adPos = dataProviders.size();
        int footerPos = adPos + 1;
        List<AbstractProvider> providers = new ArrayList<>(dataProviders);
        providers.add(adProvider);
        if (footerItem != null)
            providers.add(new FooterProvider(footerItem));
        masterList.setProviders(providers);
        int nextItemColumn = 0;
        
        if (adStop.getPosition() == 0) {
            int adIndex = adProvider.addAd(adStop.getSpan(), adStop.getColumn(), -1, adStop.getAdType());
            masterList.add(adPos, adIndex);
            adGenerator.confirmUsed(adStop);
            nextItemColumn = adStop.getSpan() % columnCount;
            adStop = adGenerator.next(masterList.size(), nextItemColumn, columnCount);
        }
        
        for (int i = 0, l = dataProviders.size(); i < l; i++) {
            DataProvider dataProvider = dataProviders.get(i);
            dataProvider.clearItems();
            for (int j = 0, ll = dataProvider.size(); j < ll; j++) {
//                LOG.d("adpos: %d - adspan: %d", adStop.getPosition(), adStop.getSpan());
                if (!dataProvider.isValid(i)) {
                    continue;
                }
                dataProvider.setItem(j, nextItemColumn, -1);
                masterList.add(i, j);
                nextItemColumn = (nextItemColumn + 1) % columnCount;
                
                if (masterList.size() >= adStop.getPosition() && (i < l - 1 || j < ll - 1)) {
                    int adIndex = adProvider.addAd(adStop.getSpan(), adStop.getColumn(), -1, adStop.getAdType());
                    masterList.add(adPos, adIndex);
                    adGenerator.confirmUsed(adStop);
                    nextItemColumn = (adStop.getSpan() + nextItemColumn) % columnCount;
                    adStop = adGenerator.next(masterList.size(), nextItemColumn, columnCount);
                }
            }
        }
        
        if (footerItem != null) {
            masterList.add(footerPos, 0);
//            int col = realItemCount % columnCount;
            if (footerItem.getSpanSize() + nextItemColumn > columnCount) {
                footerItem.setColumn(0);
            } else {
                footerItem.setColumn(nextItemColumn);
            }
        }
        
        return masterList;
    }
    
    private static void correctItemCountLastSection(List<DataProvider> dataProviders, List<Section> sections) {
        int itemCount = 0;
        for (int i = 0, l = dataProviders.size(); i < l; i++) {
            DataProvider provider = dataProviders.get(i);
            itemCount += provider.size();
            provider.clearItems();
        }
        for (int i = 0, l = sections.size() - 1; i < l; i++) {
            Section section = sections.get(i);
            if (section.itemCount < 0)
                throw new IllegalStateException("Section's item count must be equal or greater than 0. Current is " + section.itemCount);
            if (itemCount < section.itemCount) {
                section.itemCount = itemCount;
                itemCount = 0;
            } else
                itemCount -= section.itemCount;
        }
        
        sections.get(sections.size() - 1).itemCount = itemCount;
    }
    
    private static void cleanDataProvider(List<DataProvider> dataProviders) {
        //remove null or empty data provider
        if (dataProviders == null)
            return;
        for (int i = dataProviders.size() - 1; i >= 0; i--) {
            DataProvider dataProvider = dataProviders.get(i);
            if (dataProvider == null || dataProvider.size() == 0)
                dataProviders.remove(i);
            
        }
        
    }
}
