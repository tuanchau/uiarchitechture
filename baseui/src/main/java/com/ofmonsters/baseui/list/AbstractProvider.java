package com.ofmonsters.baseui.list;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by tuanchauict on 10/14/16.
 */
public abstract class AbstractProvider {
    private Set<Integer> mRemovedItems = new HashSet<>();
    private AtomicInteger mInstanceCount = new AtomicInteger(-1);
    private String mName;

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public abstract Item get(int index);

    public boolean remove(Integer index) {
        return mRemovedItems.add(index);
    }

    public void recover(Integer index) {
        mRemovedItems.remove(index);
    }

    public boolean isValid(int index) {
        return !mRemovedItems.contains(index);
    }

    public int getNoOfRemoved() {
        return mRemovedItems.size();
    }

    public abstract void close();

    public final AbstractProvider clone() {
        if (mInstanceCount.getAndIncrement() > 0) {
            throw new IllegalStateException("Object cannot be cloned anymore. Only clone once");
        }

        if (mInstanceCount.get() < 0) {
            return this;
        }

        AbstractProvider abstractProvider = cloneHelper();
        abstractProvider.mRemovedItems = mRemovedItems;
        return abstractProvider;
    }

    protected AbstractProvider cloneHelper() {
        throw new UnsupportedOperationException();
    }
}
