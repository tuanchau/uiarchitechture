package com.ofmonsters.baseui.list;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.orhanobut.logger.LLogger;

import java.util.Arrays;

/**
 * Created by tuanchauict on 9/11/16.
 */
public class CursorItemProvider<T extends Identifiable> extends DataProvider {
    private static final LLogger LOG = LLogger.getLocalLogger().withTag("CURSORPROVIDER").on();

    public static class CursorItem<T extends Identifiable> extends DataItem<T> {
        private int mRow;
        private CursorItemProvider<T> mProvider;
        
        public CursorItem(CursorItemProvider<T> provider, int row, int column, int sectionIndex) {
            super(column, 1, sectionIndex);
            mRow = row;
            mProvider = provider;
        }
        
        @Override
        public int getType() {
            return MasterList.TYPE_ITEM;
        }
        
        @Override
        public T getData() {
            return mProvider.getData(mRow);
        }

//        @Override
//        public int getDataId() {
//            return getD
//        }

        public int getRow() {
            return mRow;
        }
    }
    
    public interface DataExtractor<T> {
        void initialize(@NonNull Cursor cursor);
        
        T extractData(@NonNull Cursor cursor);
    }
    
    private Item<T>[] mItems;
    private Cursor mCursor;
    private DataExtractor<T> mDataExtractor;
    private boolean mClosable = true;
    
    CursorItemProvider(@NonNull Cursor cursor, DataExtractor<T> extractor) {
        if(cursor == null)
            throw new NullPointerException("Cursor must not be null");
        mCursor = cursor;
        mDataExtractor = extractor;
        mDataExtractor.initialize(cursor);
        mItems = new Item[cursor.getCount()];
    }
    
    @Override
    public void clearItems() {
        Arrays.fill(mItems, null);
    }
    
    @Override
    public void setItem(int row, int column, int sectionIndex) {
        mItems[row] = new CursorItem<>(this, row, column, sectionIndex);
    }

    // Temporarily made public. TODO
    public T getData(int row) {
        mCursor.moveToPosition(row);
        return mDataExtractor.extractData(mCursor);
    }

    // TODO temporary only
    public Cursor getCursor() { return mCursor; }
    
    @Override
    public int size() {
        return mCursor.getCount() - getNoOfRemoved();
    }
    
    @Override
    public Item<T> get(int index) {
        return mItems[index];
    }
    
    @Override
    public void close() {
        if (!mClosable)
            return;
        if (mCursor != null && !mCursor.isClosed()) {
            try {
                mCursor.close();
            } catch (Exception e) {
                LOG.e(e, "cursor item provider close error");
            }
        }
    }
    
    @Override
    protected AbstractProvider cloneHelper() {
        this.mClosable = false;
        return new CursorItemProvider<>(mCursor, mDataExtractor);
    }
}
