package com.ofmonsters.baseui.list.placement;

/**
 * Created by tuanchauict on 5/8/17.
 */

public class AdPosition {
    private int mSpan;
    private int mColumn;
    private int mPosition;
    private int mAdType;
    
    public static final AdPosition NO_MORE_ADS = new AdPosition(0, 0, Integer.MAX_VALUE, 0);
    
    public AdPosition(int span, int column, int position, int adType) {
        mSpan = span;
        mColumn = column;
        mPosition = position;
        mAdType = adType;
    }
    
    public int getSpan() {
        return mSpan;
    }
    
    public void setSpan(int span) {
        mSpan = span;
    }
    
    public int getColumn() {
        return mColumn;
    }
    
    public void setColumn(int column) {
        mColumn = column;
    }
    
    public int getPosition() {
        return mPosition;
    }
    
    public void setPosition(int position) {
        mPosition = position;
    }
    
    public int getAdType() {
        return mAdType;
    }
    
    public void setAdType(int adType) {
        mAdType = adType;
    }
}
