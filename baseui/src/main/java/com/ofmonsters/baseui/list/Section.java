package com.ofmonsters.baseui.list;

/**
 * Created by tuanchauict on 11/5/15.
 */
public class Section {
    public int itemCount;
    public String title;
    public String shortTitle;
    public int type;
    public boolean customView;
    public Object data;
    public boolean index;

    /**
     * @param itemCount number item of section. Last section can be anything.
     * @param text
     */
    public Section(int itemCount, String text) {
        this.itemCount = itemCount;
        this.title = text;
        this.type = -1;
        this.customView = false;
        this.index = true;
    }

    public Section(int itemCount, String title, String shortTitle){
        this.itemCount = itemCount;
        this.title = title;
        this.type = -1;
        this.customView = false;
        this.shortTitle = shortTitle;
        this.index = true;
    }

    public Section(int itemCount, String title, int type) {
        this.itemCount = itemCount;
        this.title = title;
        this.type = type;
        this.customView = false;
        this.index = true;
    }

    public Section(){
    }

    public Section(boolean customView, int type, Object data){
        this.customView = customView;
        this.type = type;
        this.data = data;
        this.index = false;
    }

    public Section(boolean customView, int type, Object data, boolean index, String shortTitle){
        this.customView = customView;
        this.type = type;
        this.data = data;
        this.index = index;
        this.shortTitle = shortTitle;
    }

    public String toString(){
        return shortTitle == null ? title : shortTitle;
    }
}
