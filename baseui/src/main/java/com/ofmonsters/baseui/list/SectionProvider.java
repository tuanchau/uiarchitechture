package com.ofmonsters.baseui.list;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tuanchauict on 9/11/16.
 */
public class SectionProvider extends AbstractProvider {
    public static class SectionItem extends Item<Section> {
        private Section mSection;
        private int mAbsolutePosition;

        public SectionItem(Section section, int absolutePosition, int spanSize, int sectionIndex) {
            super(0, spanSize, sectionIndex);
            mSection = section;
            mAbsolutePosition = absolutePosition;
        }

        public Section getData() {
            return mSection;
        }

        @Override
        public int getType() {
            return mSection.customView ? mSection.type : MasterList.TYPE_SECTION;
        }

        public int getAbsolutePosition(){
            return mAbsolutePosition;
        }

        @Override
        public String toString() {
            return "Section: " + mSection.title;
        }
    }


    private int mSpanSize = 1;
    private List<Item> mSectionItems = new ArrayList<>();
    private int mIndexSectionCount = 0;
    private List<Section> mSections = new ArrayList<>();


    public SectionProvider(int spanSize) {
        mSpanSize = spanSize;
    }

    public void setSpanSize(int spanSize) {
        mSpanSize = spanSize;
        for (Item item : mSectionItems) {
            item.setSpanSize(spanSize);
        }
    }

    public void addSection(Section section, int absolutePosition, int sectionIndex) {
        mSectionItems.add(new SectionItem(section, absolutePosition, mSpanSize, sectionIndex));
        if (section.index) //only accept none custom view
            mIndexSectionCount++;
        mSections.add(section);
    }

    @NonNull
    public List<Section> getSections(){
        return mSections;
    }

    @Nullable
    @Override
    public Item get(int index) {
        if(index < 0 || index >= mSectionItems.size()){
            return null;
        }
        return mSectionItems.get(index);
    }

    public int getIndexSectionCount(){
        return mIndexSectionCount;
    }

    public int size(){
        return mSectionItems.size();
    }

    @Override
    public void close() {
        //do nothing
    }
}
