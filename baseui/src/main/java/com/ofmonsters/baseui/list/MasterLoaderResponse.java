package com.ofmonsters.baseui.list;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by tuanchauict on 9/19/16.
 */
public class MasterLoaderResponse {
    private boolean mShouldClear;
    private boolean mAddTop;
    private boolean mLastPage;
    private List<DataProvider> mDataProviders;
    
    /**
     * For single data provider
     *
     * @param dataProvider
     */
    public MasterLoaderResponse(DataProvider dataProvider) {
        this(true, true, true, dataProvider);
    }
    
    /**
     * For list of provider
     * @param dataProviders
     */
    public MasterLoaderResponse(List<DataProvider> dataProviders) {
        this(true, true, true, dataProviders);
    }
    
    public MasterLoaderResponse(boolean shouldClear, boolean addTop, boolean lastPage, DataProvider dataProvider) {
        mAddTop = addTop;
        mShouldClear = shouldClear;
        mDataProviders = Arrays.asList(dataProvider);
        mLastPage = lastPage;
    }
    
    public MasterLoaderResponse(boolean shouldClear, boolean addTop, boolean lastPage, List<DataProvider> dataProviders) {
        mAddTop = addTop;
        mShouldClear = shouldClear;
        mDataProviders = dataProviders;
        mLastPage = lastPage;
    }
    
    public boolean isShouldClear() {
        return mShouldClear;
    }
    
    public void setShouldClear(boolean shouldClear) {
        this.mShouldClear = shouldClear;
    }
    
    public boolean isAddTop() {
        return mAddTop;
    }
    
    public void setAddTop(boolean addTop) {
        mAddTop = addTop;
    }
    
    @NonNull
    public List<DataProvider> getDataProviders() {
        if(mDataProviders == null){
            return new ArrayList<>();
        }
        return mDataProviders;
    }
    
    public void setDataProviders(List<DataProvider> dataProviders) {
        mDataProviders = dataProviders;
    }
    
    public boolean isEmpty(){
        return mDataProviders == null || mDataProviders.isEmpty();
    }
    
    public boolean isLastPage() {
        return mLastPage;
    }
    
    public void setLastPage(boolean lastPage) {
        mLastPage = lastPage;
    }
}
