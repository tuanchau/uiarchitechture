package com.ofmonsters.baseui.listfragment;

/**
 * Created by tuanchauict on 10/2/16.
 */
@Deprecated
public class PagingHandler {
    private int mCurrentPage;
    private int mNextPage;

    public boolean shouldLoadNextpage(int totalItem, int bottomVisibleItem){
        return bottomVisibleItem > totalItem - 5;
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(int currentPage) {
        mCurrentPage = currentPage;
    }

    public int getNextPage() {
        return mNextPage;
    }

    public void setNextPage(int nextPage) {
        mNextPage = nextPage;
    }


}
