package com.ofmonsters.baseui.listfragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IdRes;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.ofmonsters.baseui.R;
import com.ofmonsters.baseui.utils.ResourceUtils;
import com.ofmonsters.baseui.utils.ScreenUtils;
import com.ofmonsters.baseui.view.RecentSearchView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tuanchauict on 9/26/16.
 */
@Deprecated
public abstract class BaseSearchableCatalogFragment extends BaseCatalogFragment implements SearchStringAction {
    protected static final int SEARCH_DELAY_INTERVAL = 500;

    private RecentSearchView mRecentSearchView;
    private String mSearchString;
    private SearchHandler mSearchHandler;
    private MenuItem mMenuItemSearch;

    private List<Boolean> mMenuItemsVisibleState;


    public void reloadData(String searchString) {
        mSearchString = TextUtils.isEmpty(searchString) ? null : searchString;
        reloadData();
    }


    @IdRes
    protected int getMenuSearchItemId() {
        return -1;
    }

    /**
     * @return true if allow search runs each character input
     */
    protected boolean allowInstantSearch() {
        return true;
    }

    @Override
    public void onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu();
        if (!isDetached())
            onSearchActionCollapseOrDestroy(null);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mMenuItemSearch != null) {
            ((SearchView) mMenuItemSearch.getActionView()).setMaxWidth(getResources().getDisplayMetrics().widthPixels);
        }
    }

    protected void setupSearchAction(Menu menu) {
        mMenuItemSearch = menu.findItem(getMenuSearchItemId());
        if (mMenuItemSearch == null)
            return;
        if (!shouldDisplaySearchAction()) {
            mMenuItemSearch.setVisible(false);
            return;
        }

        if (allowInstantSearch()) {
            mSearchHandler = new SearchHandler(this);
        }

        mMenuItemSearch.setVisible(true);
        SearchView searchView = (SearchView) mMenuItemSearch.getActionView();
        searchView.setQueryHint(ResourceUtils.getString(getContext(), R.attr.base_ui_search_text_box_hint_text));
        searchView.setMaxWidth(ScreenUtils.getWidthPixels(getActivity()));
        MenuItemCompat.setOnActionExpandListener(mMenuItemSearch, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                enableRefresh(false);
                showRecentSearch((SearchView) mMenuItemSearch.getActionView());
                hideAllMenuItems(menu);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onSearchActionCollapseOrDestroy(menu);
                return true;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                hideKeyboard(searchView);

                if (allowInstantSearch())
                    mSearchHandler.removeMessages(0);
                //TODO hide recycle view and show loading
                hideDataViews();
                reloadData(query);


                if (!TextUtils.isEmpty(query)) {
                    saveNewSearchString(query);
                }

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (!TextUtils.isEmpty(newText)) {
                    if (mRecentSearchView != null)
                        mRecentSearchView.hide();
                } else {
//                    if (searchView.isActivated())
                    if (mRecentSearchView == null) {
                        showRecentSearch((SearchView) mMenuItemSearch.getActionView());
                    } else if (mRecentSearchView.isEnabled()) {
                        mRecentSearchView.show();
                    }
                }
                if (!allowInstantSearch()) {
                    return true;
                }
                
                Message message = mSearchHandler.obtainMessage(0, newText);
                mSearchHandler.removeMessages(0);
                mSearchHandler.sendMessageDelayed(message, SEARCH_DELAY_INTERVAL);
//                    mAdapter.setShowingFooterView(newText.length() >= AppSettings.MINIMUM_CHARACTERS_TO_ENABLE_SEARCH_OTHERS_MODE);

//                mAdapter.setHasFooter(hasFooter() && newText != null && newText.length() > 0 && searchView.isEnabled());

                return true;
            }
        });
    }

    private void onSearchActionCollapseOrDestroy(Menu menu) {
        enableRefresh(true);
        hideRecentSearch();
        restoreVisibilityAllMenuItems(menu);
        mSearchString = null;
    }

    protected boolean shouldDisplaySearchAction() {
        return true;
    }

    @Override
    protected void onAdapterDataChanged() {
        super.onAdapterDataChanged();
        if (mMenuItemSearch != null) {
            mMenuItemSearch.setEnabled(getItemCount() > 0);
        }
    }

    public String getSearchString() {
        return mSearchString;
    }

    protected void hideRecentSearch() {
        if (mRecentSearchView == null) {
            return;
        }
        mRecentSearchView.hide();
        mRecentSearchView.setEnabled(false);
    }

    protected void showRecentSearch(SearchView searchView) {
        setupRecentSearchView();
        if (mRecentSearchView == null) {
            return;
        }
        mRecentSearchView.setSearchView(searchView);
        mRecentSearchView.show();
        mRecentSearchView.setEnabled(true);
    }

    private void setupRecentSearchView() {
        if (mRecentSearchView == null) {
//            mRecentSearchView = findLazyViewById(R.id.stub_recent_search_view, R.id.recent_search_view);
//            mRecentSearchView.setSearchStringAction(this);
        }
    }

    protected void hideAllMenuItems(Menu menu) {
        if (mMenuItemsVisibleState == null) {
            mMenuItemsVisibleState = new ArrayList<>();
        } else {
            mMenuItemsVisibleState.clear();
        }

        for (int i = 0; i < menu.size(); i++) {
            mMenuItemsVisibleState.add(menu.getItem(i).isVisible());
            menu.getItem(i).setVisible(false);
        }
    }

    protected void restoreVisibilityAllMenuItems(Menu menu) {
        if (mMenuItemsVisibleState == null || menu == null
                || menu.size() != mMenuItemsVisibleState.size()) {
            return;
        }

        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setVisible(mMenuItemsVisibleState.get(i));
        }
    }


    protected static class SearchHandler extends Handler {

        WeakReference<BaseSearchableCatalogFragment> mFragmentRef;

        public SearchHandler(BaseSearchableCatalogFragment fragment) {
            mFragmentRef = new WeakReference<>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            BaseSearchableCatalogFragment fragment = mFragmentRef.get();
            if (fragment != null && !fragment.isDetached() && !fragment.isRemoving()) {
                fragment.hideDataViews();
                fragment.reloadData((String) msg.obj);
            }
        }
    }
    
    public void hideKeyboard(View focusedView) {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
    }
}
