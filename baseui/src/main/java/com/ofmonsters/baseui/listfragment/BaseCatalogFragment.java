package com.ofmonsters.baseui.listfragment;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.notabasement.fastscrollrecyclerview.FastScrollListener;
import com.notabasement.fastscrollrecyclerview.FastScrollRecyclerView;
import com.ofmonsters.baseui.BaseFragment;
import com.ofmonsters.baseui.R;
import com.ofmonsters.baseui.adapter.AbstractMasterAdapter;
import com.ofmonsters.baseui.annotation.Visibility;
import com.ofmonsters.baseui.list.DataProvider;
import com.ofmonsters.baseui.list.FooterProvider;
import com.ofmonsters.baseui.list.Item;
import com.ofmonsters.baseui.list.MasterList;
import com.ofmonsters.baseui.list.MasterLoaderResponse;
import com.ofmonsters.baseui.list.Section;
import com.ofmonsters.baseui.list.SectionProvider;
import com.ofmonsters.baseui.list.placement.AdPositionGenerator;
import com.ofmonsters.baseui.utils.ResourceUtils;
import com.ofmonsters.baseui.utils.ScreenUtils;
import com.orhanobut.logger.LLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by tuanchauict on 9/18/16.
 */
@Deprecated
public abstract class BaseCatalogFragment extends BaseFragment
        implements FastScrollListener {
    
    
    private static final LLogger LOG = LLogger.getLocalLogger().withTag("CATALOG").on();
    private static final String EXTRA_LAST_TOP_POSITION = "last-top-pos";
    private static final String EXTRA_LAST_TOP_OFFSET = "last-top-offset";
    
    protected static final int LAZY_RECYCLER_INDEX = 0;
    protected static final int LAZY_STICK_SECTION_INDEX = 1;
    protected static final int LAZY_LOADING_INDEX = 2;
    protected static final int LAZY_HEADER_CONTAINER_INDEX = 3;
    protected static final int LAZY_FOOTER_INDEX = 4;
    protected static final int LAZY_ERROR_CONTAINER_INDEX = 5;
    protected static final int LAZY_EMPTY_CONTAINER_INDEX = 6;
    
    
    ProgressBar mLoadingView;
    SwipeRefreshLayout mRefreshLayout;
    FastScrollRecyclerView mRecyclerView;
    FrameLayout mStickSectionContainer;
    FrameLayout mHeaderContainer;
    FrameLayout mFooterContainer;
    
    FrameLayout mErrorStateViewContainer;
    FrameLayout mEmptyStateViewContainer;
    
    private List<LazyObject<View>> mLazyViews = Arrays.asList(
            () -> mRecyclerView,                //0
            () -> mStickSectionContainer,       //1
            () -> mLoadingView,                 //2
            () -> mHeaderContainer,             //3
            () -> mFooterContainer,             //4
            () -> mErrorStateViewContainer,     //5
            () -> mEmptyStateViewContainer      //6
    );
    
    
    private int mNumberColumns;
    
    private int mLastOrientation = -1;
    private int mLastTopPosition = -1; //top visible position
    private int mLastTopOffset = 0;
    
    private AbstractMasterAdapter mAdapter;
    
    private List<DataProvider> mDataProviders = new ArrayList<>();
    private AtomicBoolean mLoaded = new AtomicBoolean();
    
    private GridLayoutManager mGridLayoutManager;
    private MasterGridSpacingDecoration mSpacingDecoration;
    
    private int mCurrentPage;
    private boolean mHasNextPage;
    
    protected RecyclerView.AdapterDataObserver mAdapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            onAdapterDataChanged();
        }
    };
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(EXTRA_LAST_TOP_POSITION, mLastTopPosition);
        outState.putInt(EXTRA_LAST_TOP_OFFSET, mLastTopOffset);
    }
    
    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            mLastTopPosition = savedInstanceState.getInt(EXTRA_LAST_TOP_POSITION);
            mLastTopOffset = savedInstanceState.getInt(EXTRA_LAST_TOP_OFFSET);
        }
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = initAdapter();
    }
    
    @Override
    public void onDestroy() {
        releaseAdapter();
        super.onDestroy();
    }
    
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getViewLayoutId(), container, false);
        
        mLoadingView = findView(view, R.id.loading);
        mRefreshLayout = findView(view, R.id.swipe_refresh_layout);
        mRecyclerView = findView(view, R.id.recycler_view);
        mRecyclerView.setFastScrollView(findView(view, R.id.fast_scroll_view));
        
        mRefreshLayout.setEnabled(false);
        
        setupRecyclerView();
        setupRefreshLayout();
        
        if (!requiredReloadDataOnResume()) {
            loadData();
        }
        
        return view;
    }
    
    @Override
    public void onDestroyView() {
        if (mAdapter != null) {
            
            if (mAdapter.getMasterList() != null) {
                mAdapter.getMasterList().release(null);
            }
        }
        super.onDestroyView();
    }
    
    @Override
    public void onResume() {
        super.onResume();
//        clearSearchViewFocus();
        if (requiredReloadDataOnResume() && !mLoaded.get()) {
            loadData();
        }
    }
    
    public boolean requiredReloadDataOnResume() {
        return true;
    }
    
    @Override
    public void onPause() {
        super.onPause();
    }
    
    @LayoutRes
    protected abstract int getViewLayoutId();
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        saveLastTopPosition();
        List<Section> sections = null;
        if (mAdapter.getMasterList().getSectionProvider() != null)
            sections = mAdapter.getMasterList().getSectionProvider().getSections();
        int numColumns = getNumberColumns(ScreenUtils.getOrientation(getContext()));
        
        callProduceMasterList(mDataProviders, sections)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(masterList -> {
                    int space = (int) ResourceUtils.getDimension(getContext(),
                            R.attr.base_ui_catalog_item_space,
                            R.dimen.default_catalog_item_space);
                    
                    mSpacingDecoration.setInfo(numColumns,
                            ScreenUtils.getWidthPixels(getActivity()), space);
                    
                    mGridLayoutManager.setSpanCount(numColumns);
                    
                    this.setupAdapter(masterList, mDataProviders);
                });
        super.onConfigurationChanged(newConfig);
    }
    
    //region RecyclerView
    @CallSuper
    protected void setupRecyclerView() {
        int orientation = getOrientation();
        mRecyclerView.getRecycledViewPool()
                .setMaxRecycledViews(MasterList.TYPE_ITEM,
                        getNumberColumns(orientation) * getNumberRows(orientation));
        
        mRecyclerView.getRecycledViewPool()
                .setMaxRecycledViews(MasterList.TYPE_SECTION, 3);
        
        mRecyclerView.getRecycledViewPool()
                .setMaxRecycledViews(MasterList.TYPE_AD, 1);
        
        mRecyclerView.getRecycledViewPool()
                .setMaxRecycledViews(MasterList.TYPE_FOOTER, 1);
        
        mRecyclerView.setAdapter(mAdapter);
        
        Activity context = getActivity();
        mNumberColumns = getNumberColumns(getOrientation());
        mGridLayoutManager = new GridLayoutManager(getContext(), mNumberColumns);
        GridLayoutManager.SpanSizeLookup spanSizeLookup = new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (mAdapter == null) {
                    return 1;
                }
                Item item = mAdapter.getItem(position);
                if (item == null) {
                    return 1;
                }
                
                return item.getSpanSize();
            }
        };
        
        mGridLayoutManager.setSpanSizeLookup(spanSizeLookup);
        mSpacingDecoration = new MasterGridSpacingDecoration();
        int space = (int) ResourceUtils.getDimension(context,
                R.attr.base_ui_catalog_item_space,
                R.dimen.default_catalog_item_space);
        mSpacingDecoration.setInfo(mNumberColumns, ScreenUtils.getWidthPixels(getActivity()), space);
        mRecyclerView.addItemDecoration(mSpacingDecoration);
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        
        mRecyclerView.getRecycledViewPool()
                .setMaxRecycledViews(MasterList.TYPE_ITEM,
                        mNumberColumns * getNumberRows(getOrientation()));
        
        RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
            int mLastTop = -1;
            
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                mLastTop = onRecyclerViewScroll(mLastTop);
//                LOG.d("scroll dy = %d", dy);
                if (hasSectionIndexer()) {
                    //TODO remove handle hide scroll delay
                    //TODO send a new delay message delay
                }
                
                if (isAllowPaging() && hasNextPage() && mAdapter != null) {
                    int bottom = mGridLayoutManager.findLastVisibleItemPosition();
                    if (shouldLoadNextPage(mAdapter.getItemCount(), bottom)) {
                        saveLastTopPosition();
                        loadData();
                    }
                }
            }
        };

//        mRecyclerView.setStateChangeListener(this);
        
        mRecyclerView.addOnScrollListener(onScrollListener);
//        setupIndexer();
    }
    
    private int onRecyclerViewScroll(int lastTop) {
        SectionProvider sectionProvider = mAdapter.getMasterList().getSectionProvider();
        if (sectionProvider == null || sectionProvider.getIndexSectionCount() == 0) {
            return lastTop;
        }
        
        int top = mGridLayoutManager.findFirstVisibleItemPosition();
        if (lastTop == top) {
            return lastTop;
        }
        
        lastTop = top;
        if (mAdapter == null) {
            return lastTop;
        }
        if (top < 0) {
            return lastTop;
        }
        Item item = mAdapter.getItem(top);
        
        if (item instanceof SectionProvider.SectionItem) {
            Section section = (Section) item.getData();
            if (section.customView && item.getSectionIndex() < 0) {
                //custom view not has section -> hide section header
                setStickSectionVisible(false);
                return lastTop;
            }
        }
        
        int sectionIndex = item.getSectionIndex();
        if (sectionIndex < 0 || sectionIndex >= sectionProvider.size()) {
            return lastTop;
        }
        Section section = (Section) sectionProvider.get(sectionIndex).getData();
        if (section == null) {
            return lastTop;
        }
        
        updateStickSectionView(section);
        return lastTop;
    }
    //endregion
    
    //region Lazy views
    protected void setStickSectionView(View stickSectionView) {
//        mStickSectionContainer = findLazyViewById(R.id.stub_stick_section_container, R.id.stick_section_container);
//        mStickSectionContainer.removeAllViews();
//        mStickSectionContainer.addView(stickSectionView);
    }
    
    private void updateStickSectionView(@Nullable Section section) {
        if (mStickSectionContainer != null) {
            updateStickSection(section);
            setStickSectionVisible(section != null);
        }
    }
    
    protected void updateStickSection(@Nullable Section section) {
        throw new UnsupportedOperationException("Please implement this function before use");
    }
    
    
    protected void setStickSectionVisible(boolean visible) {
        if (mStickSectionContainer != null) {
            mStickSectionContainer.setVisibility(bool2Visible(visible));
        }
    }
    
    protected void setHeaderView(View headerView) {
//        mHeaderContainer = findLazyViewById(R.id.stub_header_container, R.id.header_container);
//        mHeaderContainer.removeAllViews();
//        mHeaderContainer.addView(headerView);
    }
    
    public void setHeaderVisible(boolean visible) {
        if (mHeaderContainer != null)
            mHeaderContainer.setVisibility(bool2Visible(visible));
    }
    
    protected void setFooterView(View footerView) {
//        mFooterContainer = findLazyViewById(R.id.stub_footer_container, R.id.footer_container);
//        mFooterContainer.removeAllViews();
//        mFooterContainer.addView(footerView);
    }
    
    
    public void setFooterVisisble(boolean visible) {
        if (mFooterContainer != null)
            mFooterContainer.setVisibility(bool2Visible(visible));
    }
    
    
    //endregion
    
    //region Load data
    public void reloadData() {
        if (mAdapter != null) {
            //TODO something or nothing
        }
        mCurrentPage = -1;
        loadData();
    }
    
    @NonNull
    protected abstract Observable<MasterLoaderResponse> prepareLoader(int page);
    
    private void loadData() {
        enableRefresh(false);
//        setMenuFilterEnable(false);
        showLoading(true);
        
        prepareLoader(mCurrentPage + 1)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if (isContextInvalid())
                        return;
                    
                    enableRefresh(true);
                    
                    int length = mDataProviders.size();
                    if (response == null) {
                        onLoadDataError(new NullPointerException());
                        return;
                    }
                    if (response.isShouldClear()) {
                        length = 0;
                    }
                    
                    List<DataProvider> dataProviders = new ArrayList<>(length + 1);
                    
                    if (response.isAddTop()) {
                        if (response.getDataProviders().size() > 0)
                            dataProviders.add(response.getDataProviders().get(0));
                        for (int i = 0; i < length; i++) {
                            dataProviders.add((DataProvider) mDataProviders.get(i).clone());
                        }
                    } else {
                        for (int i = 0; i < length; i++) {
                            dataProviders.add((DataProvider) mDataProviders.get(i).clone());
                        }
                        if (response.getDataProviders().size() > 0)
                            dataProviders.add(response.getDataProviders().get(0));
                    }
                    
                    mHasNextPage = !response.isLastPage();
                    onLoaded(dataProviders);
                }, e -> {
                    onLoadDataError(e);
                });
    }
    
    private void onLoadDataError(Throwable e) {
        if (isContextInvalid())
            return;
        showLoading(false);
        hideRefreshing();
        if (mAdapter == null || mAdapter.getItemCount() == 0) {
            displayErrorState(e);
        }
    }
    
    /**
     * Do separate section and generate new master list.
     * Then call {@link #setupAdapter(MasterList, List<DataProvider))}
     *
     * @param providers
     */
    protected void onLoaded(List<DataProvider> providers) {
        mCurrentPage++;
        cookData(providers)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(masterList -> {
                    setupAdapter(masterList, providers);
                });
    }
    
    
    private Observable<MasterList> cookData(List<DataProvider> providers) {
        return Observable.create(subscriber -> {
            Observable<List<Section>> observable = generateSection(providers);
            List<Section> sections = null;
            if (observable != null) {
                sections = observable.toBlocking().first();
            }
            
            MasterList masterList = callProduceMasterList(providers, sections).toBlocking().first();
            subscriber.onNext(masterList);
            subscriber.onCompleted();
        });
    }
    
    @Nullable
    protected abstract Observable<List<Section>> generateSection(List<DataProvider> providers);
    
    /**
     * Override this function for each situation
     *
     * @return null if no footer
     */
    protected FooterProvider.FooterItem getFooterItem(boolean hasNextPage, boolean willEmpty) {
        return null;
    }
    
    //Checking whether no section and no data provider
    private boolean checkEmpty(List<Section> sections, List<DataProvider> dataProviders) {
        if (sections != null && !sections.isEmpty())
            return false;
        if (dataProviders == null || dataProviders.isEmpty())
            return true;
        for (DataProvider p : dataProviders) {
            if (p.size() > 0)
                return false;
        }
        return true;
    }
    
    protected Observable<MasterList> callProduceMasterList(List<DataProvider> providers, @Nullable List<Section> sections) {
        boolean willEmpty = checkEmpty(sections, providers);
        return produceMasterList(providers, sections, getAdPositionGenerator(), getFooterItem(mHasNextPage, willEmpty));
    }
    
    @NonNull
    protected abstract Observable<MasterList> produceMasterList(List<DataProvider> providers,
                                                                @Nullable List<Section> sections,
                                                                @Nullable AdPositionGenerator adGenerator,
                                                                @Nullable FooterProvider.FooterItem item);
    
    private void releaseAdapter() {
        mAdapter.unregisterAdapterDataObserver(mAdapterDataObserver);
        mAdapter = null;
    }
    
    private AbstractMasterAdapter initAdapter() {
        AbstractMasterAdapter adapter = newAdapter();
        adapter.registerAdapterDataObserver(mAdapterDataObserver);
        return adapter;
    }
    
    protected void setupAdapter(MasterList masterList, List<DataProvider> providers) {
        if (isContextInvalid()) return;
        
        if (masterList == null || masterList.size() == 0) {
            displayEmptyState();
            return;
        }
        
        mAdapter.setMasterList(masterList);
        applyViewsVisible(mLazyViews, false, LAZY_RECYCLER_INDEX);
        hideRefreshing();
        restoreRecyclerLastTopPosition();
        
        mDataProviders = providers;
    }
    
    @NonNull
    protected abstract AbstractMasterAdapter newAdapter();
    
    /**
     * create ad position generator based on display ad by list of position or range of random
     *
     * @return null if fragment contains no ad
     */
    protected AdPositionGenerator getAdPositionGenerator() {
        return null;
    }
    //endregion
    
    //region Modify data
    public void removeItem(int position) {
        mAdapter.getMasterList().remove(position);
        SectionProvider sectionProvider = mAdapter.getMasterList().getSectionProvider();
        List<Section> sections = sectionProvider == null ? null : sectionProvider.getSections();
        FooterProvider.FooterItem footerItem = getFooterItem(mHasNextPage, checkEmpty(sections, mDataProviders));
        saveLastTopPosition();
        callProduceMasterList(mDataProviders, sections)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(masterList -> {
                    setupAdapter(masterList, mDataProviders);
                });
    }
    
    public void removeItems(Collection<Integer> positions) {
        for (int pos : positions) {
            mAdapter.getMasterList().remove(pos);
        }
        SectionProvider sectionProvider = mAdapter.getMasterList().getSectionProvider();
        List<Section> sections = sectionProvider == null ? null : sectionProvider.getSections();
        saveLastTopPosition();
        callProduceMasterList(mDataProviders, sections)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(masterList -> {
                    setupAdapter(masterList, mDataProviders);
                });
    }
    //endregion
    
    //region Sidebar Indexer
//    private void setupIndexer() {
    //TODO
//        if (mSectionIndexerSidebar == null) return;
//        if (hasSectionIndexer() && mAdapter != null) {
////            setStickSectionVisible(true);
//            mSectionIndexerSidebar.setOnFastScroll(this);
////            mRecyclerView.setPadding(0, 0, getResources().getDimensionPixelOffset(R.dimen.activity_vertical_margin), 0);
//            mSectionIndexerSidebar.setSectionIndexer(mAdapter);
//        } else {
////            setStickSectionVisible(false);
////            mRecyclerView.setPadding(0, 0, 0, 0);
//        }
//    }
    
    protected boolean hasSectionIndexer() {
        return false;
    }
    //TODO
//    @Override
//    public void onStartScroll() {
//        enableRefresh(false);
//    }
//
//    @Override
//    public void onScroll(int section, int sectionPosition, float scrollPercent, float sectionPercent) {
//        mGridLayoutManager.scrollToPositionWithOffset(sectionPosition,
//                -getResources().getDimensionPixelOffset(R.dimen.thumb_item_spacing));
//    }
//
//    @Override
//    public void onStopScroll() {
//        enableRefresh(true);
//    }
    
    @Override
    public void onFastScrollStart() {
        enableRefresh(false);
    }
    
    @Override
    public void onFastScrollStop() {
        enableRefresh(true);
    }
    
    
    //endregion
    
    //region RefreshLayout
    private void setupRefreshLayout() {
        mRefreshLayout.setDistanceToTriggerSync(
                (int) getResources().getDimension(R.dimen.swipe_refresh_distance));
        mRefreshLayout.setOnRefreshListener(() -> {
            mLastTopPosition = 0;
            onRequestRefreshList();
        });
    }
    
    protected abstract void onRequestRefreshList();
    //endregion
    
    //region Error/Empty view
    protected void displayErrorState(Throwable e) {
        LOG.e(e, "display error");
        if (mAdapter == null || mAdapter.getItemCount() == 0) {
            if (mErrorStateViewContainer == null) {
                mErrorStateViewContainer = findLazyViewById(R.id.stub_error_state_view_container, R.id.error_state_view_container);
                View errorView = getErrorView(mErrorStateViewContainer);
                if (errorView != null) {
                    mErrorStateViewContainer.addView(errorView);
                }
            }
            
            bindErrorView();
        }
        
        applyViewsVisible(mLazyViews, false, LAZY_ERROR_CONTAINER_INDEX);
        hideRefreshing();
    }
    
    protected View getErrorView(ViewGroup parent) {
        return null;
    }
    
    protected void bindErrorView() {
        
    }
    
    protected void displayEmptyState() {
        if (mAdapter == null || mAdapter.getItemCount() == 0) {
            if (mEmptyStateViewContainer == null) {
                mEmptyStateViewContainer = findLazyViewById(R.id.stub_empty_state_view_container, R.id.empty_state_view_container);
                View errorView = getEmptyView(mEmptyStateViewContainer);
                if (errorView != null) {
                    mEmptyStateViewContainer.addView(errorView);
                }
            }
            
            bindEmptyView();
        }
        
        applyViewsVisible(mLazyViews, false, LAZY_EMPTY_CONTAINER_INDEX);
        
        hideRefreshing();
    }
    
    protected View getEmptyView(ViewGroup parent) {
        return null;
    }
    
    protected void bindEmptyView() {
        
    }
    
    //endregion
    
    //region Utility
    
    
    protected void applyViewsVisible(List<LazyObject<View>> views, boolean visible) {
        for (int i = 0; i < views.size(); i++) {
            LazyObject<View> lv = views.get(i);
            if (lv.get() != null) {
                lv.get().setVisibility(bool2Visible(visible));
            }
        }
    }
    
    protected void applyViewsVisible(List<LazyObject<View>> views, List<Boolean> visible) {
        for (int i = 0; i < views.size(); i++) {
            LazyObject<View> lv = views.get(i);
            if (lv.get() != null) {
                lv.get().setVisibility(bool2Visible(visible.get(i)));
            }
        }
    }
    
    protected void applyViewsVisible(List<LazyObject<View>> views, boolean... visible) {
        for (int i = 0; i < views.size(); i++) {
            LazyObject<View> lv = views.get(i);
            if (lv.get() != null) {
                lv.get().setVisibility(bool2Visible(visible[i]));
            }
        }
    }
    
    protected void applyViewsVisible(List<LazyObject<View>> views, boolean visible, int... excepts) {
        Set<Integer> set = new HashSet<>();
        for (int e : excepts) {
            set.add(e);
        }
        for (int i = 0; i < views.size(); i++) {
            LazyObject<View> lv = views.get(i);
            if (lv.get() != null) {
                lv.get().setVisibility(bool2Visible(set.contains(i) != visible));
            }
        }
    }
    
    @Visibility
    private int bool2Visible(boolean visible) {
        return visible ? View.VISIBLE : View.GONE;
    }
    
    public void hideDataViews() {
        //TODO hide all data views and show loading view
    }
    
    
    public boolean isRefreshing() {
        return mRefreshLayout != null && mRefreshLayout.isRefreshing();
    }
    
    protected void showLoading(boolean show) {
        mLoadingView.setVisibility(bool2Visible(show));
    }
    
    protected void enableRefresh(boolean enabled) {
//        mRefreshLayout.setEnabled(enabled);
        mRefreshLayout.setEnabled(false);
    }
    
    protected void hideRefreshing() {
        if (mRefreshLayout != null && mRefreshLayout.isRefreshing()) {
            mRefreshLayout.setRefreshing(false);
        }
    }
    
    protected abstract int getNumberColumns(int orientation);
    
    protected abstract int getNumberRows(int orientation);
    
    protected int getOrientation() {
        if (mLastOrientation < 0) {
            mLastOrientation = ScreenUtils.getOrientation(getContext());
        }
        return mLastOrientation;
    }
    
    @CallSuper
    protected void onAdapterDataChanged() {
//        List<?> sections = mAdapter.getIndexerSections();
//        boolean noSection = sections == null || sections.checkEmpty();
//        setupIndexer();
        
        int position = mGridLayoutManager.findFirstVisibleItemPosition();
        
        if (position < 0 || position >= mAdapter.getItemCount()) {
            setStickSectionVisible(false);
            return;
        }
        Item item = mAdapter.getItem(position);
        SectionProvider sectionProvider = mAdapter.getMasterList().getSectionProvider();
        if (item.getSectionIndex() < 0 || sectionProvider == null) {
            setStickSectionVisible(false);
        } else {
            Item sectionItem = sectionProvider.get(item.getSectionIndex());
            if (sectionItem == null) {
                setStickSectionVisible(false);
            } else {
                setStickSectionVisible(true);
                updateStickSectionView((Section) sectionItem.getData());
            }
        }
        
        //TODO find the top item and get its section if need
//        setStickSectionVisible(mAdapter.getItemCount() == 0 || noSection);
//        updateStickSectionView(noSection ? "" : sections.get(0).toString());
    }
    
    
    //endregion
    
    //region Save/Restore position
    public void restoreRecyclerLastTopPosition() {
//        Logger.d("restore recycler last top = %s", mLastTopPosition);
        if (mRecyclerView != null) {
            mRecyclerView.postDelayed(() -> {
                if (mLastTopPosition >= mAdapter.getItemCount()) {
                    mLastTopPosition = mAdapter.getItemCount() - 1;
                }
                mGridLayoutManager.scrollToPositionWithOffset(mLastTopPosition, mLastTopOffset);
                int lastTop = mLastTopPosition;
                mLastTopPosition = -1;
                
                if (lastTop > 0) {
                    
                    Item item = mAdapter.getItem(lastTop - 1);
                    SectionProvider sectionProvider = mAdapter.getMasterList().getSectionProvider();
                    if (sectionProvider == null)
                        return;
                    Item sectionItem = sectionProvider.get(item.getSectionIndex());
                    if (sectionItem == null)
                        return;
                    Section section = (Section) sectionItem.getData();
                    updateStickSectionView(section);
                }
            }, 0L);
        }
    }
    
    public void saveLastTopPosition() {
        //Save last top if and only if mLastTopPosition < 0 to make sure last top has been restored
        if (mRecyclerView != null && mLastTopPosition < 0) {
            GridLayoutManager gridLayoutManager = (GridLayoutManager) mRecyclerView.getLayoutManager();
            int pos = gridLayoutManager.findFirstCompletelyVisibleItemPosition();
            View v = gridLayoutManager.findViewByPosition(pos);
            if (v != null) {
                mLastTopPosition = pos;
                mLastTopOffset = (int) (v.getTop() - ResourceUtils.getDimension(getContext(),
                        R.attr.base_ui_catalog_item_space,
                        R.dimen.default_catalog_item_space));
            }
        }
    }
    //endregion
    
    public int getItemCount() {
        return mAdapter.getItemCount();
    }
    
    //region Paging
    public boolean isAllowPaging() {
        return false;
    }
    
    public boolean hasNextPage() {
        return mHasNextPage;
    }
    
    public int getCurrentPage() {
        return mCurrentPage;
    }
    
    public boolean shouldLoadNextPage(int total, int bottom) {
        return false;
    }
    //endregion
    
    protected interface LazyObject<T> {
        T get();
    }
}
