package com.ofmonsters.baseui.listfragment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.ofmonsters.baseui.list.DataProvider;
import com.ofmonsters.baseui.list.FooterProvider;
import com.ofmonsters.baseui.list.MasterList;
import com.ofmonsters.baseui.list.MasterLoaderResponse;
import com.ofmonsters.baseui.list.Section;
import com.ofmonsters.baseui.list.SectionProvider;
import com.ofmonsters.baseui.list.placement.AdPositionGenerator;
import com.orhanobut.logger.LLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by tuanchauict on 3/20/17.
 */

public abstract class BasePresenter implements ListFragmentContract.CPresenter {
    private static final LLogger LOG = LLogger.getLocalLogger().withTag("BASEPRESENTER").on();
    
    private final ListFragmentContract.CView<ListFragmentContract.CPresenter> mView;
    private final CompositeSubscription mSubscriptions;
    
    
    private final AtomicBoolean mLoading = new AtomicBoolean(false);
    private final AtomicBoolean mForceReload = new AtomicBoolean(false);
    
    private List<DataProvider> mDataProviders = new ArrayList<>();
    
    private AtomicInteger mOffset = new AtomicInteger(0);
    
    private boolean mHasNextPage = true;
    
    private int mOrientation = -1000;
    
    public BasePresenter(@NonNull ListFragmentContract.CView<ListFragmentContract.CPresenter> view) {
        mView = view;
        mSubscriptions = new CompositeSubscription();
    }
    
    
    @Override
    public void subscribe() {
        if (mDataProviders == null || mDataProviders.isEmpty()) {
            loadData();
        } else {
            //TODO get current orientation and saved orientation. call generateMasterList if and only if different
            if (mOrientation != mView.getOrientation())
                generateMasterList(mDataProviders);
        }
    }
    
    @Override
    public void unsubscribe() {
        mSubscriptions.clear();
        mLoading.set(false);
        mForceReload.set(false);
    }
    
    public ListFragmentContract.CView<ListFragmentContract.CPresenter> getView() {
        return mView;
    }
    
    /**
     * @return -1 for all, > 0 for pagination
     */
    protected int getPageSize() {
        return -1;
    }
    
    @Override
    public void reload(boolean showLoading) {
        if (mLoading.get())
            return;
        mOffset.set(0);
        loadData(showLoading);
    }
    
    @Override
    public void forceReload(boolean showLoading) {
        mForceReload.set(true);
        reload(showLoading);
    }
    
    @NonNull
    protected abstract Observable<MasterLoaderResponse> prepareLoader(int offset, int length);
    
    public void loadData(boolean showLoading) {
        if (mLoading.get())
            return;
        if (showLoading)
            mView.showLoadingIndicator(true);
        mLoading.set(true);
        Subscription subscription = prepareLoader(mOffset.get(), getPageSize())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if (response == null) {
                        LOG.w("PLEASE DONT RESPONSE NULL. I DON'T KNOW HOW TO PROCESS IT :(" );
                        generateMasterList(mDataProviders);
                        return;
                    } else if(response.isEmpty()){
                        if(response.isShouldClear())
                            mDataProviders = new ArrayList<DataProvider>();
                        generateMasterList(mDataProviders);
                        return;
                    }
                    onLoaded(response);
                }, e -> {
                    mLoading.set(false);
                    mForceReload.set(false);
                    mView.showLoadingIndicator(false);
                    mView.setRefreshEnable(true);
                    mView.showErrorState(e);
                }, () -> {
                    mLoading.set(false);
                    mForceReload.set(false);
                });
        
        mSubscriptions.add(subscription);
    }
    
    @Override
    public void loadData() {
        loadData(true);
    }
    
    private void onLoaded(@NonNull MasterLoaderResponse response) {
        int length = mDataProviders.size();
        if (response.isShouldClear()) {
            length = 0;
            mOffset.set(0);
        }
        List<DataProvider> providers = response.getDataProviders();
        if (allowPaging()) {
            for (DataProvider provider : providers)
                mOffset.addAndGet(provider.size());
        }
        
        List<DataProvider> dataProviders = new ArrayList<>(length + 1);
        if (response.isAddTop()) {
            for (DataProvider provider : providers) {
                if (provider != null) {
                    dataProviders.add(provider);
                }
            }
            for (int i = 0; i < length; i++) {
                dataProviders.add((DataProvider) mDataProviders.get(i).clone());
            }
        } else {
            for (int i = 0; i < length; i++) {
                dataProviders.add((DataProvider) mDataProviders.get(i).clone());
            }
            for (DataProvider provider : providers) {
                if (provider != null)
                    dataProviders.add(provider);
            }
        }
        
        mDataProviders = dataProviders;
        mHasNextPage = !response.isLastPage();
        
        generateMasterList(dataProviders);
    }
    
    @Override
    public void reproduceMasterList() {
        generateMasterList(mDataProviders);
    }
    
    private void generateMasterList(List<DataProvider> dataProviders) {
        mLoading.set(true);
        Subscription subscription = cookData(dataProviders)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(masterList -> {
                    if (masterList == null || masterList.size() == 0) {
                        
                        mView.showEmptyState();
                        onPostProduceMasterList(masterList);
                        return;
                    }
                    mView.showLoadingIndicator(false);
                    mView.setRefreshEnable(true);
                    mView.showList(masterList);
                    mOrientation = mView.getOrientation();
                    onPostProduceMasterList(masterList);
                }, e -> {
                    mLoading.set(false);
                    mView.showErrorState(e);
                    onPostProduceMasterList(null);
                }, () -> {
                    mLoading.set(false);
                });
        mSubscriptions.add(subscription);
    }
    
    @Override
    public void onPostProduceMasterList(@Nullable MasterList masterList) {
        
    }
    
    private Observable<MasterList> cookData(List<DataProvider> providers) {
        return Observable.fromCallable(() -> {
            Observable<List<Section>> observable = generateSection(providers);
            List<Section> sectionList = null;
            if (observable != null)
                sectionList = observable.toBlocking().first();
            return callProduceMasterList(providers, sectionList).toBlocking().first();
        });
    }
    
    
    @Nullable
    protected abstract Observable<List<Section>> generateSection(List<DataProvider> providers);
    
    protected Observable<MasterList> callProduceMasterList(List<DataProvider> providers, @Nullable List<Section> sections) {
        boolean willEmpty = checkEmpty(sections, providers);
        return produceMasterList(providers, sections, getAdPositionGenerator(),
                getFooterItem(mHasNextPage, willEmpty),
                mView.getNumberColumns(mView.getOrientation()));
    }
    
    @NonNull
    protected abstract Observable<MasterList> produceMasterList(List<DataProvider> providers,
                                                                @Nullable List<Section> sections,
                                                                @Nullable AdPositionGenerator adGenerator,
                                                                @Nullable FooterProvider.FooterItem item,
                                                                int column);
    
    
    /**
     * create ad position generator based on display ad by list of position or range of random
     *
     * @return null if fragment contains no ad
     */
    protected AdPositionGenerator getAdPositionGenerator() {
        return null;
    }
    
    /**
     * Override this function for each situation
     *
     * @return null if no footer
     */
    protected FooterProvider.FooterItem getFooterItem(boolean hasNextPage, boolean willEmpty) {
        return null;
    }
    
    @Override
    public void removeItem(MasterList masterList, int position) {
        removeItems(masterList, Arrays.asList(position));
    }
    
    @Override
    public void removeItems(MasterList masterList, Collection<Integer> positions) {
        if (masterList == null)
            return;
        for (int pos : positions) {
            masterList.remove(pos);
        }
        SectionProvider sectionProvider = masterList.getSectionProvider();
        List<Section> sections = sectionProvider == null ? null : sectionProvider.getSections();
        Subscription subscription = callProduceMasterList(mDataProviders, sections)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(masterList1 -> {
                    if (masterList1 == null || masterList1.size() == 0) {
                        mView.showEmptyState();
                        return;
                    }
                    mView.showList(masterList1);
                    mOrientation = mView.getOrientation();
                }, e -> {
                    //IGNORE
                });
        mSubscriptions.add(subscription);
    }
    
    //Checking whether no section and no data provider
    private boolean checkEmpty(List<Section> sections, List<DataProvider> dataProviders) {
        if (sections != null && !sections.isEmpty())
            return false;
        if (dataProviders == null || dataProviders.isEmpty())
            return true;
        for (DataProvider p : dataProviders) {
            if (p.size() > 0)
                return false;
        }
        return true;
    }
    
    @Override
    public boolean hasNextPage() {
        return mHasNextPage;
    }
    
    @Override
    public boolean isForcedReload() {
        return mForceReload.get();
    }
    
    public void bindToLifecycle(Subscription subscription) {
        mSubscriptions.add(subscription);
    }
}
