package com.ofmonsters.baseui.listfragment;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.ofmonsters.baseui.adapter.AbstractMasterAdapter;
import com.ofmonsters.baseui.list.MasterList;
import com.ofmonsters.baseui.list.Section;

import java.util.Collection;
import java.util.Objects;

import rx.Subscription;

/**
 * Created by tuanchauict on 3/20/17.
 */

public interface ListFragmentContract {
    interface CView<T> {
        void showLoadingIndicator(boolean active);
        
        void setRefreshEnable(boolean enable);
        
        void showList(MasterList masterList);
        
        android.view.View getStickSectionView(FrameLayout parent);
        
        void updateStickSection(@Nullable Section section);
        
        void setStickSectionVisible(boolean visible);
        
        void showErrorState(Throwable e);
        
        android.view.View getErrorView(ViewGroup parent);
        
        void showEmptyState();
        
        android.view.View getEmptyView(ViewGroup parent);
        
        int getOrientation();
        
        int getNumberColumns(int orientation);
        
        int getNumberRows(int orientation);
        
        int getItemWidth();
        
        int getItemHeight();
        
        int getItemSpace();
        
        @NonNull
        AbstractMasterAdapter newAdapter();
        
        @LayoutRes
        int getViewLayoutId();
        
        boolean shouldLoadNextPage(int totalItem, int lastVisibleItem);
        
        void removeItem(int position);
        
        void removeItems(Collection<Integer> positions);
        
        T getPresenter();
        
        String getString(@StringRes int resId);
        
        String getString(@StringRes int resId, Object... formatArgs);
        
        void notifyDataSetChanged();
        
        void notifyDataSetChanged(int position);
    }
    
    interface CPresenter {
        void subscribe();
        
        void unsubscribe();
        
        void reload(boolean showLoading);
        
        void forceReload(boolean showLoading);
        
        void loadData(boolean showLoading);
        
        void loadData();
        
        void reproduceMasterList();
        
        void removeItem(MasterList masterList, int position);
        
        void removeItems(MasterList masterList, Collection<Integer> positions);
        
        boolean hasNextPage();
        
        boolean allowPaging();
        
        CView<CPresenter> getView();
        
        void bindToLifecycle(Subscription subscription);
        
        void onPostProduceMasterList(@Nullable MasterList masterList);
        
        boolean isForcedReload();
    }
}
