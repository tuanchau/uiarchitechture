package com.ofmonsters.baseui.listfragment;

import android.content.res.Configuration;

import com.ofmonsters.baseui.utils.ScreenUtils;


/**
 * Created by tuanchauict on 3/27/17.
 */

public abstract class BaseMVPGridFragment extends BaseMVPCatalogFragment {
    
    private int mLandscapeColumns = -1;
    private int mPortraitColumns = -1;
    private int mLandscapeRows = -1;
    private int mPortraitRows = -1;
    
    @Override
    public int getNumberColumns(int orientation) {
        if (isLandscape(orientation)) {
            if (mLandscapeColumns > 0) {
                return mLandscapeColumns;
            }
            mLandscapeColumns = calculateColumns();
            return mLandscapeColumns;
        } else {
            if (mPortraitColumns > 0) {
                return mPortraitColumns;
            }
            mPortraitColumns = calculateColumns();
            return mPortraitColumns;
        }
    }
    
    private int calculateColumns() {
        int maxWidth = ScreenUtils.getWidthPixels(getActivity());
        int space = getItemSpace();
        int itemWidth = getItemWidth();
        return (maxWidth - space) / (itemWidth + space);
    }
    
    @Override
    public int getNumberRows(int orientation) {
        if (isLandscape(orientation)) {
            if (mLandscapeRows > 0) {
                return mLandscapeRows;
            }
            mLandscapeRows = calculateRows();
            return mLandscapeRows;
        } else {
            if (mPortraitRows > 0) {
                return mPortraitRows;
            }
            mPortraitRows = calculateRows();
            return mPortraitRows;
        }
    }
    
    private int calculateRows() {
        int maxHeight = ScreenUtils.getHeightPixels(getActivity());
        int space = getItemSpace();
        int itemHeight = getItemHeight();
        return maxHeight / (itemHeight + space) + 3;
    }
    
    public boolean isLandscape(int orientation) {
        return orientation == Configuration.ORIENTATION_LANDSCAPE;
    }
}
