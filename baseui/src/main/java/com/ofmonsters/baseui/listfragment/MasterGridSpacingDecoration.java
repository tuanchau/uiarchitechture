package com.ofmonsters.baseui.listfragment;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ofmonsters.baseui.adapter.AbstractMasterAdapter;
import com.ofmonsters.baseui.list.Item;
import com.ofmonsters.baseui.list.MasterList;

/**
 * Created by tuanchauict on 9/18/16.
 */
public class MasterGridSpacingDecoration extends RecyclerView.ItemDecoration {
    private int mSpace;
    private int mNumberColumns;
    private int[] mLeftSpaces;
    private int[] mRightSpaces;
    
    
    public void setInfo(int numberColumns, int screenWidth, int space) {
        mSpace = space;
        mNumberColumns = numberColumns;
        int itemWidth = screenWidth / numberColumns;
        int realItemWidth = (screenWidth - space * (numberColumns + 1)) / numberColumns;
        
        mLeftSpaces = new int[numberColumns];
        mRightSpaces = new int[numberColumns];
        
        mLeftSpaces[0] = space;
        mRightSpaces[0] = itemWidth - realItemWidth - space;
        for (int i = 1; i < numberColumns; i++) {
            mLeftSpaces[i] = space - mRightSpaces[i - 1];
            mRightSpaces[i] = itemWidth - realItemWidth - mLeftSpaces[i];
        }
    }
    
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int space = mSpace;
        int numCol = mNumberColumns;
        int[] leftSpaces = mLeftSpaces;
        int[] rightSpaces = mRightSpaces;
        int childPosition = parent.getChildLayoutPosition(view);
        
        
        AbstractMasterAdapter adapter = (AbstractMasterAdapter) parent.getAdapter();
        Item item = adapter.getItem(childPosition);
        int column = item.getColumn();
        int span = item.getSpanSize();
        
        if (item.getType() != MasterList.TYPE_SECTION
                || (childPosition > 0 && adapter.getItem(childPosition - 1).getType() == MasterList.TYPE_ITEM)) {
            outRect.top = space;
            if (item.getType() != MasterList.TYPE_SECTION) {
                outRect.left = leftSpaces[column];
                outRect.right = rightSpaces[column + span - 1];
            }
        }
        
        //for bottom most items
        int adapterCount = adapter.getItemCount();
        if (childPosition < adapterCount - numCol) {
            return;
        }
        for (int i = 1; i <= numCol; i++) {
            Item it = adapter.getItem(adapterCount - i);
            if (it == item) {
                outRect.bottom = space;
            }
            if (it.getColumn() == 0) {
                break;
            }
        }
    }
}
