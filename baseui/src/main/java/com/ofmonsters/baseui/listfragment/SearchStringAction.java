package com.ofmonsters.baseui.listfragment;

import java.util.List;

/**
 * Created by tuanchauict on 9/26/16.
 */
@Deprecated
public interface SearchStringAction {
    void saveNewSearchString(String searchString);

    List<String> getRecentSearchStrings();

    void clearSearchStrings();
}
