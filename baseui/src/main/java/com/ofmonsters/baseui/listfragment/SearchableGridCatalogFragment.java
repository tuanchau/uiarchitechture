package com.ofmonsters.baseui.listfragment;

import com.ofmonsters.baseui.R;
import com.ofmonsters.baseui.utils.ResourceUtils;
import com.ofmonsters.baseui.utils.ScreenUtils;

/**
 * Created by tuanchauict on 10/2/16.
 */
@Deprecated
public abstract class SearchableGridCatalogFragment extends BaseSearchableCatalogFragment {

    @Override
    protected int getViewLayoutId() {
        return R.layout.frag_grid_layout;
    }

    @Override
    protected int getNumberColumns(int orientation) {

        int maxWidth = ScreenUtils.getWidthPixels(getActivity());
        int itemWidth = (int) ResourceUtils.getDimension(getContext(),
                R.attr.base_ui_grid_item_estimate_width,
                R.dimen.default_grid_item_estimate_width);
        int space = (int) ResourceUtils.getDimension(getContext(),
                R.attr.base_ui_catalog_item_space,
                R.dimen.default_catalog_item_space);
        int column = (maxWidth - space) / (itemWidth + space);
//        Logger.d("max width = %s, column = %s", maxWidth, column);
        return column;
    }

    @Override
    protected int getNumberRows(int orientation) {
        int maxHeight = ScreenUtils.getHeightPixels(getActivity());
        int itemHeight = (int) ResourceUtils.getDimension(getContext(),
                R.attr.base_ui_grid_item_estimate_height,
                R.dimen.default_grid_item_estimate_height);
        int space = (int) ResourceUtils.getDimension(getContext(),
                R.attr.base_ui_catalog_item_space,
                R.dimen.default_catalog_item_space);
        int row = maxHeight / (itemHeight + space) + 3;
        return row;
    }
}
