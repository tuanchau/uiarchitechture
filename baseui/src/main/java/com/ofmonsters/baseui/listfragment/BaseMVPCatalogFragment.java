package com.ofmonsters.baseui.listfragment;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.notabasement.fastscrollrecyclerview.FastScrollListener;
import com.notabasement.fastscrollrecyclerview.FastScrollRecyclerView;
import com.ofmonsters.baseui.BaseFragment;
import com.ofmonsters.baseui.R;
import com.ofmonsters.baseui.adapter.AbstractMasterAdapter;
import com.ofmonsters.baseui.annotation.Visibility;
import com.ofmonsters.baseui.list.Item;
import com.ofmonsters.baseui.list.MasterList;
import com.ofmonsters.baseui.list.Section;
import com.ofmonsters.baseui.list.SectionProvider;
import com.ofmonsters.baseui.utils.ScreenUtils;
import com.orhanobut.logger.LLogger;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by tuanchauict on 3/21/17.
 */

public abstract class BaseMVPCatalogFragment extends BaseFragment implements ListFragmentContract.CView<ListFragmentContract.CPresenter> {
    private static final LLogger LOG = LLogger.getLocalLogger().withTag("CATALOG").off();
    private static final String EXTRA_LAST_TOP_POSITION = "last-top-pos";
    private static final String EXTRA_LAST_TOP_OFFSET = "last-top-offset";
    
    protected static final int LAZY_RECYCLER_INDEX = 0;
    protected static final int LAZY_STICK_SECTION_INDEX = 1;
    protected static final int LAZY_LOADING_INDEX = 2;
    protected static final int LAZY_HEADER_CONTAINER_INDEX = 3;
    protected static final int LAZY_FOOTER_INDEX = 4;
    protected static final int LAZY_ERROR_CONTAINER_INDEX = 5;
    protected static final int LAZY_EMPTY_CONTAINER_INDEX = 6;
    
    private ProgressBar mLoadingView;
    private SwipeRefreshLayout mRefreshLayout;
    private FastScrollRecyclerView mRecyclerView;
    private FrameLayout mStickSectionContainer;
    private FrameLayout mHeaderContainer;
    private FrameLayout mFooterContainer;
    
    private FrameLayout mErrorViewContainer;
    private FrameLayout mEmptyViewContainer;
    private View mEmptyView;
    
    
    private List<LazyObject<View>> mLazyViews = Arrays.asList(
            () -> mRecyclerView,                //0
            () -> mStickSectionContainer,       //1
            () -> mLoadingView,                 //2
            () -> mHeaderContainer,             //3
            () -> mFooterContainer,             //4
            () -> mErrorViewContainer,          //5
            () -> mEmptyViewContainer           //6
    );
    
    private int mLastOrientation = -1;
    private int mNumberColumns;
    private int mLastTopPosition = -1;
    private int mLastTopOffset = 0;
    
    private AbstractMasterAdapter mAdapter;
    
    private GridLayoutManager mGridLayoutManager;
    private MasterGridSpacingDecoration mSpacingDecoration;
    
    private RecyclerView.AdapterDataObserver mAdapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            onDataChanged();
            updateMenuItems();
        }
    };
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(EXTRA_LAST_TOP_POSITION, mLastTopPosition);
        outState.putInt(EXTRA_LAST_TOP_OFFSET, mLastTopOffset);
    }
    
    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            mLastTopPosition = savedInstanceState.getInt(EXTRA_LAST_TOP_POSITION);
            mLastTopOffset = savedInstanceState.getInt(EXTRA_LAST_TOP_OFFSET);
        }
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mAdapter = initAdapter();
    }
    
    @Override
    public void onDestroy() {
        releaseAdapter();
        super.onDestroy();
    }
    
    private AbstractMasterAdapter initAdapter() {
        AbstractMasterAdapter adapter = newAdapter();
        adapter.registerAdapterDataObserver(mAdapterDataObserver);
        return adapter;
    }
    
    private void releaseAdapter() {
        if (mAdapter != null) {
            if (mAdapter.getMasterList() != null)
                mAdapter.getMasterList().release(null);
            mAdapter.unregisterAdapterDataObserver(mAdapterDataObserver);
        }
        mAdapter = null;
    }
    
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getViewLayoutId(), container, false);
        mLoadingView = findView(view, R.id.loading);
        mRefreshLayout = findView(view, R.id.swipe_refresh_layout);
        mRecyclerView = findView(view, R.id.recycler_view);
        mRecyclerView.setFastScrollView(findView(view, R.id.fast_scroll_view));
        mStickSectionContainer = findView(view, R.id.stick_section_container);
        mHeaderContainer = findView(view, R.id.header_container);
        mFooterContainer = findView(view, R.id.footer_container);
        
        mErrorViewContainer = null;
        mEmptyView = null;
        mEmptyViewContainer = null;
        
        mLastOrientation = getOrientation();
        setupRecyclerView();
        
        mRefreshLayout.setDistanceToTriggerSync(
                (int) getResources().getDimension(R.dimen.swipe_refresh_distance));
        mRefreshLayout.setOnRefreshListener(() -> {
            mLastTopPosition = 0;
            mLastTopOffset = 0;
            getPresenter().forceReload(true);
        });
        
        View stickSectionView = getStickSectionView(mStickSectionContainer);
        if (stickSectionView != null) {
            mStickSectionContainer.addView(stickSectionView);
        } else {
            mStickSectionContainer.setVisibility(View.GONE);
            mStickSectionContainer = null;
        }
        
        return view;
    }
    
    
    public void setFastScrollEnable(boolean visible){
        mRecyclerView.setFastScrollVisible(visible);
    }
    
    public boolean isFastScrollVisible(){
        return mRecyclerView.isFastScrollVisible();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        getPresenter().subscribe();
    }
    
    @Override
    public void onPause() {
        getPresenter().unsubscribe();
        super.onPause();
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation != mLastOrientation) {
            resetViewsOnConfigurationChanged();
            int oldCol = getNumberColumns(mLastOrientation);
            mLastOrientation = newConfig.orientation;
            int newCol = getNumberColumns(mLastOrientation);
            if (newCol != oldCol || mAdapter == null || mAdapter.getItemCount() <= 0) {
                // only reproduce master list if new number of columns != old number of columns
                // OR adapter is empty (to reload empty views)
                getPresenter().reproduceMasterList();
            }
        }
    }

    protected void resetViewsOnConfigurationChanged() {
        if (mEmptyViewContainer != null) mEmptyViewContainer.setVisibility(View.INVISIBLE);
        if (mEmptyView != null) mEmptyView.setVisibility(View.INVISIBLE);
        if (mErrorViewContainer != null) mErrorViewContainer.setVisibility(View.INVISIBLE);
        mEmptyViewContainer = null;
        mEmptyView = null;
        mErrorViewContainer = null;
    }
    
    public void onDataChanged() {
        
    }

    /**
     * Update menu items according to data changed, e.g. hide the menu when empty screen.
     */
    protected void updateMenuItems() {
        //TODO something if need, mostly this function doesn't do anything.
    }
    
    public AbstractMasterAdapter getAdapter() {
        return mAdapter;
    }
    
    private void setupRecyclerView() {
        int orientation = getOrientation();
        mNumberColumns = getNumberColumns(orientation);
        RecyclerView.RecycledViewPool pool = mRecyclerView.getRecycledViewPool();
        pool.setMaxRecycledViews(MasterList.TYPE_ITEM, mNumberColumns * getNumberRows(orientation));
        pool.setMaxRecycledViews(MasterList.TYPE_SECTION, 3);
        pool.setMaxRecycledViews(MasterList.TYPE_AD, 1);
        pool.setMaxRecycledViews(MasterList.TYPE_FOOTER, 1);
        
        mRecyclerView.setAdapter(mAdapter);
        Activity context = getActivity();
        mGridLayoutManager = new GridLayoutManager(context, mNumberColumns);
        GridLayoutManager.SpanSizeLookup spanSizeLookup = new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                Item item = mAdapter.getItem(position);
                if (item == null)
                    return 1;
                return item.getSpanSize();
            }
        };
        mGridLayoutManager.setSpanSizeLookup(spanSizeLookup);
        mSpacingDecoration = new MasterGridSpacingDecoration();
        mSpacingDecoration.setInfo(mNumberColumns, ScreenUtils.getWidthPixels(context), getItemSpace());
        mRecyclerView.addItemDecoration(mSpacingDecoration);
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        
        RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
            int mLastTop = -1;
            
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                mLastTop = updateViewOnScrolled(mLastTop);
                ListFragmentContract.CPresenter presenter = getPresenter();
                if (presenter.allowPaging() && presenter.hasNextPage() && mAdapter != null) {
                    int bottom = mGridLayoutManager.findLastVisibleItemPosition();
                    if (shouldLoadNextPage(mAdapter.getItemCount(), bottom)) {
                        presenter.loadData();
                    }
                }
            }
        };
        
        mRecyclerView.addOnScrollListener(onScrollListener);
        mRecyclerView.setFastScrollListener(new FastScrollListener() {
            @Override
            public void onFastScrollStart() {
                if (!mRefreshLayout.isRefreshing())
                    setRefreshEnable(false);
            }
            
            @Override
            public void onFastScrollStop() {
                setRefreshEnable(true);
            }
        });
    }
    
    private int updateViewOnScrolled(int lastTop) {
        if (mAdapter == null || mAdapter.getMasterList() == null)
            return -1;
        SectionProvider sectionProvider = mAdapter.getMasterList().getSectionProvider();
        if (sectionProvider == null || sectionProvider.getIndexSectionCount() == 0) {
            setStickSectionVisible(false);
            return -1;
        }
        
        int top = mGridLayoutManager.findFirstVisibleItemPosition();
        if (lastTop == top) {
            return lastTop;
        }
        lastTop = top;
        if (mAdapter == null)
            return -1;
        Item item = mAdapter.getItem(top);
        if (item instanceof SectionProvider.SectionItem) {
            Section section = (Section) item.getData();
            if (section.customView && item.getSectionIndex() < 0) {
                setStickSectionVisible(false);
                return lastTop;
            }
        }
        int sectionIndex = item.getSectionIndex();
        if (sectionIndex < 0 || sectionIndex >= sectionProvider.size()) {
            setStickSectionVisible(false);
            return lastTop;
        }
        Section section = (Section) sectionProvider.get(sectionIndex).getData();
        if (section != null) {
            LOG.d("section: %s", section.title);
            updateStickSection(section);
            setStickSectionVisible(true);
        }
        return lastTop;
    }
    
    @Override
    public void showList(@NonNull MasterList masterList) {
        if (isContextInvalid()) return;
        int columns = getNumberColumns(mLastOrientation);
        mGridLayoutManager.setSpanCount(columns);
        mSpacingDecoration.setInfo(columns, ScreenUtils.getWidthPixels(getActivity()), getItemSpace());
        mAdapter.setMasterList(masterList);
        applyViewsVisible(mLazyViews, false, LAZY_RECYCLER_INDEX, LAZY_STICK_SECTION_INDEX);
        updateViewOnScrolled(-1);
        setRefreshEnable(true);
        showLoadingIndicator(false);
        
        mRecyclerView.setFastScrollVisible(shouldDisplayFastScroll(masterList));
//        restoreRecyclerLastTopPosition();
    }
    
    protected boolean shouldDisplayFastScroll(MasterList masterList){
        return true;
    }
    
    @Override
    public void setStickSectionVisible(boolean visible) {
        if (mStickSectionContainer != null) {
//            if(mStickSectionContainer.getVisibility() != View.VISIBLE && visible){
//                mStickSectionContainer.setVisibility(bool2Visible(visible));
//                mStickSectionContainer.getParent().requestLayout();
//            } else {
//            }
            mStickSectionContainer.setVisibility(bool2Visible(visible));
        }
    }
    
    @Override
    public void showLoadingIndicator(boolean active) {
        if (mRefreshLayout != null) {
            mRefreshLayout.setVisibility(View.VISIBLE);
        }
        if (active) {
            if (isEmpty()) {
                mLoadingView.setVisibility(View.VISIBLE);
                applyViewsVisible(mLazyViews, false, LAZY_LOADING_INDEX);
                setRefreshEnable(false);
            } else {
                setRefreshEnable(true);
                mRefreshLayout.setRefreshing(true);
            }
        } else {
            if (mRefreshLayout != null) {
                setRefreshEnable(true);
                mRefreshLayout.setRefreshing(false);
            }
            if (mLoadingView != null) {
                mLoadingView.setVisibility(View.GONE);
            }
        }
    }
    
    @Override
    public void setRefreshEnable(boolean enable) {
        if (mRefreshLayout != null) {
            mRefreshLayout.setVisibility(View.VISIBLE);
            mRefreshLayout.setEnabled(enable);
        }
    }
    
    @Override
    public void showErrorState(Throwable e) {
        LOG.e(e, "display error");
        if (isEmpty()) {
            if (mErrorViewContainer == null) {
                mErrorViewContainer = findLazyViewById(R.id.stub_error_state_view_container, R.id.error_state_view_container);
                View errorView = getErrorView(mErrorViewContainer);
                if (errorView != null) {
                    mErrorViewContainer.removeAllViews();
                    mErrorViewContainer.addView(errorView);
                }
            }
            bindErrorView(e);
            applyViewsVisible(mLazyViews, false, LAZY_ERROR_CONTAINER_INDEX);
            showLoadingIndicator(false);

            if (!isPullToRefreshEnabledForErrorStates() && mRefreshLayout != null) {
                mRefreshLayout.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Override if users need to pull-to-refresh to reload the screen after errors happened.
     * By default, the pull-to-refresh is disabled and a reload button is shown.
     */
    protected boolean isPullToRefreshEnabledForErrorStates() { return false; }
    
    protected void bindErrorView(Throwable e) {
        
    }
    
    @Override
    public void showEmptyState() {
        if (mEmptyViewContainer == null) {
            mEmptyViewContainer = findLazyViewById(R.id.stub_empty_state_view_container, R.id.empty_state_view_container);
            if (mEmptyView == null) {
                mEmptyView = getEmptyView(mEmptyViewContainer);
            }
            if (mEmptyView != null) {
                mEmptyViewContainer.removeAllViews();
                mEmptyViewContainer.addView(mEmptyView);
            }
        }
        mAdapter.setMasterList(MasterList.getEmptyList());
        applyViewsVisible(mLazyViews, false, LAZY_EMPTY_CONTAINER_INDEX);
        showLoadingIndicator(false);
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
        bindEmptyView();
    }
    
    
    
    protected void bindEmptyView() {
        
    }
    
    public void setHeaderView(View headerView) {
        if (headerView == null)
            return;
        mHeaderContainer.removeAllViews();
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        headerView.setLayoutParams(params);
        mHeaderContainer.addView(headerView);
    }
    
    public void setHeaderVisible(boolean visible) {
        mHeaderContainer.setVisibility(bool2Visible(visible));
    }
    
    public void setFooterView(View footerView) {
        if (footerView == null)
            return;
        mFooterContainer.removeAllViews();
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        footerView.setLayoutParams(params);
        mFooterContainer.addView(footerView);
    }
    
    public void setFooterVisible(boolean visible) {
        mFooterContainer.setVisibility(bool2Visible(visible));
    }
    
    private boolean isEmpty() {
        return mAdapter == null || mAdapter.getItemCount() == 0;
    }
    
    
    @Override
    public boolean shouldLoadNextPage(int totalItem, int lastVisibleItem) {
        return false;
    }
    
    @Override
    public void removeItem(int position) {
        if (mAdapter == null)
            return;
        getPresenter().removeItem(mAdapter.getMasterList(), position);
    }
    
    @Override
    public void removeItems(Collection<Integer> positions) {
        if (mAdapter == null)
            return;
        getPresenter().removeItems(mAdapter.getMasterList(), positions);
    }
    
    @Override
    public void notifyDataSetChanged() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }
    
    @Override
    public void notifyDataSetChanged(int position) {
        if (mAdapter != null) {
            mAdapter.notifyItemChanged(position);
        }
    }
    
    public int getOrientation() {
        if (mLastOrientation < 0) {
            mLastOrientation = ScreenUtils.getOrientation(getContext());
        }
        return mLastOrientation;
    }
    
    @Visibility
    private int bool2Visible(boolean visible) {
        return visible ? View.VISIBLE : View.GONE;
    }
    
    protected void applyViewsVisible(List<LazyObject<View>> views, boolean visible, int... excepts) {
        Set<Integer> set = new HashSet<>();
        for (int e : excepts) {
            set.add(e);
        }
        for (int i = 0; i < views.size(); i++) {
            LazyObject<View> lv = views.get(i);
            if (lv.get() != null) {
                lv.get().setVisibility(bool2Visible(set.contains(i) != visible));
            }
        }
    }
    
    
    protected interface LazyObject<T> {
        T get();
    }
}
