Define all values of these list in theme style:

```<attr name="base_ui_catalog_item_space" format="dimension|reference"/>
<attr name="base_ui_grid_item_estimate_width" format="dimension|reference"/>
<attr name="base_ui_grid_item_estimate_height" format="dimension|reference"/>
<attr name="base_ui_list_item_estimate_height" format="dimension|reference"/>

<attr name="base_ui_recent_search_background" format="color|reference"/>
<attr name="base_ui_recent_search_title" format="string|reference"/>
<attr name="base_ui_recent_search_title_text_color" format="color|reference"/>
<attr name="base_ui_ic_recent_search_clear" format="reference"/>
<attr name="base_ui_recent_search_item_text_color" format="color|reference"/>
<attr name="base_ui_recent_search_item_text_size" format="dimension|reference"/>```
