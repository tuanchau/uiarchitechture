package com.notabasement.fastscrollrecyclerview;

/**
 * Created by tuanchauict on 11/21/16.
 * Apply to the Adapter
 */

public interface EstimateScrollerAdapter {
    int getItemCount();
    
    /**
     * Be careful with TYPE_AD, it should be combined of TYPE_AD and adType
     * @param position
     * @return
     */
    int getItemViewType(int position);
    int getColumn(int position);
    int getEstimateHeight(int type);
    int getSpacing();
}
