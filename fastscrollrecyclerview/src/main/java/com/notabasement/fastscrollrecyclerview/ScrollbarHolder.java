package com.notabasement.fastscrollrecyclerview;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by tuanchauict on 5/29/17.
 */

class ScrollbarHolder {
    private final Scrollbar mScrollBar;
    private final ScrollerContentHeight mScrollerContentHeight;
    private boolean mVisible = false;
    
    public ScrollbarHolder(Context context, Scrollbar.ScrollbarDelegate delegate, AttributeSet attrs) {
        mScrollBar = new Scrollbar(context, delegate, attrs);
        mScrollerContentHeight = new ScrollerContentHeight();
    }
    
    public void setVisible(boolean visible) {
        mVisible = visible;
    }
    
    public boolean isVisible(){
        return mVisible && !mScrollerContentHeight.isWorking();
    }
    
    public Scrollbar getScrollBar() {
        return mScrollBar;
    }
    
    public ScrollerContentHeight getScrollerContentHeight() {
        return mScrollerContentHeight;
    }
    
    
}
