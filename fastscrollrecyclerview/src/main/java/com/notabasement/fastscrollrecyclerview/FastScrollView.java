package com.notabasement.fastscrollrecyclerview;

import android.content.Context;
import android.graphics.Canvas;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by tuanchauict on 5/26/17.
 */

public class FastScrollView extends View {
    
    private ScrollbarHolder mScrollbarHolder;
    
    public FastScrollView(Context context) {
        super(context);
    }
    
    public FastScrollView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }
    
    public FastScrollView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    
    @Override
    public void setFocusableInTouchMode(boolean focusableInTouchMode) {
        super.setFocusableInTouchMode(false);
    }
    
    public void setScrollbarHolder(ScrollbarHolder scrollbarHolder) {
        mScrollbarHolder = scrollbarHolder;
    }
    
    @CallSuper
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (mScrollbarHolder.isVisible()) {
            mScrollbarHolder.getScrollBar().draw(canvas);
        }
    }
}
