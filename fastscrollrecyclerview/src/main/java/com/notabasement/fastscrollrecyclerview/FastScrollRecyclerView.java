package com.notabasement.fastscrollrecyclerview;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.orhanobut.logger.LLogger;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by tuanchauict on 11/18/16.
 */

public class FastScrollRecyclerView extends RecyclerView implements Scrollbar.ScrollbarDelegate {
    private static final LLogger LOG = LLogger.getLocalLogger().withTag("FSRV");
    
    private static final int MSG_NOTIFY_DATA_SET_CHANGED = 1;
    
    
    private int mTouchDeltaY;
    private boolean mFastScrolling = false;
    
    private Adapter mAdapter;
    private LinearLayoutManager mLinearLayoutManager = null;
    private EstimateScrollerAdapter mEstimateScroller;
    
    private FastScrollListener mFastScrollListener;
    
    private SectionedAdapter mSectionedAdapter;
    
    private FastScrollView mFastScrollView;
    private ScrollbarHolder mScrollbarHolder;
    
    private AdapterDataObserver mAdapterDataObserver = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            notifyDataSetChanged();
        }
    };
    
    private Handler mControlHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MSG_NOTIFY_DATA_SET_CHANGED)
                onNotifyDataSetChanged();
        }
    };
    
    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable parcelable = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putParcelable("super", parcelable);
        bundle.putBoolean("fastScrollVisible", mScrollbarHolder.isVisible());
        return bundle;
    }
    
    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        Bundle bundle = (Bundle) state;
        super.onRestoreInstanceState(bundle.getParcelable("super"));
        mScrollbarHolder.setVisible(bundle.getBoolean("fastScrollVisible", false));
    }
    
    public FastScrollRecyclerView(Context context) {
        this(context, null);
    }
    
    public FastScrollRecyclerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }
    
    public FastScrollRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Utils.init(context);
        mScrollbarHolder = new ScrollbarHolder(context, this, attrs);
        
        addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                LOG.i("State changed: %s", newState);
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    mScrollbarHolder.getScrollBar().triggerVisible(true);
                } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    mScrollbarHolder.getScrollBar().triggerVisible(false);
                }
            }
            
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                mScrollbarHolder.getScrollBar().triggerVisible(false);
                updateScrollbar();
            }
        });
    }
    
    @Override
    public void setLayoutManager(LayoutManager layout) {
        super.setLayoutManager(layout);
        if (layout instanceof LinearLayoutManager)
            mLinearLayoutManager = (LinearLayoutManager) layout;
    }
    
    @Override
    public void setAdapter(Adapter adapter) {
        super.setAdapter(adapter);
        if (mAdapter != null) {
            mAdapter.unregisterAdapterDataObserver(mAdapterDataObserver);
        }
        mAdapter = adapter;
        mEstimateScroller = null;
        mSectionedAdapter = null;
        
        if (mAdapter != null) {
            mAdapter.registerAdapterDataObserver(mAdapterDataObserver);
            if (mAdapter instanceof EstimateScrollerAdapter) {
                mEstimateScroller = (EstimateScrollerAdapter) mAdapter;
            }
            
            if (mAdapter instanceof SectionedAdapter) {
                mSectionedAdapter = (SectionedAdapter) mAdapter;
            }
        }
        
        notifyDataSetChanged();
    }
    
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        
        mScrollbarHolder.getScrollBar().setBoundingSize(getWidth(), getHeight());
        invalidateScrollbar();
    }
    
    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (isInEditMode())
            return;
        if (mAdapter != null) {
            mAdapter.registerAdapterDataObserver(mAdapterDataObserver);
        }
    }
    
    @Override
    protected void onDetachedFromWindow() {
        if (!isInEditMode() && mAdapter != null) {
            mAdapter.unregisterAdapterDataObserver(mAdapterDataObserver);
        }
        super.onDetachedFromWindow();
    }
    
    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {
        if (mFastScrolling) {
            return true;
        } else {
            if (e.getAction() == MotionEvent.ACTION_DOWN) {
                mFastScrolling = mScrollbarHolder.getScrollBar().canInteract((int) e.getX(), (int) e.getY());
                mScrollbarHolder.getScrollBar().setFastScrolling(mFastScrolling);
            }
        }
        return mFastScrolling || super.onInterceptTouchEvent(e);
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent e) {
        return handleTouchEvent(e) || super.onTouchEvent(e);
    }
    
    @Override
    public void draw(Canvas c) {
        super.draw(c);
        if (mFastScrollView == null) {
            if (mScrollbarHolder.isVisible())
                mScrollbarHolder.getScrollBar().draw(c);
        }
    }
    
    public void setFastScrollView(FastScrollView fastScrollView) {
        mFastScrollView = fastScrollView;
        mFastScrollView.setScrollbarHolder(mScrollbarHolder);
    }
    
    private boolean handleTouchEvent(MotionEvent ev) {
        if (mAdapter == null || !mScrollbarHolder.isVisible())
            return false;
        
        int action = ev.getAction();
        int y = (int) ev.getY();
        
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (mFastScrolling && mFastScrollListener != null) {
                    mFastScrollListener.onFastScrollStart();
                }
                mTouchDeltaY = y - mScrollbarHolder.getScrollBar().getThumbY();
                break;
            case MotionEvent.ACTION_MOVE:
                y -= mTouchDeltaY;
                if (mFastScrolling) {
                    float percent = mScrollbarHolder.getScrollBar().touchPercent(y);
//                    long t = System.currentTimeMillis();
                    int index = mScrollbarHolder.getScrollerContentHeight().getAbsoluteIndex(percent);
                    int offset = mScrollbarHolder.getScrollerContentHeight().getRowOffset(index, percent);

//                    LOG.d("scroll to %-6.5f, index = %-4d offset = %-4d", mScrollbar.touchPercent(y), index, offset);
                    mLinearLayoutManager.scrollToPositionWithOffset(index, offset);
                    if (mSectionedAdapter != null) {
                        mScrollbarHolder.getScrollBar().setSectionName(mSectionedAdapter.getSectionName(index));
                    } else {
                        mScrollbarHolder.getScrollBar().setSectionName(null);
                    }
                    mScrollbarHolder.getScrollBar().triggerVisible(false);
                }
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                if (mFastScrolling && mFastScrollListener != null) {
                    mFastScrollListener.onFastScrollStop();
                }
                mFastScrolling = false;
                mScrollbarHolder.getScrollBar().setFastScrolling(false);
                invalidateScrollbar();
                mScrollbarHolder.getScrollBar().triggerVisible(false);
                break;
        }
        
        return mFastScrolling;
    }
    
    private void updateScrollbar() {
        if (mAdapter == null || mLinearLayoutManager == null || mEstimateScroller == null)
            return;
        
        if (mScrollbarHolder.getScrollerContentHeight().isWorking())
            return;
        
        int totalItems = mAdapter.getItemCount();
        
        LinearLayoutManager layoutManager = mLinearLayoutManager;
        int botVisible = layoutManager.findLastVisibleItemPosition();
        if (botVisible == totalItems) {
            mScrollbarHolder.getScrollBar().setThumbPercent(1);
        } else {
//            calculatePercent()
//                    .compose(RxUtils.applyComputationSchedulers())
//                    .subscribe(percent -> {
//                        mScrollbar.setThumbPercent(percent);
//                    });
            
            long t = System.nanoTime();
            int topVisible = layoutManager.findFirstVisibleItemPosition();
            View topVisibleView = layoutManager.findViewByPosition(topVisible);
            if (topVisible > 80000)
                LOG.d("#1: run time = %d", System.nanoTime() - t);
            t = System.nanoTime();
            int offset = 0;
            if (topVisibleView != null) {
                offset = -topVisibleView.getTop() + mEstimateScroller.getSpacing();// + topVisibleView.getY();
            }
            if (topVisible > 80000)
                LOG.d("#2: run time = %d", System.nanoTime() - t);
            t = System.nanoTime();
            int topHeight = mScrollbarHolder.getScrollerContentHeight().getY(topVisible);
            if (topVisible > 80000)
                LOG.d("#3: run time = %d", System.nanoTime() - t);
            
            float percent = (float) (topHeight + offset) / (mScrollbarHolder.getScrollerContentHeight().getTotalHeight() - getHeight());
//            LOG.d("t %-3d | tH %-4d | o %-5d | pc %-6.4f, tth %-5d",
//                    topVisible, topHeight, offset, percent, (mScrollerContentHeight.getTotalHeight() - getHeight()));
            percent = percent > 1 ? 1 : percent;
            mScrollbarHolder.getScrollBar().setThumbPercent(percent);
        }
        invalidateScrollbar();
    }

//    private Observable<Float> calculatePercent() {
//        return Observable.create(subscriber -> {
//            LinearLayoutManager layoutManager = mLinearLayoutManager;
//            long t = System.nanoTime();
//            int topVisible = layoutManager.findFirstVisibleItemPosition();
//            View topVisibleView = layoutManager.findViewByPosition(topVisible);
//            if (topVisible > 80000)
//                LOG.d("#1: run time = %d", System.nanoTime() - t);
//            t = System.nanoTime();
//            int offset = 0;
//            if (topVisibleView != null) {
//                offset = -topVisibleView.getTop() + mEstimateScroller.getSpacing();// + topVisibleView.getY();
//            }
//            if (topVisible > 80000)
//                LOG.d("#2: run time = %d", System.nanoTime() - t);
//            t = System.nanoTime();
//            int topHeight = mScrollerContentHeight.getY(topVisible);
//            if (topVisible > 80000)
//                LOG.d("#3: run time = %d", System.nanoTime() - t);
//
//            float percent = (float) (topHeight + offset) / (mScrollerContentHeight.getTotalHeight() - getHeight());
////            LOG.d("t %-3d | tH %-4d | o %-5d | pc %-6.4f, tth %-5d",
////                    topVisible, topHeight, offset, percent, (mScrollerContentHeight.getTotalHeight() - getHeight()));
//            percent = percent > 1 ? 1 : percent;
////            mScrollbar.setThumbPercent(percent);
//            subscriber.onNext(percent);
//            subscriber.onCompleted();
//        });
//
//    }
    
    private void onNotifyDataSetChanged() {
        
        LOG.d("update new scroller data");
        mScrollbarHolder.getScrollBar().setAvailable(false);
        mScrollbarHolder.getScrollerContentHeight().update(mEstimateScroller)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(__ -> {
                    updateScrollbar();
                    mScrollbarHolder.getScrollBar().setAvailable(true);
                    if (getScrollState() == RecyclerView.SCROLL_STATE_DRAGGING)
                        mScrollbarHolder.getScrollBar().triggerVisible(true);
                });
    }
    
    private void notifyDataSetChanged() {
        mControlHandler.removeMessages(MSG_NOTIFY_DATA_SET_CHANGED);
        mControlHandler.sendEmptyMessageDelayed(MSG_NOTIFY_DATA_SET_CHANGED, 400);
    }
    
    public void invalidateScrollbar() {
        
        if (mFastScrollView == null) {
            invalidate(mScrollbarHolder.getScrollBar().getInvalidateRect());
            if (mScrollbarHolder.getScrollBar().shouldInvalidatePopup())
                invalidate(mScrollbarHolder.getScrollBar().getPopupInvalidateRect());
        } else {
            mFastScrollView.invalidate(mScrollbarHolder.getScrollBar().getInvalidateRect());
            if (mScrollbarHolder.getScrollBar().shouldInvalidatePopup()) {
                mFastScrollView.invalidate(mScrollbarHolder.getScrollBar().getPopupInvalidateRect());
            }
        }
    }
    
    public void setFastScrollListener(FastScrollListener fastScrollListener) {
        mFastScrollListener = fastScrollListener;
    }
    
    public boolean isFastScrollVisible() {
        return mScrollbarHolder.isVisible();
    }
    
    public void setFastScrollVisible(boolean fastScrollVisible) {
        mScrollbarHolder.setVisible(fastScrollVisible);
        if (mFastScrollView != null)
            mFastScrollView.setVisibility(fastScrollVisible ? VISIBLE : GONE);
    }
    
    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (mFastScrollView != null) {
            mFastScrollView.setVisibility(visibility);
        }
    }
}
