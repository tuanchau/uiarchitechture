package com.notabasement.fastscrollrecyclerview;

/**
 * Created by tuanchauict on 5/26/17.
 */
public interface SectionedAdapter {
    String getSectionName(int position);
}
