package com.tuanchauict.uiarchitechture;

import android.os.Bundle;

import com.orhanobut.logger.LLogger;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends BaseActivity {

    private static List<Integer> BUTTONS = Arrays.asList(
            R.id.test_0,
            R.id.test_1,
            R.id.test_2,
            R.id.test_3,
            R.id.test_4,
            R.id.test_5,
            R.id.test_6,
            R.id.test_7,
            R.id.test_8,
            R.id.test_9,
            R.id.test_10,
            R.id.test_11,
            R.id.test_12,
            R.id.test_13,
            R.id.test_14,
            R.id.test_15,
            R.id.test_16,
            R.id.test_17
    );


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LLogger.compactOn();
        setContentView(R.layout.activity_main);

        for (int btn : BUTTONS) {
            findViewById(btn).setOnClickListener(view -> {
                startActivity(TestActivity.newIntent(this, BUTTONS.indexOf(btn)));
            });
        }
    }


}
