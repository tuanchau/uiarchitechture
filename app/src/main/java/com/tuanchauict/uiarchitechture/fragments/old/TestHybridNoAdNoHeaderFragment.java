package com.tuanchauict.uiarchitechture.fragments.old;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.ofmonsters.baseui.adapter.AdMasterAdapter;
import com.ofmonsters.baseui.list.ArrayItemProvider;
import com.ofmonsters.baseui.list.DataProvider;
import com.ofmonsters.baseui.list.FooterProvider;
import com.ofmonsters.baseui.list.MasterList;
import com.ofmonsters.baseui.list.MasterLoaderResponse;
import com.ofmonsters.baseui.list.Section;
import com.ofmonsters.baseui.list.placement.AdPositionGenerator;
import com.ofmonsters.baseui.list.placement.ItemPlacement;
import com.tuanchauict.uiarchitechture.StickSectionFragment;
import com.tuanchauict.uiarchitechture.adapter.FakeGridItem;
import com.tuanchauict.uiarchitechture.adapter.ExampleGridAdapter;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

import static com.tuanchauict.uiarchitechture.adapter.FakeFooterHolder.TYPE_ALL_ITEMS_SHOWN;
import static com.tuanchauict.uiarchitechture.adapter.FakeFooterHolder.TYPE_LOADING;

/**
 * Created by tuanchauict on 10/12/16.
 */

public class TestHybridNoAdNoHeaderFragment extends StickSectionFragment {


    @NonNull
    @Override
    protected Observable<MasterLoaderResponse> prepareLoader(int page) {
        List<FakeGridItem> data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new FakeGridItem("Item " + (getCurrentPage() * 20 + i)));
        }
        DataProvider provider = DataProvider.newProvider(data);
        MasterLoaderResponse response = new MasterLoaderResponse(isRefreshing() || getCurrentPage() == 0, false, getCurrentPage() >= 10, provider);
        return Observable.just(response);
    }

    @Nullable
    @Override
    protected Observable<List<Section>> generateSection(List<DataProvider> providers) {
        return Observable.just(null);
    }

    @NonNull
    @Override
    protected Observable<MasterList> produceMasterList(List<DataProvider> providers,
                                                       List<Section> sections,
                                                       AdPositionGenerator adGenerator,
                                                       FooterProvider.FooterItem item) {
        return ItemPlacement.generate(providers, sections,
                getNumberColumns(getOrientation()), adGenerator, item);
    }

    @Override
    protected AdMasterAdapter newAdapter() {
        ExampleGridAdapter adapter = new ExampleGridAdapter(getContext(), null, null);
        return adapter;
    }


    @Override
    protected FooterProvider.FooterItem getFooterItem(boolean hasNextPage, boolean willEmpty) {
        if(willEmpty)
            return null;

        if(hasNextPage){
            return new FooterProvider.FlexColumnFooterItem(0, 1, TYPE_LOADING);
        } else {
            return new FooterProvider.FixColumnFooterItem(0, getNumberColumns(getOrientation()), TYPE_ALL_ITEMS_SHOWN);
        }
    }

    @Override
    protected void onRequestRefreshList() {
        hideRefreshing();
    }

    @Override
    public boolean shouldLoadNextPage(int total, int bottom) {
        return bottom + 5 >= total;
    }

    @Override
    public boolean isAllowPaging() {
        return true;
    }
}
