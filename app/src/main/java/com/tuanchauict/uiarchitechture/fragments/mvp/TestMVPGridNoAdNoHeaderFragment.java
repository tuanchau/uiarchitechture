package com.tuanchauict.uiarchitechture.fragments.mvp;

import android.support.annotation.NonNull;

import com.ofmonsters.baseui.adapter.AbstractMasterAdapter;
import com.ofmonsters.baseui.listfragment.ListFragmentContract;
import com.tuanchauict.uiarchitechture.BaseMangaGridFragment;
import com.tuanchauict.uiarchitechture.adapter.ExampleGridAdapter;
import com.tuanchauict.uiarchitechture.tmp.NativeAdHandler;

/**
 * Created by tuanchauict on 3/27/17.
 */

public class TestMVPGridNoAdNoHeaderFragment extends BaseMangaGridFragment {
    
    ListFragmentContract.CPresenter mPresenter = new TestPresenter(this);
    
    @NonNull
    @Override
    public AbstractMasterAdapter newAdapter() {
        ExampleGridAdapter adapter = new ExampleGridAdapter(getContext(), null, new NativeAdHandler());
        
        return adapter;
    }
    
    @Override
    public ListFragmentContract.CPresenter getPresenter() {
        return mPresenter;
    }
}
