package com.tuanchauict.uiarchitechture.fragments.mvp;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.ofmonsters.baseui.list.ArrayItemProvider;
import com.ofmonsters.baseui.list.DataProvider;
import com.ofmonsters.baseui.list.FooterProvider;
import com.ofmonsters.baseui.list.MasterList;
import com.ofmonsters.baseui.list.MasterLoaderResponse;
import com.ofmonsters.baseui.list.Section;
import com.ofmonsters.baseui.list.placement.AdPositionGenerator;
import com.ofmonsters.baseui.list.placement.ItemPlacement;
import com.ofmonsters.baseui.listfragment.BasePresenter;
import com.ofmonsters.baseui.listfragment.ListFragmentContract;
import com.tuanchauict.uiarchitechture.adapter.FakeGridItem;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import rx.Observable;

/**
 * Created by tuanchauict on 3/28/17.
 */

public class TestPresenter extends BasePresenter {
    public TestPresenter(@NonNull ListFragmentContract.CView<ListFragmentContract.CPresenter> view) {
        super(view);
    }
    
    @Override
    public boolean allowPaging() {
        return false;
    }
    
    AtomicInteger count = new AtomicInteger(0);
    
    @NonNull
    @Override
    protected Observable<MasterLoaderResponse> prepareLoader(int offset, int length) {
        return Observable.fromCallable(() -> {
            Thread.sleep(5000);
            List<FakeGridItem> data = new ArrayList<>();
            for (int i = 0; i < 300; i++) {
                data.add(new FakeGridItem("Item " + count.get() + "-" + i));
            }
            count.incrementAndGet();
            return data;
        }).map(list -> {
            DataProvider provider = DataProvider.newProvider(list);
            MasterLoaderResponse response = new MasterLoaderResponse(provider);
            return response;
        });
    }
    
    @Nullable
    @Override
    protected Observable<List<Section>> generateSection(List<DataProvider> providers) {
        return Observable.fromCallable(() -> {
            List<Section> result = new ArrayList<Section>();
            for (int i = 0; i < providers.get(0).size(); i += 23) {
                result.add(new Section(23, "Section " + result.size(), "S" + result.size()));
            }
            return result;
//            return null;
        });
    }
    
    @NonNull
    @Override
    protected Observable<MasterList> produceMasterList(List<DataProvider> providers,
                                                       @Nullable List<Section> sections,
                                                       @Nullable AdPositionGenerator adGenerator,
                                                       @Nullable FooterProvider.FooterItem item,
                                                       int column) {
        return ItemPlacement.generate(providers, sections, column, adGenerator, item);
    }
}
