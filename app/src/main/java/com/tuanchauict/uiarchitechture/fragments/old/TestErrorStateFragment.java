package com.tuanchauict.uiarchitechture.fragments.old;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ofmonsters.baseui.adapter.AbstractMasterAdapter;
import com.ofmonsters.baseui.listfragment.BaseGridFragment;
import com.ofmonsters.baseui.list.DataProvider;
import com.ofmonsters.baseui.list.FooterProvider;
import com.ofmonsters.baseui.list.MasterList;
import com.ofmonsters.baseui.list.MasterLoaderResponse;
import com.ofmonsters.baseui.list.Section;
import com.ofmonsters.baseui.list.placement.AdPositionGenerator;
import com.tuanchauict.uiarchitechture.R;

import java.util.List;

import rx.Observable;

/**
 * Created by tuanchauict on 11/1/16.
 */

public class TestErrorStateFragment extends BaseGridFragment {
    @NonNull
    @Override
    protected Observable<MasterLoaderResponse> prepareLoader(int page) {
        return Observable.just(null);
    }

    @Nullable
    @Override
    protected Observable<List<Section>> generateSection(List<DataProvider> providers) {
        return null;
    }

    @NonNull
    @Override
    protected Observable<MasterList> produceMasterList(List<DataProvider> providers, @Nullable List<Section> sections, @Nullable AdPositionGenerator adGenerator, @Nullable FooterProvider.FooterItem item) {
        return Observable.just(new MasterList());
    }

    @Override
    protected AbstractMasterAdapter newAdapter() {
        return null;
    }

    @Override
    protected void onRequestRefreshList() {
        hideRefreshing();
    }

    @Override
    protected View getEmptyView(ViewGroup parent) {
        return LayoutInflater.from(getContext()).inflate(R.layout.view_empty_state, parent, false);
    }

    @Override
    protected View getErrorView(ViewGroup parent) {
        return LayoutInflater.from(getContext()).inflate(R.layout.view_error_state, parent, false);
    }
}
