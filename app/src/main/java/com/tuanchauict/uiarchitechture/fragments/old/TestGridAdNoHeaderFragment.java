package com.tuanchauict.uiarchitechture.fragments.old;

import com.ofmonsters.baseui.list.placement.AdPositionGenerator;
import com.ofmonsters.baseui.list.placement.SimpleItemStyleRangeAdPositionGenerator;
import com.orhanobut.logger.LLogger;

/**
 * Created by tuanchauict on 9/21/16.
 */

public class TestGridAdNoHeaderFragment extends TestGridNoAdNoHeaderFragment {
    private static final LLogger LOG = LLogger.getLocalLogger().withTag("TESTGRID").on();

    @Override
    protected AdPositionGenerator getAdPositionGenerator() {
        return new SimpleItemStyleRangeAdPositionGenerator(3, 4, 2, 0, 1);
    }
}
