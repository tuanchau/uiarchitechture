package com.tuanchauict.uiarchitechture.fragments.old;

import com.ofmonsters.baseui.adapter.AdMasterAdapter;
import com.ofmonsters.baseui.list.placement.AdPositionGenerator;
import com.ofmonsters.baseui.list.placement.SimpleListStyleRangeAdPositionGenerator;
import com.tuanchauict.uiarchitechture.adapter.ExampleGridAdapter;
import com.tuanchauict.uiarchitechture.tmp.NativeAdHandler;

/**
 * Created by tuanchauict on 10/12/16.
 */

public class TestHybridAdNoHeaderFragment extends TestHybridNoAdNoHeaderFragment {

    @Override
    protected AdMasterAdapter newAdapter() {
        ExampleGridAdapter adapter = new ExampleGridAdapter(getContext(), null, new NativeAdHandler());

        return adapter;
    }

    @Override
    protected AdPositionGenerator getAdPositionGenerator() {
        return new SimpleListStyleRangeAdPositionGenerator(3, 4, 2, 0);
    }
}
