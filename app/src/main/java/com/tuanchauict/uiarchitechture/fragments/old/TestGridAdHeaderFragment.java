package com.tuanchauict.uiarchitechture.fragments.old;

import android.support.annotation.Nullable;

import com.ofmonsters.baseui.list.DataProvider;
import com.ofmonsters.baseui.list.Section;
import com.ofmonsters.baseui.list.placement.AdPositionGenerator;
import com.ofmonsters.baseui.list.placement.SimpleItemStyleRangeAdPositionGenerator;
import com.orhanobut.logger.LLogger;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by tuanchauict on 9/21/16.
 */

public class TestGridAdHeaderFragment extends TestGridNoAdNoHeaderFragment {
    private static final LLogger LOG = LLogger.getLocalLogger().withTag("TESTGRID").on();

    @Nullable
    @Override
    protected Observable<List<Section>> generateSection(List<DataProvider> providers) {
        return Observable.create(subscriber -> {
            List<Section> result = new ArrayList<Section>();
            for (int i = 0; i < providers.get(0).size(); i += 23) {
                result.add(new Section(23, "Section " + result.size(), "S" + i));
            }
            subscriber.onNext(result);
            subscriber.onCompleted();
        });
    }

    @Override
    protected AdPositionGenerator getAdPositionGenerator() {
        return new SimpleItemStyleRangeAdPositionGenerator(3, 4, 2, -1, 1);
    }
}
