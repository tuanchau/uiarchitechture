package com.tuanchauict.uiarchitechture.fragments.old;

import android.support.annotation.Nullable;

import com.orhanobut.logger.LLogger;
import com.ofmonsters.baseui.list.DataProvider;
import com.ofmonsters.baseui.list.Section;
import com.ofmonsters.baseui.list.placement.AdPositionGenerator;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by tuanchauict on 9/21/16.
 */

public class TestGridNoAdHeaderFragment extends TestGridNoAdNoHeaderFragment {
    private static final LLogger LOG = LLogger.getLocalLogger().withTag("TESTGRID").on();

    @Nullable
    @Override
    protected Observable<List<Section>> generateSection(List<DataProvider> providers) {
        return Observable.create(subscriber -> {
            List<Section> result = new ArrayList<Section>();
            for (int i = 0; i < providers.get(0).size(); i += 23) {
                result.add(new Section(23, "Section " + result.size(), "S" + result.size()));
            }
            subscriber.onNext(result);
            subscriber.onCompleted();
        });
    }

    @Override
    protected AdPositionGenerator getAdPositionGenerator() {
        return null;
    }
}
