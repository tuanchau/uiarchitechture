package com.tuanchauict.uiarchitechture.fragments.old;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;

import com.ofmonsters.baseui.adapter.AdMasterAdapter;
import com.ofmonsters.baseui.listfragment.SearchableGridCatalogFragment;
import com.ofmonsters.baseui.list.ArrayItemProvider;
import com.ofmonsters.baseui.list.DataProvider;
import com.ofmonsters.baseui.list.FooterProvider;
import com.ofmonsters.baseui.list.MasterList;
import com.ofmonsters.baseui.list.MasterLoaderResponse;
import com.ofmonsters.baseui.list.Section;
import com.ofmonsters.baseui.list.placement.AdPositionGenerator;
import com.ofmonsters.baseui.list.placement.ItemPlacement;
import com.orhanobut.logger.Logger;
import com.tuanchauict.uiarchitechture.adapter.FakeGridItem;
import com.tuanchauict.uiarchitechture.R;
import com.tuanchauict.uiarchitechture.adapter.SearchableGridAdapter;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

import static com.tuanchauict.uiarchitechture.adapter.FakeFooterHolder.TYPE_ALL_ITEMS_SHOWN;
import static com.tuanchauict.uiarchitechture.adapter.FakeFooterHolder.TYPE_LOADING;

/**
 * Created by tuanchauict on 10/2/16.
 */

public class TestSearchNoAdNoHeaderFragment extends SearchableGridCatalogFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @NonNull
    @Override
    protected Observable<MasterLoaderResponse> prepareLoader(int page) {
        List<FakeGridItem> data = new ArrayList<>();
        Logger.d("searchString = %s", getSearchString());
        for (int i = 0; i < 10; i++) {
            String text = "Item " + i;
            if (TextUtils.isEmpty(getSearchString()) || text.contains(getSearchString()))
                data.add(new FakeGridItem(text));
        }
        DataProvider provider = DataProvider.newProvider(data);

        MasterLoaderResponse response = new MasterLoaderResponse(false, false, false, provider);
        response.setShouldClear(
                !isAllowPaging() || page == 0
        );
        return Observable.just(response);
    }

    @Nullable
    @Override
    protected Observable<List<Section>> generateSection(List<DataProvider> providers) {
        return Observable.just(null);
    }

    @NonNull
    @Override
    protected Observable<MasterList> produceMasterList(
            List<DataProvider> providers,
            List<Section> sections,
            AdPositionGenerator adGenerator,
            FooterProvider.FooterItem item) {
        return Observable.create(subscriber -> {
            if (subscriber.isUnsubscribed())
                return;
            MasterList masterList = ItemPlacement.generate(
                    providers,
                    sections,
                    getNumberColumns(getOrientation()),
                    getAdPositionGenerator(),
                    item
            ).toBlocking().first();
            subscriber.onNext(masterList);
            subscriber.onCompleted();
        });
    }

    @Override
    protected AdMasterAdapter newAdapter() {
        return new SearchableGridAdapter(getContext(), null, null);
    }

    @Override
    protected FooterProvider.FooterItem getFooterItem(boolean hasNextPage, boolean willEmpty) {
        if(willEmpty)
            return null;

        if(hasNextPage){
            return new FooterProvider.FlexColumnFooterItem(0, 1, TYPE_LOADING);
        } else {
            return new FooterProvider.FixColumnFooterItem(0, getNumberColumns(getOrientation()), TYPE_ALL_ITEMS_SHOWN);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (!isAdded()) {
            return;
        }

        menu.clear();
        inflater.inflate(R.menu.search_test, menu);

        setupSearchAction(menu);
    }

    @Override
    protected int getMenuSearchItemId() {
        return R.id.action_search;
    }

    @Override
    protected void onRequestRefreshList() {
        hideRefreshing();
    }

    @Override
    public void saveNewSearchString(String searchString) {

    }

    @Override
    public List<String> getRecentSearchStrings() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add("Recent " + i);
        }
        return list;
    }

    @Override
    public void clearSearchStrings() {

    }
}
