package com.tuanchauict.uiarchitechture.fragments.old;

import android.support.annotation.Nullable;
import android.view.View;

import com.ofmonsters.baseui.adapter.AdMasterAdapter;
import com.ofmonsters.baseui.list.DataProvider;
import com.ofmonsters.baseui.list.Section;
import com.ofmonsters.baseui.list.placement.AdPositionGenerator;
import com.ofmonsters.baseui.list.placement.SimpleListStyleRangeAdPositionGenerator;
import com.tuanchauict.uiarchitechture.adapter.ExampleGridAdapter;
import com.tuanchauict.uiarchitechture.adapter.OnCatalogItemClickListener;
import com.tuanchauict.uiarchitechture.tmp.NativeAdHandler;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by tuanchauict on 10/12/16.
 */

public class TestHybridAdHeaderFragment extends TestHybridNoAdNoHeaderFragment {

    @Nullable
    @Override
    protected Observable<List<Section>> generateSection(List<DataProvider> providers) {
        List<Section> result = new ArrayList<Section>();
        int size = 0;
        for(DataProvider d: providers){
            size += d.size();
        }
        for (int i = 0; i < size; i += 23) {
            result.add(new Section(23, "Section " + result.size(), "S" + result.size()));
        }
        return Observable.just(result);
    }

    @Override
    protected AdMasterAdapter newAdapter() {
        ExampleGridAdapter adapter = new ExampleGridAdapter(getContext(), null, new NativeAdHandler());
        adapter.setOnItemClickListener(new OnCatalogItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
        
            }
    
            @Override
            public void onMoreButtonClick(View view, int position) {
        
            }
        });
        return adapter;
    }

    @Override
    protected AdPositionGenerator getAdPositionGenerator() {
        return new SimpleListStyleRangeAdPositionGenerator(3, 4, 2, 0);
    }
}
