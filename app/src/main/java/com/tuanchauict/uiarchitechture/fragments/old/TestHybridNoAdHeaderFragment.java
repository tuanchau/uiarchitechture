package com.tuanchauict.uiarchitechture.fragments.old;

import android.support.annotation.Nullable;

import com.ofmonsters.baseui.adapter.AdMasterAdapter;
import com.ofmonsters.baseui.list.DataProvider;
import com.ofmonsters.baseui.list.Section;
import com.tuanchauict.uiarchitechture.adapter.ExampleGridAdapter;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by tuanchauict on 10/12/16.
 */

public class TestHybridNoAdHeaderFragment extends TestHybridNoAdNoHeaderFragment {

    @Nullable
    @Override
    protected Observable<List<Section>> generateSection(List<DataProvider> providers) {
        List<Section> result = new ArrayList<Section>();
        int size = 0;
        for (DataProvider d : providers) {
            size += d.size();
        }
        for (int i = 0; i < size; i += 23) {
            result.add(new Section(23, "Section " + result.size(), "S" + result.size()));
        }
        return Observable.just(result);
    }

    @Override
    protected AdMasterAdapter newAdapter() {
        ExampleGridAdapter adapter = new ExampleGridAdapter(getContext(), null, null);

        return adapter;
    }

}
