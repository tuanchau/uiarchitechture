package com.tuanchauict.uiarchitechture.fragments.old;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.ofmonsters.baseui.adapter.AdMasterAdapter;
import com.ofmonsters.baseui.list.ArrayItemProvider;
import com.ofmonsters.baseui.list.DataProvider;
import com.ofmonsters.baseui.list.FooterProvider;
import com.ofmonsters.baseui.list.MasterList;
import com.ofmonsters.baseui.list.MasterLoaderResponse;
import com.ofmonsters.baseui.list.Section;
import com.ofmonsters.baseui.list.placement.AdPositionGenerator;
import com.ofmonsters.baseui.list.placement.ItemPlacement;
import com.orhanobut.logger.LLogger;
import com.tuanchauict.uiarchitechture.StickSectionFragment;
import com.tuanchauict.uiarchitechture.adapter.FakeGridItem;
import com.tuanchauict.uiarchitechture.adapter.OnCatalogItemClickListener;
import com.tuanchauict.uiarchitechture.adapter.ExampleGridAdapter;
import com.tuanchauict.uiarchitechture.tmp.NativeAdHandler;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * Created by tuanchauict on 9/21/16.
 */

public class TestGridNoAdNoHeaderFragment extends StickSectionFragment {
    private static final LLogger LOG = LLogger.getLocalLogger().withTag("TESTGRID").on();

    @NonNull
    @Override
    protected Observable<MasterLoaderResponse> prepareLoader(int page) {
        List<FakeGridItem> data = new ArrayList<>();
        for (int i = 0; i < 400; i++) {
            data.add(new FakeGridItem("Item " + i));
        }
        DataProvider provider = DataProvider.newProvider(data);
        MasterLoaderResponse response = new MasterLoaderResponse(provider);
        return Observable.just(response);
    }

    @Nullable
    @Override
    protected Observable<List<Section>> generateSection(List<DataProvider> providers) {
        return Observable.just(null);
    }

    @NonNull
    @Override
    protected Observable<MasterList> produceMasterList(
            List<DataProvider> providers, List<Section> sections, AdPositionGenerator adGenerator, FooterProvider.FooterItem item) {
        return Observable.create(subscriber -> {
            if (subscriber.isUnsubscribed())
                return;
            //TODO change item placement's generate to accept list of providers
            MasterList masterList = ItemPlacement.generate(
                    providers,
                    sections,
                    getNumberColumns(getOrientation()),
                    adGenerator,
                    item
            ).toBlocking().first();
            subscriber.onNext(masterList);
            subscriber.onCompleted();
        });
    }

    @Override
    protected AdMasterAdapter newAdapter() {
        ExampleGridAdapter adapter = new ExampleGridAdapter(getContext(), null, new NativeAdHandler());

        adapter.setOnItemClickListener(new OnCatalogItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                LOG.d("item click: %s", position);
                removeItem(position);
            }

            @Override
            public void onMoreButtonClick(View view, int position) {
                LOG.d("item more click: %s", position);
            }
        });
        return adapter;
    }

    @Override
    protected void onRequestRefreshList() {
        hideRefreshing();
    }

}
