package com.tuanchauict.uiarchitechture.tmp;

/**
 * Created by tuanchauict on 4/30/16.
 */
public class AdConfig {
    private static final String PREF_AD_CONFIG = "ad-config-json";
    private static AdConfig sInstance;


    private static LocalAd sDefaultLocalAd;

    public static LocalAd getLocalAd() {
        return new LocalAd();
    }

    private static LocalAd defaultLocalAd() {
        if (sDefaultLocalAd == null) {
            sDefaultLocalAd = new LocalAd();
        }
        return sDefaultLocalAd;
    }

    public static long getNativeRefreshRate() {
        return 30000L;
    }


    public static class LocalAd {
        private String mAppLink;
        private String mBannerUrl;
        private String mContextText;
        private String mCTA;
        private String mIconUrl;
        private String mPromoText;
        private String mTitleText;

        public String getAppLink() {
            return mAppLink;
        }

        public String getBannerUrl() {
            return mBannerUrl;
        }

        public String getContextText() {
            return mContextText;
        }

        public String getCTA() {
            return mCTA;
        }

        public String getIconUrl() {
            return mIconUrl;
        }

        public String getPromoText() {
            return mPromoText;
        }

        public String getTitleText() {
            return mTitleText;
        }
    }
}
