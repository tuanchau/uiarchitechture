package com.tuanchauict.uiarchitechture.tmp;


import android.content.Context;
import android.view.View;

import com.ofmonsters.baseui.adapter.AdEntry;
import com.ofmonsters.baseui.adapter.AbstractAdHolder;
import com.orhanobut.logger.Logger;


/**
 * Created by tuanchauict on 12/2/15.
 */
public class LocalAdEntry extends AdEntry implements View.OnClickListener {
    protected String mAppLink;
    protected String mBannerUrl;
    protected String mContextText;
    protected String mCTA;
    protected String mIconUrl;
    protected String mPromoText;
    protected String mTitleText;

    protected Context mContext;

    protected AbstractAdHolder mHolder;

    public LocalAdEntry(Context context) {
        mContext = context;
        AdConfig.LocalAd localAd = AdConfig.getLocalAd();

        mAppLink = localAd.getAppLink();
        mBannerUrl = localAd.getBannerUrl();
        mContextText = localAd.getContextText();
        mCTA = localAd.getCTA();
        mIconUrl = localAd.getIconUrl();
        mPromoText = localAd.getPromoText();
        mTitleText = localAd.getTitleText();
    }

    @Override
    public void render(AbstractAdHolder holder) {
//        Logger.d("render ad local");
        mHolder = holder;
//        holder.setSponsoredText();
//        holder.setMainImage(mBannerUrl);
//        holder.setIcon(mIconUrl);
//        holder.setTitle(mTitleText);
//        holder.setContext(mContextText);
//        holder.setSponsored(R.string.ad_sponsored);
//        holder.setBody(mPromoText);
//        holder.setCTAText(mCTA);

        holder.itemView.setVisibility(View.VISIBLE);
        holder.itemView.setOnClickListener(this);
    }

    public String getAppLink() {
        return mAppLink;
    }

    @Override
    public void releaseAd() {
        super.releaseAd();
        if (mHolder != null) {
            mHolder.itemView.setOnTouchListener(null);
            mHolder.itemView.setOnClickListener(null);
//            mHolder.mInteractionView2.setVisibility(View.GONE);
//            mHolder.mInteractionView2.setOnClickListener(null);
        }
    }

    @Override
    public void setContext(Context context) {
        mContext = context;
    }
    
    @Override
    public void onClick(View v) {
        Logger.d("click");
    }

    @Override
    public boolean shouldCache() {
        return false;
    }
}
