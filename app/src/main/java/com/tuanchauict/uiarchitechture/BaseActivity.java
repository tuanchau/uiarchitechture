package com.tuanchauict.uiarchitechture;

import android.content.Context;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

/**
 * Created by tuanchauict on 9/18/16.
 */
public abstract class BaseActivity extends RxAppCompatActivity {

    //region Fragment control
    public void addFragment(Fragment fragment, int containerId, boolean addToBackStack, String tag) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(containerId, fragment, tag);
        if (addToBackStack) ft.addToBackStack(tag);
        ft.commit();
    }

    public Fragment findFragment(String tag) {
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    public void replaceFragment(Fragment fragment, int containerId, String tag) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(containerId, fragment, tag);
        ft.commit();
    }

    public void removeFragment(Fragment fragment, boolean removeFromBackStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.remove(fragment);
        ft.commit();
        if (removeFromBackStack) {
            getSupportFragmentManager().popBackStack();
        }
    }
    //endregion

    public void hideKeyboard(View focusedView) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
    }

    public boolean isFinishing(){
        if(Build.VERSION.SDK_INT >= 17){
            return super.isDestroyed() || super.isFinishing();
        } else {
            return super.isFinishing();
        }
    }

    public <T extends View> T findView(@IdRes int id){
        View v = findViewById(id);
        if(v == null)
            return null;
        return (T) v;
    }


}
