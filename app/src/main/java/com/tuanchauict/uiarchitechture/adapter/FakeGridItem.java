package com.tuanchauict.uiarchitechture.adapter;

import com.ofmonsters.baseui.list.Identifiable;

/**
 * Created by tuanchauict on 9/22/16.
 */
public class FakeGridItem implements Identifiable {
    public String title;

    public FakeGridItem(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public int getId() {
        return hashCode();
    }

}
