package com.tuanchauict.uiarchitechture.adapter;

import android.view.View;
import android.widget.TextView;

import com.ofmonsters.baseui.adapter.AbstractCatalogViewHolder;
import com.tuanchauict.uiarchitechture.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by tuanchauict on 10/25/16.
 */

public class FakeGridHolder extends AbstractCatalogViewHolder<OnCatalogItemClickListener> {
    @Bind(R.id.title)
    TextView mTitle;

    public FakeGridHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }

    @Override
    public void render(Object data, boolean selectionMode, boolean selected) {
        FakeGridItem item = (FakeGridItem) data;
        mTitle.setText(item.getTitle());
    }
}
