package com.tuanchauict.uiarchitechture.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.notabasement.fastscrollrecyclerview.EstimateScrollerAdapter;
import com.notabasement.fastscrollrecyclerview.SectionedAdapter;
import com.ofmonsters.baseui.adapter.AbstractCatalogViewHolder;
import com.ofmonsters.baseui.adapter.AbstractFooterHolder;
import com.ofmonsters.baseui.adapter.AbstractSectionViewHolder;
import com.ofmonsters.baseui.adapter.AdMasterAdapter;
import com.ofmonsters.baseui.list.Item;
import com.ofmonsters.baseui.list.MasterList;
import com.ofmonsters.baseui.list.Section;
import com.tuanchauict.uiarchitechture.R;

/**
 * Created by tuanchauict on 10/24/16.
 */

public class ExampleGridAdapter extends AdMasterAdapter<OnCatalogItemClickListener> implements EstimateScrollerAdapter, SectionedAdapter {
    public ExampleGridAdapter(Context context, @Nullable MasterList masterList, @Nullable NativeAdHandler adHandler) {
        super(context, masterList, adHandler);
    }
    
    @Override
    public AbstractCatalogViewHolder onCreateItemViewHolder(LayoutInflater inflater, ViewGroup parent) {
        View view = inflater.inflate(R.layout.item_video_grid, parent, false);
        return new FakeGridHolder(view);
    }
    
    @Override
    public AbstractSectionViewHolder onCreateSectionViewHolder(LayoutInflater inflater, ViewGroup parent) {
        View view = inflater.inflate(R.layout.item_header_text, parent, false);
        return new FakeSectionViewHolder(view);
    }
    
    
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateAdViewHolder(LayoutInflater inflater, ViewGroup parent, int adType) {
        if(adType == 0){
            View view = inflater.inflate(R.layout.item_native_injection_ad, parent, false);
            return new FakeAdInjectionHolder(view);
        } else {
            View view = inflater.inflate(R.layout.item_native_item_ad, parent, false);
            return new FakeAdInjectionHolder(view);
        }
    }
    
    @Override
    public void onBindAdViewHolder(RecyclerView.ViewHolder holder, Item item) {
        
    }
    
    @NonNull
    @Override
    public AbstractFooterHolder onCreateFooterViewHolder(LayoutInflater inflater, ViewGroup parent) {
        return new FakeFooterHolder(inflater.inflate(R.layout.item_footer, parent, false));
    }
    
    @Override
    public int getColumn(int position) {
        Item item = getItem(position);
        return item.getColumn();
    }
    
    @Override
    public int getEstimateHeight(int type) {
        float d = getContext().getResources().getDisplayMetrics().density;
        switch (type) {
            case MasterList.TYPE_ITEM:
                return (int) (d * 190);
            case MasterList.TYPE_SECTION:
                return (int) (24 * d);
            case MasterList.TYPE_FOOTER:
                return (int) (50 * d);
            default:
                if (isTypeAd(type)) {
                    int adType = getAdType(type);
                    if (adType == 0) {
                        return (int) (400 * d);
                    } else {
                        return (int) (d * 190);
                    }
                }
            
        }
        return 0;
    }
    
    @Override
    public int getSpacing() {
        return getContext().getResources().getDimensionPixelOffset(R.dimen.thumb_item_spacing);
    }
    
    @NonNull
    @Override
    public String getSectionName(int position) {
        Section section = getSection(position);
        return section != null ? section.title : null;
    }
}
