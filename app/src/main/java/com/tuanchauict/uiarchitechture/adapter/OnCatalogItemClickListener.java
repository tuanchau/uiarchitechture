package com.tuanchauict.uiarchitechture.adapter;

import android.view.View;

/**
 * Created by tuanchauict on 10/26/15.
 */
public interface OnCatalogItemClickListener {
    void onItemClick(View view, int position);

    void onMoreButtonClick(View view, int position);
}
