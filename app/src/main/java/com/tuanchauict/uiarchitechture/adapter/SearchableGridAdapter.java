package com.tuanchauict.uiarchitechture.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ofmonsters.baseui.adapter.AbstractFooterHolder;
import com.ofmonsters.baseui.list.MasterList;
import com.tuanchauict.uiarchitechture.R;

/**
 * Created by tuanchauict on 10/24/16.
 */

public class SearchableGridAdapter extends ExampleGridAdapter {
    public SearchableGridAdapter(Context context, @Nullable MasterList masterList, @Nullable NativeAdHandler adHandler) {
        super(context, masterList, adHandler);
    }

    @Override
    public AbstractFooterHolder onCreateFooterViewHolder(LayoutInflater inflater, ViewGroup parent) {
        return new FakeFooterHolder(inflater.inflate(R.layout.item_footer, parent, false));
    }

}
