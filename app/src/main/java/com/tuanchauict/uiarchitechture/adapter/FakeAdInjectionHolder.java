package com.tuanchauict.uiarchitechture.adapter;

import android.view.View;

import com.ofmonsters.baseui.adapter.AbstractAdHolder;
import com.ofmonsters.baseui.adapter.AdEntry;

/**
 * Created by tuanchauict on 10/25/16.
 */

public class FakeAdInjectionHolder extends AbstractAdHolder {
    public FakeAdInjectionHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void renderAd(AdEntry adEntry) {

    }
}
