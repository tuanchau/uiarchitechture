package com.tuanchauict.uiarchitechture.adapter;

import android.view.View;
import android.widget.TextView;

import com.ofmonsters.baseui.adapter.AbstractSectionViewHolder;
import com.ofmonsters.baseui.list.Section;
import com.tuanchauict.uiarchitechture.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by tuanchauict on 10/25/16.
 */

public class FakeSectionViewHolder extends AbstractSectionViewHolder {

    @Bind(R.id.text)
    TextView mText;

    public FakeSectionViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void render(Section section) {
        mText.setText(section.title);
    }
}
