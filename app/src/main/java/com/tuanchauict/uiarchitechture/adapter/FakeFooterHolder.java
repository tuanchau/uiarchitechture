package com.tuanchauict.uiarchitechture.adapter;

import android.view.View;

import com.ofmonsters.baseui.adapter.AbstractFooterHolder;
import com.tuanchauict.uiarchitechture.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by tuanchauict on 10/28/16.
 */

public class FakeFooterHolder extends AbstractFooterHolder {
    public static final Integer TYPE_LOADING = 1;
    public static final Integer TYPE_ALL_ITEMS_SHOWN = 2;

    @Bind(R.id.loading_container)
    View mLoadingContainer;
    @Bind(R.id.nomore_container)
    View mNoMoreContainer;


    public FakeFooterHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void render(Object data, int column, int spanSize) {
        Integer type = (Integer) data;
        if (type == TYPE_LOADING) {
            mLoadingContainer.setVisibility(View.VISIBLE);
            mNoMoreContainer.setVisibility(View.GONE);
        } else {
            mLoadingContainer.setVisibility(View.GONE);
            mNoMoreContainer.setVisibility(View.VISIBLE);
        }
    }
}
