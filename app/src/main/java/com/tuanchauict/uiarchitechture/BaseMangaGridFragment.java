package com.tuanchauict.uiarchitechture;

import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ofmonsters.baseui.list.Section;
import com.ofmonsters.baseui.listfragment.BaseMVPGridFragment;

/**
 * Created by tuanchauict on 3/27/17.
 */

public abstract class BaseMangaGridFragment extends BaseMVPGridFragment {
    TextView mStickSectionView;
    
    
    @Override
    public int getItemWidth() {
        return getResources().getDimensionPixelOffset(R.dimen.thumb_item_estimate_width);
    }
    
    @Override
    public int getItemHeight() {
        return getResources().getDimensionPixelOffset(R.dimen.thumb_item_estimate_height);
    }
    
    @Override
    public int getItemSpace() {
        return getResources().getDimensionPixelOffset(R.dimen.thumb_item_spacing);
    }
    
    @Override
    public View getErrorView(ViewGroup parent) {
        return LayoutInflater.from(getContext()).inflate(R.layout.view_error_state, parent, false);
    }
    
    @Override
    public View getEmptyView(ViewGroup parent) {
        return LayoutInflater.from(getContext()).inflate(R.layout.view_empty_state, parent, false);
    }
    
    @Override
    public int getViewLayoutId() {
        return R.layout.frag_grid_layout;
    }
    
    @Override
    public View getStickSectionView(FrameLayout parent) {
        View stickSectionView = LayoutInflater.from(getContext()).inflate(R.layout.item_header_text, parent, false);
        mStickSectionView = findView(stickSectionView, R.id.text);
        return stickSectionView;
    }
    
    @Override
    public void updateStickSection(@Nullable Section section) {
        mStickSectionView.setText(section.title);
    }
}
