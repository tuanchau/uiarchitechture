package com.tuanchauict.uiarchitechture;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.orhanobut.logger.Logger;
import com.tuanchauict.uiarchitechture.fragments.mvp.TestMVPGridNoAdNoHeaderFragment;
import com.tuanchauict.uiarchitechture.fragments.old.TestEmptyStateFragment;
import com.tuanchauict.uiarchitechture.fragments.old.TestErrorStateFragment;
import com.tuanchauict.uiarchitechture.fragments.old.TestGridAdHeaderFragment;
import com.tuanchauict.uiarchitechture.fragments.old.TestGridAdNoHeaderFragment;
import com.tuanchauict.uiarchitechture.fragments.old.TestGridNoAdHeaderFragment;
import com.tuanchauict.uiarchitechture.fragments.old.TestGridNoAdNoHeaderFragment;
import com.tuanchauict.uiarchitechture.fragments.old.TestHybridAdHeaderFragment;
import com.tuanchauict.uiarchitechture.fragments.old.TestHybridAdNoHeaderFragment;
import com.tuanchauict.uiarchitechture.fragments.old.TestHybridNoAdHeaderFragment;
import com.tuanchauict.uiarchitechture.fragments.old.TestHybridNoAdNoHeaderFragment;
import com.tuanchauict.uiarchitechture.fragments.old.TestSearchNoAdNoHeaderFragment;

/**
 * Created by tuanchauict on 10/26/16.
 */

public class TestActivity extends BaseActivity {

    static{
        Logger.compactOn();
    }


    public static final int TEST_NO_AD_NO_HEADER = 0;
    public static final int TEST_AD_NO_HEADER = 1;
    public static final int TEST_AD_HEADER = 2;
    public static final int TEST_NO_AD_HEADER = 3;
    public static final int TEST_HYBRID_NO_AD_NO_HEADER = 4;
    public static final int TEST_HYBRID_AD_NO_HEADER = 5;
    public static final int TEST_HYBRID_AD_HEADER = 6;
    public static final int TEST_HYBRID_NO_AD_HEADER = 7;
    public static final int TEST_SEARCH_NO_AD_NO_HEADER = 8;
    public static final int TEST_SEARCH_AD_NO_HEADER = 9;
    public static final int TEST_SEARCH_AD_HEADER = 10;
    public static final int TEST_SEARCH_NO_AD_HEADER = 11;
    public static final int TEST_SEARCH_HYBRID_NO_AD_NO_HEADER = 12;
    public static final int TEST_SEARCH_HYBRID_AD_NO_HEADER = 13;
    public static final int TEST_SEARCH_HYBRID_AD_HEADER = 14;
    public static final int TEST_SEARCH_HYBRID_NO_AD_HEADER = 15;
    public static final int TEST_EMPTY = 16;
    public static final int TEST_ERROR = 17;

    public static Intent newIntent(Context context, int testCase) {
        Intent intent = new Intent(context, TestActivity.class);
        intent.putExtra("testcase", testCase);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        int testcase = getIntent() != null ? getIntent().getIntExtra("testcase", 0) : TEST_AD_HEADER;
//        testcase = TEST_NO_AD_NO_HEADER;
        Fragment fragment = null;
        switch (testcase) {
            case TEST_NO_AD_NO_HEADER:
                fragment = new TestMVPGridNoAdNoHeaderFragment();
                break;
            case TEST_AD_NO_HEADER:
                fragment = new TestGridAdNoHeaderFragment();
                break;
            case TEST_AD_HEADER:
                fragment = new TestGridAdHeaderFragment();
                break;
            case TEST_NO_AD_HEADER:
                fragment = new TestGridNoAdHeaderFragment();
                break;

            case TEST_HYBRID_NO_AD_NO_HEADER:
                fragment = new TestHybridNoAdNoHeaderFragment();
                break;
            case TEST_HYBRID_AD_NO_HEADER:
                fragment = new TestHybridAdNoHeaderFragment();
                break;
            case TEST_HYBRID_AD_HEADER:
                fragment = new TestHybridAdHeaderFragment();
                break;
            case TEST_HYBRID_NO_AD_HEADER:
                fragment = new TestHybridNoAdHeaderFragment();
                break;

            case TEST_SEARCH_NO_AD_NO_HEADER:
                fragment = new TestSearchNoAdNoHeaderFragment();
                break;
            case TEST_SEARCH_AD_NO_HEADER:
                fragment = new TestSearchNoAdNoHeaderFragment();
                break;
            case TEST_SEARCH_AD_HEADER:
                fragment = new TestSearchNoAdNoHeaderFragment();
                break;
            case TEST_SEARCH_NO_AD_HEADER:
                fragment = new TestSearchNoAdNoHeaderFragment();
                break;

            case TEST_SEARCH_HYBRID_NO_AD_NO_HEADER:
                fragment = new TestGridNoAdNoHeaderFragment();
                break;
            case TEST_SEARCH_HYBRID_AD_NO_HEADER:
                fragment = new TestGridAdNoHeaderFragment();
                break;
            case TEST_SEARCH_HYBRID_AD_HEADER:
                fragment = new TestGridAdHeaderFragment();
                break;
            case TEST_SEARCH_HYBRID_NO_AD_HEADER:
                fragment = new TestGridAdNoHeaderFragment();
                break;
            case TEST_EMPTY:
                fragment = new TestEmptyStateFragment();
                break;
            case TEST_ERROR:
                fragment = new TestErrorStateFragment();

        }
        setTitle(fragment.getClass().getSimpleName());
        replaceFragment(fragment, R.id.fragment_container, "test");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
