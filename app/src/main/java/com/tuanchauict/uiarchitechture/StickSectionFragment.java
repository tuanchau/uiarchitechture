package com.tuanchauict.uiarchitechture;

import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.ofmonsters.baseui.listfragment.BaseGridFragment;
import com.ofmonsters.baseui.list.Section;

/**
 * Created by tuanchauict on 10/28/16.
 */

public abstract class StickSectionFragment extends BaseGridFragment {
    TextView mStickSectionView;

    @Override
    public void onStart() {
        super.onStart();
        View stickSectionView = LayoutInflater.from(getContext()).inflate(R.layout.item_header_text, null, false);
        mStickSectionView = findView(stickSectionView, R.id.text);
        setStickSectionView(stickSectionView);
    }

    @Override
    protected void updateStickSection(@Nullable Section section) {
        mStickSectionView.setText(section.title);
    }
}
