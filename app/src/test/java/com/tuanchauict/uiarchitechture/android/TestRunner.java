package com.tuanchauict.uiarchitechture.android;

import com.tuanchauict.uiarchitechture.R;

import org.junit.runners.model.InitializationError;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.manifest.AndroidManifest;

/**
 * Created by tuanchauict on 8/3/16.
 */
public class TestRunner extends RobolectricGradleTestRunner{
    public TestRunner(Class<?> klass) throws InitializationError {
        super(klass);
    }

    @Override
    protected AndroidManifest getAppManifest(Config config) {
        final AndroidManifest appManifest = super.getAppManifest(config);
        return new AndroidManifest(
                appManifest.getAndroidManifestFile(),
                appManifest.getResDirectory(),
                appManifest.getAssetsDirectory()) {
            @Override
            public String getRClassName() throws Exception {
                return R.class.getName();
            }

            @Override
            public String getApplicationName() {
                return TestApp.class.getName();
            }
        };
    }
}
