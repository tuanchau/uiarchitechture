package com.tuanchauict.uiarchitechture.android;

import android.support.multidex.MultiDexApplication;

import com.orhanobut.logger.LLogger;
import com.orhanobut.logger.LogService;
import com.orhanobut.logger.Logger;

/**
 * Created by tuanchauict on 8/3/16.
 */
public class TestApp extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
//        App.setAppContext(this.getApplicationContext());
//        CacheService.init(this.getApplicationContext());
        Logger.compactOn();
        LLogger.addLogService(new LogService() {
            @Override
            public void run(String tag, String log, int level) {
                System.out.println(log);
            }
        });
    }
}
