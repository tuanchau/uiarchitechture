package com.ofmonsters.baseui.list;

import org.junit.Test;

import java.util.Arrays;

/**
 * Created by tuanchauict on 10/4/16.
 */
public class HybridDataProviderTest {

    @Test
    public void testHybridDataProvider_insert(){
        HybridDataProvider h = new HybridDataProvider();
        DataProvider p;
        p = new ArrayItemProvider(Arrays.asList(0,1,2,3));
        h.add(p);
        p = new ArrayItemProvider(Arrays.asList(5,6,7));
        h.add(p);

        for(int i = 0; i < h.size(); i++){
            h.setItem(i, 0, 0);
        }

        for(int i = 0; i < h.size(); i++){
            Item item = h.get(i);
            System.out.println(i + " " + item.getData());
        }
    }

    @Test
    public void testHybridDataProvider_remove(){
        HybridDataProvider h = new HybridDataProvider();
        DataProvider p;
        p = new ArrayItemProvider(Arrays.asList(0,1,2));
        h.add(p);
        p = new ArrayItemProvider(Arrays.asList(5,6,7));
        h.add(p);

        for(int i = 0; i < h.size(); i++){
            h.setItem(i, 0, 0);
        }
        h.remove(1);
        System.out.println(h.isValid(1));

        for(int i = 0; i < h.size(); i++){
            Item item = h.get(i);
            System.out.println(i + " " + item.getData());
        }
    }


}