package com.ofmonsters.baseui.list.placement;

import android.os.Build;

import com.ofmonsters.baseui.list.DataProvider;
import com.ofmonsters.baseui.list.Identifiable;
import com.tuanchauict.uiarchitechture.BuildConfig;
import com.tuanchauict.uiarchitechture.android.TestApp;
import com.tuanchauict.uiarchitechture.android.TestRunner;
import com.ofmonsters.baseui.list.ArrayItemProvider;
import com.ofmonsters.baseui.list.FooterProvider;
import com.ofmonsters.baseui.list.Item;
import com.ofmonsters.baseui.list.MasterList;
import com.ofmonsters.baseui.list.Section;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by tuanchauict on 9/19/16.
 */
@RunWith(TestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP, application = TestApp.class)
public class ItemPlacementTest {

    private static class IntegerItem implements Identifiable {
        int v;
        IntegerItem(int i) {
            v = i;
        }

        @Override
        public int getId() {
            return v;
        }
    }
    
    private void printList(MasterList list) {
        for (int i = 0; i < list.size(); i++) {
            Item item = list.get(i);
            if (item.getColumn() == 0) {
                System.out.println();
            }
            System.out.print(list.get(i) + " ");
        }
        System.out.println();
    }
    
    @Test
    public void testGenerate_noSection_rangePos() throws Exception {
        List<IntegerItem> data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(i));
        }
        ArrayItemProvider dataProvider = (ArrayItemProvider) DataProvider.newProvider(data);
        AdPositionGenerator adPositionGenerator =
                new SimpleListStyleRangeAdPositionGenerator(2, 3, 1, 1);
        MasterList list = ItemPlacement.generate(Arrays.asList(dataProvider), null, 3, adPositionGenerator).toBlocking().first();
        System.out.println(list.size());
        printList(list);
    }
    
    @Test
    public void testGenerate_noSection_rangePos_remove() throws Exception {
        List<IntegerItem> data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(i));
        }
        AdPositionGenerator adPositionGenerator =
                new SimpleListStyleRangeAdPositionGenerator(2, 3, 1, 1);
        ArrayItemProvider dataProvider = (ArrayItemProvider) DataProvider.newProvider(data);
        List<DataProvider> dataProviders = Arrays.asList(dataProvider);
        MasterList list = ItemPlacement.generate(dataProviders, null, 3, adPositionGenerator).toBlocking().first();
        System.out.println(list.size());
        printList(list);
        list.remove(4);
        list = ItemPlacement.generate(dataProviders, null, 3, adPositionGenerator).toBlocking().first();
        list.remove(2);
        list = ItemPlacement.generate(dataProviders, null, 3, adPositionGenerator).toBlocking().first();
        printList(list);
    }
    
    @Test
    public void testGenerate_noSection_rangePos_footerFix() throws Exception {
        List<IntegerItem> data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(i));
        }
        ArrayItemProvider dataProvider = (ArrayItemProvider) DataProvider.newProvider(data);
        AdPositionGenerator adPositionGenerator =
                new SimpleListStyleRangeAdPositionGenerator(2, 3, 1, 1);
        
        FooterProvider.FooterItem footer = new FooterProvider.FixColumnFooterItem(0, 1, null);
        
        MasterList list = ItemPlacement.generate(Arrays.asList(dataProvider), null, 3, adPositionGenerator, footer).toBlocking().first();
        System.out.println(list.size());
        printList(list);
    }
    
    @Test
    public void testGenerate_noSection_rangePos_footerFlex() throws Exception {
        List<IntegerItem> data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(i));
        }
        ArrayItemProvider dataProvider = (ArrayItemProvider) DataProvider.newProvider(data);
        AdPositionGenerator adPositionGenerator =
                new SimpleListStyleRangeAdPositionGenerator(2, 3, 1, 1);
        
        FooterProvider.FooterItem footer = new FooterProvider.FlexColumnFooterItem(0, 3, null);
        
        MasterList list = ItemPlacement.generate(Arrays.asList(dataProvider), null, 3, adPositionGenerator, footer).toBlocking().first();
        System.out.println(list.size());
        printList(list);
    }
    
    @Test
    public void testGenerate_withSection() throws Exception {
        List<IntegerItem> data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(i));
        }
        ArrayItemProvider dataProvider = (ArrayItemProvider) DataProvider.newProvider(data);
        List<Section> sections = new ArrayList<>();
        sections.add(new Section(10, "S1"));
//        sections.add(new Section(9, "S2"));
//        sections.add(new Section(0, "S3"));
//        sections.add(new Section(0, "S4"));
        AdPositionGenerator adPositionGenerator =
                new SimpleListStyleRangeAdPositionGenerator(2, 3, 0, 1);
        MasterList list = ItemPlacement.generate(Arrays.asList(dataProvider), sections, 3, adPositionGenerator).toBlocking().first();
        System.out.println(list.size());
        printList(list);
    }
    
    @Test
    public void testGenerate_withSection_remove() throws Exception {
        List<IntegerItem> data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(i));
        }
        ArrayItemProvider dataProvider = (ArrayItemProvider) DataProvider.newProvider(data);
        List<Section> sections = new ArrayList<>();
        sections.add(new Section(10, "S1"));
        sections.add(new Section(9, "S2"));
        sections.add(new Section(0, "S3"));
//        sections.add(new Section(0, "S4"));
        AdPositionGenerator adPositionGenerator =
                new SimpleListStyleRangeAdPositionGenerator(2, 3, 1, 1);
        MasterList list = ItemPlacement.generate(Arrays.asList(dataProvider), sections, 3, adPositionGenerator).toBlocking().first();
        System.out.println(list.size());
        printList(list);
        list.remove(2);
        list = ItemPlacement.generate(Arrays.asList(dataProvider), sections, 3, adPositionGenerator).toBlocking().first();
        printList(list);
    }
    
    @Test
    public void testGenerate_withoutSection_adList() throws Exception {
        List<IntegerItem> data = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            data.add(new IntegerItem(i));
        }
        ArrayItemProvider dataProvider = (ArrayItemProvider) DataProvider.newProvider(data);
        
        AdPositionGenerator adPositionGenerator =
                new SimpleListStyleArrayAdPositionGenerator(Arrays.asList(0, 5, 6, 10));
        
        MasterList list = ItemPlacement.generate(Arrays.asList(dataProvider), null, 2, adPositionGenerator).toBlocking().first();
        printList(list);
        
    }
    
    @Test
    public void testGenerate_withinSection_listData_noAd() throws Exception {
        List<IntegerItem> data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(1000 + i));
        }
        ArrayItemProvider d1 = (ArrayItemProvider) DataProvider.newProvider(data);
        
        data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(2000 + i));
        }
        
        List<Section> sections = new ArrayList<>();
        sections.add(new Section(10, "S1"));
        sections.add(new Section(9, "S2"));
        sections.add(new Section(0, "S3"));
        
        ArrayItemProvider d2 = (ArrayItemProvider) DataProvider.newProvider(data);
        MasterList list = ItemPlacement.generateWithinSection(Arrays.asList(d1, d2), sections, 3, null, null);
        printList(list);
    }
    
    @Test
    public void testGenerate_withinSection_listData_Ad() throws Exception {
        List<IntegerItem> data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(1000 + i));
        }
        ArrayItemProvider d1 = (ArrayItemProvider) DataProvider.newProvider(data);
        
        data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(2000 + i));
        }
        ArrayItemProvider d2 = (ArrayItemProvider) DataProvider.newProvider(data);
        
        data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(3000 + i));
        }
        ArrayItemProvider d3 = (ArrayItemProvider) DataProvider.newProvider(data);
        
        
        List<Section> sections = new ArrayList<>();
        sections.add(new Section(10, "S1"));
        sections.add(new Section(90, "S2"));
        sections.add(new Section(0, "S3"));
        
        
        AdPositionGenerator adPositionGenerator =
                new SimpleListStyleRangeAdPositionGenerator(2, 3, 1, 1);
        
        MasterList list = ItemPlacement.generateWithinSection(Arrays.asList(d1, d2, d3), sections, 3, adPositionGenerator, null);
        printList(list);
    }
    
    
    @Test
    public void testGenerate_withoutSection_listData_noAd() throws Exception {
        List<IntegerItem> data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(1000 + i));
        }
        ArrayItemProvider d1 = (ArrayItemProvider) DataProvider.newProvider(data);
        
        data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(2000 + i));
        }
        
        ArrayItemProvider d2 = (ArrayItemProvider) DataProvider.newProvider(data);
        MasterList list = ItemPlacement.generateWithoutSection(Arrays.asList(d1, d2), 3, null, null);
        printList(list);
    }
    
    @Test
    public void testGenerate_withoutSection_listData_Ad() throws Exception {
        List<IntegerItem> data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(1000 + i));
        }
        ArrayItemProvider d1 = (ArrayItemProvider) DataProvider.newProvider(data);
        
        data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(2000 + i));
        }
        ArrayItemProvider d2 = (ArrayItemProvider) DataProvider.newProvider(data);
        
        data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new IntegerItem(3000 + i));
        }
        ArrayItemProvider d3 = (ArrayItemProvider) DataProvider.newProvider(data);
        
        AdPositionGenerator adPositionGenerator =
                new SimpleListStyleRangeAdPositionGenerator(2, 3, 1, 1);
        
        MasterList list = ItemPlacement.generateWithoutSection(Arrays.asList(d1, d2, d3), 3, adPositionGenerator, null);
        printList(list);
    }
}