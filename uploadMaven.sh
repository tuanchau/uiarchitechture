echo "Pulling new code..."

PULL_RESULT="$(git pull)"
echo $PULL_RESULT
if [ ${#PULL_RESULT} -ge 20 ]
then
    exit
fi

./gradlew  upAr

rm -rf ~/.gradle/caches/modules-2/files-2.1/com.notabasement.mangarock.android/fast_scroll_recycler
rm -rf ~/.gradle/caches/modules-2/metadata-2.16/descriptors/com.notabasement.mangarock.android/fast_scroll_recycler
rm -rf ~/.gradle/caches/modules-2/files-2.1/com.notabasement.mangarock.android/mr_ui_core_list
rm -rf ~/.gradle/caches/modules-2/metadata-2.16/descriptors/com.notabasement.mangarock.android/mr_ui_core_list
